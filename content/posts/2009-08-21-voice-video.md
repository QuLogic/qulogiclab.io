+++
title = "Voice & Video"
published = 2009-08-21T00:17:00.003000-04:00
tags = ["Pidgin"]
+++

Sooo, [Pidgin](https://pidgin.im) supports voice and video now.

It's only on XMPP (aka Google Talk), and only on Linux, but hopefully that will
change soon. I hear it's a real pain to get all the farsight/gstreamer
dependencies compiled for Windows right now.

And here's hoping we can get MSN support soon, too. I'm not too sure how well
farsight supports it though.
