+++
title = "Booting Ubuntu/Linux Mint from ISO"
published = 2010-01-18T22:50:00-05:00
tags = ["Linux", "Ubuntu", "LiveCD", "Multiboot", "Linux Mint"]
+++

[Last time]({{< ref "2010-01-16-booting-multiple-live-cds-from-a-single-usb"
>}}) I prepared the USB drive to boot from ISO. That's great, but it's no good
if there are no ISOs to boot into.

First off, I set up Ubuntu in GRUB. Fortunately, this was pretty easy to do.
Ubuntu supports booting from ISO using the `iso-scan` kernel parameter. Since
Linux Mint is derived from Ubuntu, it also supports this parameter. The
following lines in the GRUB2 config will enable booting from a Ubuntu/Linux
Mint ISO.

{{< highlight "clike" >}}
menuentry "Ubuntu 9.10 32bit" {
    loopback loop /ubuntu-9.10-desktop-i386.iso
    linux (loop)/casper/vmlinuz boot=casper \
        iso-scan/filename=/ubuntu-9.10-desktop-i386.iso noeject noprompt --
    initrd (loop)/casper/initrd.lz
}
{{< /highlight >}}

The `loopback` line loads the ISO in a way that GRUB can locate the kernel and
initrd. The stuff about casper is something used for data persistence (so that
the ISO remains read-only). I don't have that enabled yet. This GRUB entry
works similarly for Ubuntu (32- & 64-bit), Ubuntu Netbook Remix, and Linux Mint
(32- & 64-bit).
