+++
title = "Moving Out..."
published = 2009-07-24T18:26:00.005000-04:00
tags = ["PEY"]
+++

Of the office, I mean.

Here we are all packed up and ready to go. Instead of the old CRT, I'm only
taking the Dell TV/monitor that I had lying around. I still haven't found a
power supply for it, but I hope to pick one up in the extra refuse that we're
not taking with us.

{{< figure src="IMG_0868.JPG" >}}

Update: And here we are in the new location:

{{< figure src="IMG_0876.JPG" >}}

It's a little bit smaller, I think. If you notice, now we've got fancy IP
phones. What sucks is I brought that Dell TV/monitor instead of a CRT, only
finding a power supply now. And it doesn't work! Ah well, instead, I borrowed
the LCD from the other PEY student. And some of you might recognize that
calendar there...
