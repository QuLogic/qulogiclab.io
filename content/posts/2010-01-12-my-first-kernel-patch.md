+++
title = "My first kernel patch"
published = 2010-01-12T22:56:00.008000-05:00
tags = ["Linux"]
+++

OK, so it's not really anything big. In fact, it's really just a copy and paste
(almost).

So, way back when [acerhk](http://freshmeat.sourceforge.net/projects/acerhk)
actually compiled, I was able to use the extra keys on the laptop. These were
the volume, media, mail, etc. keys. Now with Fedora and a much newer kernel, it
no longer compiles.

Every once in a while, I tried to see if I could fix the compile, but never
really got anywhere. But then I found something interesting in the kernel
sources. In `drivers/input/serio/i8042-x86ia64io.h` there was something about a
Dritek quirk. I recognized the name as something the acerhk driver also
supplied. So, I rebooted and tried that out using `i8042.dritek=1` and voilà,
the extra keys worked. And a quick copy and paste later and I had me a patch.
Just like that:

{{< highlight "diff" >}}
@@ -467,6 +467,13 @@ static struct dmi_system_id __initdata i
  */
 static struct dmi_system_id __initdata i8042_dmi_dritek_table[] = {
     {
+        .ident = "Acer Aspire 5610",
+        .matches = {
+            DMI_MATCH(DMI_SYS_VENDOR, "Acer"),
+            DMI_MATCH(DMI_PRODUCT_NAME, "Aspire 5610"),
+        },
+    },
+    {
         .ident = "Acer Aspire 5630",
         .matches = {
             DMI_MATCH(DMI_SYS_VENDOR, "Acer"),
{{< /highlight >}}
