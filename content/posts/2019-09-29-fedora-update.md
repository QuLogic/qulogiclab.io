+++
title = "Fedora Update 2019 Weeks 35--38"
date = 2019-09-29T20:57:20-04:00
tags = ["Fedora"]
+++

It's been quite some time since the last update, but things have been rather
calm. There's been the usual round of updated packages, and a few new
dependencies. Otherwise, there's not been anything out of the ordinary.

<!--more-->

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-argon2" "0.2.0-7" "Rebuilt for libb2" %}}
{{% fedora-update "R-callr" "3.3.2-1" %}}
{{% fedora-update "R-curl" "4.1-1" %}}
{{% fedora-update "R-curl" "4.2-1" "Second update" %}}
{{% fedora-update "R-ellipsis" "0.3.0-1" %}}
{{% fedora-update "R-gdtools" "0.2.0-1" %}}
{{% fedora-update "R-httpuv" "1.5.2-1" %}}
{{% fedora-update "R-knitr" "1.25-1" %}}
{{% fedora-update "R-microbenchmark" "1.4.7-1" %}}
{{% fedora-update "R-mockery" "0.4.2-1" %}}
{{% fedora-update "R-nycflights13" "1.0.1-1" %}}
{{% fedora-update "R-pkgconfig" "2.0.3-1" %}}
{{% fedora-update "R-pkgdown" "1.4.0-1" %}}
{{% fedora-update "R-pkgdown" "1.4.1-1" "Second update" %}}
{{% fedora-update "R-rgdal" "1.4.4-4" "Remove old proj subpackage dependencies" %}}
{{% fedora-update "R-tidyr" "1.0.0-1" %}}
{{% fedora-update "R-tinytex" "0.16-1" %}}
{{% fedora-update "R-whisker" "0.4-1" %}}
{{% fedora-update "golang-github-briandowns-spinner" "1.7.0-1" %}}
{{% fedora-update "golang-github-cosmos72-gomacro" "2.7-8.20190923git4f667f8" "Fixes build with Go 1.13" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.3.0-1" %}}
{{% fedora-update "golang-github-git-lfs-gitobj" "1.4.1-1" %}}
{{% fedora-update "golang-github-git-lfs-wildmatch" "1.0.4-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.5.2-1" %}}
{{% fedora-update "golang-github-tdewolff-test" "1.0.4-1" %}}
{{% fedora-update "golang-github-tdewolff-test" "1.0.5-1" "Second update" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.6.20190925git95bc4ff" "Fedora 31 only" %}}
{{% fedora-update "ocrmypdf" "9.0.3-1" "Fedora 31 and above only" %}}
{{% fedora-update "proj-datumgrid-europe" "1.4-1" %}}
{{% fedora-update "python-affine" "2.3.0-1" %}}
{{% fedora-update "python-dask" "2.3.0-1" %}}
{{% fedora-update "python-dask" "2.4.0-1" "Second update" %}}
{{% fedora-update "python-dask" "2.5.0-1" "Third update" %}}
{{% fedora-update "python-fsspec" "0.5.1-1" %}}
{{% fedora-update "python-heapdict" "1.0.1-1" %}}
{{% fedora-update "python-matplotlib" "3.1.1-2" "Backport bool deprecation warning fix" %}}
{{% fedora-update "python-mplcairo" "0.2-1" "Fixes Python 3.8 build" %}}
{{% fedora-update "python-mplcursors" "0.3-1" "Fixes Python 3.8 build" %}}
{{% fedora-update "python-pandas" "0.25.1-2" "Backport patch for Python 3.8 compatibility" %}}
{{% fedora-update "python-pikepdf" "1.6.3-1" %}}
{{% fedora-update "python-pikepdf" "1.6.4-1" "Second update" %}}
{{% fedora-update "python-pyshtools" "4.5-1" %}}
{{% fedora-update "python-rasterio" "1.0.27-1" %}}
{{% fedora-update "python-rasterio" "1.0.28-1" "Second update" %}}
{{% fedora-update "python-snuggs" "1.4.7-1" %}}
{{% fedora-update "tinygo" "0.8.0-1" %}}
{{% fedora-update "xtl" "0.6.6-1" %}}
{{% fedora-update "xtl" "0.6.7-1" "Second update" %}}

New packages
============

* {{% rhbz 1750091 "R-systemfonts" %}} --- System Native Font Finding
* {{% rhbz 1751456 "R-wesanderson" %}} --- Wes Anderson Palette Generator
* {{% rhbz 1755217 "R-repurrrsive" %}} --- Examples of Recursive Lists and
  Nested or Split Data Frames
* {{% rhbz 1755695 "R-websocket" %}} --- WebSocket Client Library

Reviews
=======

* {{% rhbz 1743847 "golang-github-azure-amqp-common" %}} --- Common types and
  interfaces for use in Service Bus and Event Hubs
* {{% rhbz 1744287 "golang-mongodb-mongo-driver" %}} --- Go driver for MongoDB
* {{% rhbz 1744388 "golang-gocloud" %}} --- Library and tools for open cloud
  development in Go
* {{% rhbz 1750017 "python-django-prometheus" %}} --- Django middlewares to
  monitor your application with Prometheus.io (waiting for submitter)
* {{% rhbz 1750019 "python-inflection" %}} --- Port of Ruby on Rails inflector
  to Python
* {{% rhbz 1754302 "golang-github-google-cmdtest" %}} --- Simplify testing of
  command-line interfaces
