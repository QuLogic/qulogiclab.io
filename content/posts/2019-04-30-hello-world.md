+++
title = "Hello, World! Again..."
date = 2019-04-30T21:06:27-04:00
tags = ["Fedora", "Golang", "Haskell", "Python", "R"]
+++

Inspired by [@decathorpe](https://decathorpe.com/) and the release of [Fedora
30](https://fedoramagazine.org/announcing-fedora-30/), I've decided to start
making semi-weekly updates of my packaging work in Fedora. I only say
"semi-weekly" as I'm not sure I can commit to every week. Here's a short look
back at the past few years of packaging.

I started packaging quite some time back using
[copr](https://copr.fedorainfracloud.org/coprs/qulogic/) to create Fedora
packages for some things I've worked on ([ObsPy](https://obspy.org/),
[Cartopy](https://scitools.org.uk/cartopy), etc.), but only started officially
packaging things in August 2017. I started out with GIS- and
cartography-related *Python* packages, but eventually branched out into other
things. Now I maintain about 50 or so *Python*-based packages. Some of the more
interesting applications are generally OCR-related, like
[OCRmyPDF](https://github.com/jbarlow83/OCRmyPDF) and
[Paperwork](https://openpaper.work/).

One of the other reasons to get into packaging was to update
[git-annex](https://git-annex.branchable.com/) to something recent. This lead
down a pretty deep *Haskell* dependency tree. There are over a hundred there, but
I'm not sure I quite understand any *Haskell* yet.

Speaking of git, I also packaged [git-lfs](https://git-lfs.github.com/), which
lead to a lot of *Golang*. There are about 40 packages there, including tools
such as [fzf](https://github.com/junegunn/fzf),
[git-time-metric](https://github.com/git-time-metric/gtm), and
[godoctor](http://gorefactor.org/). As a member of the [*Go*
SIG](https://pagure.io/group/GoSIG), I also try to get other packages
up-to-date as necessary, and am working on some internal packaging tools.

However, the language with by far the most packages is, surprisingly, *R*. I'm
not really a heavy user of *R*, but I had a thought of getting the *tidyverse*
set of packages into Fedora. This turned into a long and arduous task which is
perhaps only just barely seeing the light at the end of the tunnel. I've
reached over 200 packages, but fortunately many of them are old and rarely
updated, which reduces the burden somewhat.

All told, that's >400 packages which is pretty surprising to me. It just sort
of grew unexpectedly. They're mostly in a bit of a niche though, so it allows
me to work reasonably independently. And fortunately, they don't all release at
the same time, which spreads out the load significantly. All of this probably
wouldn't be possible without reviewers like @eclipseo, who I'm pretty sure
reviewed 95% of my packages.
