+++
title = "Fedora Update 2019 Weeks 46--52"
date = 2019-12-29T21:44:28-05:00
images = ["posts/2019-12-29-fedora-update/updates-by-release.png"]
tags = ["Fedora"]
+++

It's been another little while since the last update. Mostly, this has been
because the home Internet has been down for a while, so I've not been doing
much Fedora stuff the last month. Prior to that little break though, updates
were relatively steady.

As a followup to the last update, I've now removed automated `Suggests` when
they do not exist in Fedora yet from *R* packages *that I do not own*. This is
a continuation of the work mentioned in the last update where I removed
`Suggests` from my own packages.

In any case, it looks like this will be the last update of the year. Over the
last year, I've made ~1000 commits, created 133 new packages, and issued 1944
updates. This is about 4% of all Fedora updates over the past year.

{{< image src="updates-by-type.png" alt="Graph of updates by type" >}}

Fortunately, there have been very few security updates necessary, so that line
remains pretty small. I believe the 'unspecified' update types comes from two
sources: a) the sharp rise is from when Rawhide was Branched to form Fedora 31
(as in below), and b) the continuing rise is from Rawhide getting automated
Bodhi updates (which is new this year.)

{{< image src="updates-by-release.png" alt="Graph of updates by release" >}}

Fedora 28 and 29 stopped receiving updates at their respective End-of-Life
dates, and Fedora 31 gets a sharp explosion at the time it was Branched. While
it may seem as if Fedora 31 pulls ahead in update count, keep in mind that this
graph started at 0 from the beginning of the year, and not from the start of a
release's existence. That being said, there are definitely packages that only
get updated in Rawhide or Fedora 31, which does account for some of that
difference, and you can see a few of the larger such jumps in the above graph.

Anyway, for next year, I'm hoping that updates continue to go smoothly, and
perhaps some of this can even be automated. I don't know when or if that will
happen, but a lot of these updates are pretty rote and the process could
benefit from it.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-BiocParallel" "1.20.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-BiocParallel" "1.16.5-4.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-DBI" "1.0.0-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-DelayedArray" "0.12.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-DelayedArray" "0.4.1-6.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-GenomeInfoDb" "1.22.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-GenomeInfoDb" "1.16.0-6.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-GenomicAlignments" "1.22.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-GenomicAlignments" "1.18.1-4.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-R6" "2.4.1-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-R6" "2.2.2-6.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-RSQLite" "2.1.2-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-RSQLite" "2.1.1-7.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-Rcpp" "1.0.3-2"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-Rsamtools" "2.0.3-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-Rsamtools" "1.34.1-4.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-S4Vectors" "0.24.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-S4Vectors" "0.22.0-3.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-SummarizedExperiment" "1.16.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-SummarizedExperiment" "1.10.1-6.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-XVector" "0.26.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-XVector" "0.24.0-3.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-biomaRt" "2.18.0-13"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-blob" "1.2.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-blob" "1.1.1-7.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-car" "2.0.22-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-littler" "0.3.9-2"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-lmtest" "0.9.37-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-memoise" "1.1.0-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-msm" "1.6.6-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-multcomp" "1.4.10-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-rtracklayer" "1.46.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-rtracklayer" "1.44.0-4.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-snow" "0.4.3-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-snow" "0.4.2-9.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-systemfit" "1.1.22-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-testthat" "2.3.0-2.fc32"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-testthat" "2.2.1-2.fc31"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-waveslim" "1.7.5.1-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-zoo" "1.8.6-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-XML" "3.98.1.20-1"
    "Update was forgotten when it was actually built" %}}
{{% fedora-update "R-caTools" "1.17.1.3-1" %}}
{{% fedora-update "R-curl" "4.3-1" %}}
{{% fedora-update "R-future" "1.15.1-1" %}}
{{% fedora-update "R-igraph" "1.2.4.2-1" %}}
{{% fedora-update "R-prettydoc" "0.3.1-1" %}}
{{% fedora-update "R-quadprog" "1.5.8-1" %}}
{{% fedora-update "R-rgdal" "1.4.8-1" %}}
{{% fedora-update "R-rlang" "0.4.2-1" %}}
{{% fedora-update "R-rmarkdown" "1.18-1" %}}
{{% fedora-update "R-roxygen2" "7.0.1-1" "Fedora Rawhide only" %}}
{{% fedora-update "R-roxygen2" "7.0.2-1" "Fedora Rawhide only" %}}
{{% fedora-update "R-rversions" "2.0.1-1" %}}
{{% fedora-update "R-scales" "1.1.0-1" %}}
{{% fedora-update "R-selectr" "0.4.2-1" %}}
{{% fedora-update "fzf" "0.19.0-1" %}}
{{% fedora-update "golang-github-briandowns-spinner" "1.8.0-1" %}}
{{% fedora-update "golang-github-nicksnyder-i18n-2" "2.0.3-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.6.0-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.10-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.11-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.12-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.13-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.4.0-1" %}}
{{% fedora-update "golang-github-tdewolff-test" "1.0.6-1" %}}
{{% fedora-update "ocrmypdf" "9.1.1-1" %}}
{{% fedora-update "python-dask" "2.8.0-1" %}}
{{% fedora-update "python-dask" "2.8.1-1" %}}
{{% fedora-update "python-fsspec" "0.6.0-1.fc31" %}}
{{% fedora-update "python-fsspec" "0.6.1-1.fc31" %}}
{{% fedora-update "python-matplotlib" "3.1.2-1" %}}
{{% fedora-update "python-partd" "1.1.0-1" %}}

New packages
============

* {{% rhbz 1762463 "golang-github-yuin-goldmark" %}} --- Markdown parser
  written in Go
* {{% rhbz 1763128 "golang-github-google-slothfs" %}} --- FUSE filesystem for
  light-weight, lazily-loaded, read-only Git
* {{% rhbz 1763145 "golang-bug-serial-1" %}} --- Cross-platform serial library
  for Golang

Reviews
=======

* {{% rhbz 1767918 "targetd" %}} --- Service to make storage remotely
  configurable
