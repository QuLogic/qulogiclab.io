+++
title = "Booting recovery images from USB"
published = 2010-01-30T19:29:00.007000-05:00
tags = ["Linux", "LiveCD", "Parted Magic", "memtest", "Multiboot"]
+++

So, I'm still trying to figure out the Fedora boot, but in the meantime, I
tried to figure out how to boot from a few recovery systems.

First off, let's try [Parted Magic](https://partedmagic.com/). This one is
pretty simple since they provide a "USB" version. If you extract that, you'll
find a `boot` and a `pmagic` folder. Copy the `pmagic` folder to the USB drive.
In `boot/grub/grub.lst`, you'll find a whole bunch of boot options. I only
chose the default and failsafe versions for my USB, but some of the others
might be useful for you. For GRUB2, however, it'll look a little different:

{{< highlight "clike" >}}
menuentry "Parted Magic (Default)" {
    linux /pmagic/bzImage edd=off noapic load_ramdisk=1 prompt_ramdisk=0 \
        rw vga=791 loglevel=0 max_loop=256 keymap=us
    initrd /pmagic/initramfs
}

menuentry "Parted Magic (Failsafe)" {
    linux /pmagic/bzImage edd=off acpi=off noapic load_ramdisk=1 \
        prompt_ramdisk=0 rw vga=normal nolapic nopcmcia noscsi nogpm \
        consoleboot nosmart keymap=us nosshd nosound max_loop=256
    initrd /pmagic/initramfs
}
{{< /highlight >}}

Second, you'll probably want [memtest](https://www.memtest.org/). The memtest
page has a "Pre-Compiled Bootable Binary" which works just fine. You can also
find memtest in the PartedMagic package. Whichever one you choose, it can just
be placed directly on the USB drive. The line in GRUB2 is not exactly the same
for memtest since it's 16-bit:

{{< highlight "clike" >}}
menuentry "Memory test (memtest86+)" {
    linux16 /memtest86+.bin
}
{{< /highlight >}}
