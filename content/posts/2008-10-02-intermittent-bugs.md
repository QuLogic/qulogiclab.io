+++
title = "Intermittent Bugs"
published = 2008-10-02T23:02:00.002000-04:00
tags = ["PEY"]
+++

I hate intermittent bugs.

You know, those ones that never always happen in a reproduceable manner. I had
to test this bug that seems to happen 1 time in 20+ tries. Not to mention that
I needed to do it from a cold boot, so it took 1-2 minutes per trial.
Fortunately, I was able to automate it so I didn't need do anything except hit
the power button every time it worked.

Twenty tries and I get nothing. So I send an email saying so, and that's just
when the bug shows up. Then had to try a few different versions to figure out
when it was introduced. But in the end it seemed nobody else could reproduce
the bug (though someone could before).

Swapped development boards with someone and couldn't reproduce the bug after 50
tries... What a waste of a day...
