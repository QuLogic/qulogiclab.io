+++
title = "Mentoring a student for GSoC"
published = 2010-05-03T20:18:00.003000-04:00
tags = ["GSoC", "Pidgin"]
+++

So last week the [accepted
proposals](https://www.google-melange.com/archive/gsoc/2010) for
[GSoC](https://code.google.com/soc/) were announced.
[Pidgin](https://pidgin.im/) was given 4 slots.

First up, we have Ivan Komarov working on [improving
ICQ](https://developer.pidgin.im/wiki/GSoC2010/ImproveICQ). I don't really use
ICQ, so I can't say much, but if *you* do, hopefully there'll be a lot of great
things coming out of it that will be useful to you. Next up is Adam Fowler
working on a [better chat log
viewer](https://developer.pidgin.im/wiki/GSoC2010/BetterChatLogViewer). People
really seem to complain about what we have now and at the very least, it's kind
of slow once you have a lot of logs, so there should be room for some good
improvements there. And then we have Gilles Bedel working on [detachable
libpurple
sessions](https://developer.pidgin.im/wiki/GSoC2010/DetachableLibpurple). I'm
not sure how often I'd use it, but every once in a while, I feel like checking
in on Pidgin from the laptop and it's a very painful process through SSH and
D-Bus. This project should make that sort of thing much simpler. Finally, we
have Jorge Villaseñor Salinas working on [refactoring
MSN](https://developer.pidgin.im/wiki/GSoC2010/MSN_Refactor).

So that last one is the one I'm mentoring. Personally, I've been wanting to
re-write bits and pieces of the MSN prpl for a while now, but I've never had
the time to get around to it. Jorge's already done some great work getting
custom smiley support working for Pidgin 2.5.0. I'm sure he'll have no problem
re-working the MSN prpl. I'm looking forward to getting a more structured and
(hopefully) well-designed prpl which should make development a lot easier and
quicker.
