+++
title = "Imported BlogSpot posts"
date = 2019-05-24T19:52:55-04:00
+++

I've imported my [previous BlogSpot posts](https://qulogic.blogspot.com/), and
cleaned up a few dead links and other issues.
<!--more-->
I started with
[blogger-to-hugo](https://bitbucket.org/petraszd/blogger-to-hugo), though I had
to patch it a bit in order to download the full-size images instead of
thumbnails. Some cleanup was necessary as code blocks were not perfect, and
embedded YouTube videos were not transferred over. All-in-all though, I'm sure
that script saved quite a bit of time.

There are no comments here, and I'm not sure what I'll do about that, but there
weren't _too_ many on the old posts anyway.
