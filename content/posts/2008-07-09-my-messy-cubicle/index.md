+++
title = "My messy cubicle"
published = 2008-07-09T20:45:00.005000-04:00
tags = ["PEY"]
+++

Well, it's not really that messy, but it sure is full of stuff...

{{< figure src="IMG_7280.JPG" >}}

Let's see what we have here...

On the left is my Linux computer. I was using the monitor on top of it for
that, but I got tired of the CRT-ness, so I got that next 17" LCD over for it.

The oscilloscope is for testing output from my test board, which if you ask me,
looks *nothing* like a square wave, but apparently is quite alright for DTV.
The test board is right above it, though you can't see it too clearly. I can't
give away all AMD's secrets, now can I? :wink: That pole there has a TV feed
with some random stuff for testing. And CityTV, if I really feel bored.

The darker screen behind the keyboard is the Windows laptop. It's kinda old,
and I don't like trackpads, so I use
[Synergy](https://synergy2.sourceforge.io/) to control it from the Linux
machine. Darn old keyboard and mouse, but they're way better than working on a
laptop. The larger CRT at the right is a second display for the laptop. At
least it does 85Hz, so it's not so tiring as the other one.

At the top are testing displays. The Dell on the right doesn't have a power
supply, so I don't use it yet, but I probably will for some dual-output tests.
The Samsung's a lovely 24" widescreen which I'm using to test board output.
Evidently, things aren't working, as that garbled mess you see should look
something like the image conveniently hiding everything on the three other
displays (That's right, no secret information for you!).

Finally, off camera to the right, there's another test board taking up the rest
of my desk. I have just enough room to fit my AMD coffee mug (filled with
water, of course) and Engineering notebook. On the floor, there's also a 40"
Sony I get to play with as well (You'll just have to believe me). I seriously
ran out of plugs to connect all these things.
