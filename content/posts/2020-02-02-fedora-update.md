+++
title = "Fedora Update 2020 Weeks 1--5"
date = 2020-02-02T17:43:26-05:00
tags = ["Fedora"]
+++

It looks like it's another month since the last update. This long span seems to
be becoming a trend, but I had good reason, being on vacation for much of this
one. Since, as mentioned in the last update, the home Internet was down for a
bit, that meant a bit of catch-up after both of these periods. Additionally,
this was also the period in which the Mass Rebuild for Fedora 32 was run, and I
took a little break for that. So this last period was a bit of an up-and-down
as far as packaging work goes.

The Mass Rebuild found about 20 or so of my packages that failed to build. Some
of these were false positives, and some of them are due to [changes in GCC
10](https://fedoraproject.org/wiki/Changes/GCC10), notably Fortran handling of
rank mismatches and the switch to `-fno-common` for C. For `R-Rmpfr`, a patch
was already written upstream, which I simply had to backport. For `R-deldir`, I
wrote a patch and sent it upstream, and it's in the latest release.

One other thing I'm happy about is that I've _finally_ been able to fix the
build of `R-data.table` on non-x86 architectures. This has been broken for
several upstream releases, and I've not been able to update in Fedora since
1.12.0. I wrote and proposed two patches to upstream to fix this:

* [Improve `fread` for very small or very large fp
  numbers](https://github.com/Rdatatable/data.table/pull/4165) --- This fixes
  conversion of strings to floating-point numbers on `armv7hl` and `ppc64le` by
  following a procedure similar to *R*'s builtin methods.
* [Use consistent types with
  `fwriteMainArgs.nrow`](https://github.com/Rdatatable/data.table/pull/4213)
  --- This fixes incorrect types passed to string formatting functions. Such an
  error is something common enough in C that the compiler grew warnings for it,
  but it only works on the standard library by default. By accident or de facto
  standards, the original code seemed to work everywhere but `armv7hl`.

There's one more patch in Fedora that may or may not be sent upstream, but now
the package can build on all architectures again, so we're back on the latest
version.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-R.cache" "0.14.0-1" %}}
{{% fedora-update "R-R.utils" "2.9.2-1" %}}
{{% fedora-update "R-Rmpfr" "0.7.2-6.fc32"
    "Backport fix for `-fno-common` change in gcc 10" %}}
{{% fedora-update "R-callr" "3.4.0-1" "Rawhide only" %}}
{{% fedora-update "R-cli" "2.0.0-1" "Rawhide only" %}}
{{% fedora-update "R-cli" "2.0.1-1" %}}
{{% fedora-update "R-data.table" "1.12.8-1"
    "Fix tests on alternate arches mentioned above" %}}
{{% fedora-update "R-deldir" "0.1.23-3.fc32"
    "Fix rank mismatch build errors from gcc 10" %}}
{{% fedora-update "R-fansi" "0.4.1-1" %}}
{{% fedora-update "R-farver" "2.0.2-1" %}}
{{% fedora-update "R-geepack" "1.3.1-1" %}}
{{% fedora-update "R-globals" "0.12.5-1" %}}
{{% fedora-update "R-hms" "0.5.3-1" %}}
{{% fedora-update "R-listenv" "0.8.0-1" %}}
{{% fedora-update "R-mime" "0.8-1" %}}
{{% fedora-update "R-pillar" "1.4.3-1" %}}
{{% fedora-update "R-plyr" "1.8.5-1" %}}
{{% fedora-update "R-prettycode" "1.1.0-1" %}}
{{% fedora-update "R-repr" "1.0.2-1" %}}
{{% fedora-update "R-reticulate" "1.14-1" %}}
{{% fedora-update "R-rmarkdown" "2.0-1" "Rawhide only" %}}
{{% fedora-update "R-rsconnect" "0.8.16-1" %}}
{{% fedora-update "R-stringi" "1.4.4-1" %}}
{{% fedora-update "R-tinytex" "0.18-1" %}}
{{% fedora-update "R-vctrs" "0.2.1-1" %}}
{{% fedora-update "exercism" "3.0.13-1" %}}
{{% fedora-update "git-lfs" "2.9.0-1" "Rawhide only" %}}
{{% fedora-update "git-lfs" "2.9.2-1" %}}
{{% fedora-update "golang-github-alecthomas-chroma" "0.6.9-1" %}}
{{% fedora-update "golang-github-alecthomas-chroma" "0.7.0-1" %}}
{{% fedora-update "golang-github-alecthomas-chroma" "0.7.1-1" %}}
{{% fedora-update "golang-github-imdario-mergo" "0.3.8-1" %}}
{{% fedora-update "golang-github-mattn-colorable" "0.1.4-1" %}}
{{% fedora-update "golang-github-mattn-isatty" "0.0.11-1" %}}
{{% fedora-update "golang-github-mattn-runewidth" "0.0.7-1" %}}
{{% fedora-update "golang-github-mattn-tty" "0.0.3-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "0.1.9-1" %}}
{{% fedora-update "golang-github-rs-zerolog" "1.17.2-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.6.2-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.4.1-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.4.2-1" %}}
{{% fedora-update "golang-gopkg-yaml-2" "2.2.7-1" %}}
{{% fedora-update "golang-x-sys" "0-0.30.20200103gitc96a22e" %}}
{{% fedora-update "htmltest" "0.11.0-1" %}}
{{% fedora-update "libinsane" "1.0.3-1" %}}
{{% fedora-update "ocrmypdf" "9.3.0-1" %}}
{{% fedora-update "ocrmypdf" "9.4.0-1" %}}
{{% fedora-update "ocrmypdf" "9.5.0-1" %}}
{{% fedora-update "paperwork" "1.3.1-1" %}}
{{% fedora-update "proj-datumgrid-europe" "1.5-1" %}}
{{% fedora-update "proj-datumgrid-north-america" "1.3-1" %}}
{{% fedora-update "proj-datumgrid-oceania" "1.1-1" %}}
{{% fedora-update "python-dask" "2.10.0-1" %}}
{{% fedora-update "python-dask" "2.9.1-1" %}}
{{% fedora-update "python-fiona" "1.8.13-1" %}}
{{% fedora-update "python-fsspec" "0.6.2-1" %}}
{{% fedora-update "python-gpxpy" "1.4.0-1" "Update and fix Python 3.9" %}}
{{% fedora-update "python-jupyter-console" "6.1.0-1" %}}
{{% fedora-update "python-paperwork-backend" "1.3.1-1" %}}
{{% fedora-update "python-pep8-naming" "0.9.1-1" %}}
{{% fedora-update "python-pikepdf" "1.10.0-1" %}}
{{% fedora-update "python-pikepdf" "1.8.1-1" "Rawhide only" %}}
{{% fedora-update "python-pikepdf" "1.8.2-1" "Rawhide only" %}}
{{% fedora-update "python-pikepdf" "1.8.3-1" %}}
{{% fedora-update "python-pypillowfight" "0.3.0-1" %}}
{{% fedora-update "python-rasterio" "1.1.2-1" %}}
{{% fedora-update "python-tblib" "1.6.0-1" %}}
{{% fedora-update "python-zarr" "2.4.0-1" %}}
{{% fedora-update "xeus" "0.23.3-1" %}}
{{% fedora-update "xtl" "0.6.11-1" %}}

New packages
============

* {{% rhbz 1787271 "golang-github-ssgelm-cookiejarparser" %}} --- Parses a curl
  cookiejar file into a Go http.CookieJar
* {{% rhbz 1788782 "R-broom" %}} --- Convert Statistical Analysis Objects into
  Tidy Tibbles
* {{% rhbz 1789637 "R-modelr" %}} --- Modelling Functions that Work with the
  Pipe

Reviews
=======

* {{% rhbz 1750017 "python-django-prometheus" %}} --- Django middlewares to
  monitor your application with Prometheus.io
* {{% rhbz 1768192 "git-secrets" %}} --- Prevents committing secrets and
  credentials into git repos<Paste>
* {{% rhbz 1786842 "python-requests-futures" %}} --- Asynchronous Python HTTP
  Requests
* {{% rhbz 1786953 "python-aiounittest" %}} --- Test asyncio code more easily
