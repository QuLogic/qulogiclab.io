+++
title = "Downgrading ext4 to ext3"
published = 2009-11-27T23:38:00-05:00
tags = ["Linux", "Fedora"]
+++

So, a little while ago, I replaced [Gentoo](https://www.gentoo.org/) with
[Fedora 11](https://fedoraproject.org/) on my laptop. The laptop is a little
older and prone to overheating when compiling, so I didn't see the value in
using Gentoo there. And I never quite figured out the cross-compile for my
desktop since it was AMD64-&gt;x86.

I ran into one very annoying problem, and that was with ext4. Every so often,
any disk I/O would just stop. I could no longer load programs, and if anything
attempted to write a file, it would freeze (technically, it would enter the
"uninterruptible" state and get stuck). I found a few kernel OOPS in the log,
and reported those with the Fedora tool for that, but it didn't seem like it
happened for many people.

In any case, the real problem is that I had to force a reboot, and ext4 would
*always* lose something. Sometimes it was some just-created data that could be
replaced, and other times, the whole file system seemed messed up. Random
libraries would crash or fail to load because they were corrupted. It was quite
annoying.

So, I've been looking for a way to 'downgrade' to ext3. I've used ext3 for a
long time on my desktop, and also on the laptop with Gentoo, and never ran into
anything quite so bad. The only problem was no-one seemed to have a guide to go
*to* ext 3. The best I could find is to create a new partition and copy files
there. Unfortunately, I didn't have enough free space to get that done.
(Re-partitioning is not that hard since Fedora uses LVM.)

After much searching, I have managed to find a solution.
[Fsarchiver](https://www.fsarchiver.org/) provides an easy way to back up a
complete file system. Once you have that backup, you can recreate it using a
different file system format without losing any of the metadata (assuming the
new one supports it all). Also, because it uses compression, I was able to fit
what I needed onto a USB stick, and restore from there.

I used the Fedora 11 LiveCD (on the same USB stick I used for the backup) to do
all of this. Fortunately, fsarchiver is provided in a Fedora package that can
be installed from the default repositories. I'm doing this from memory, but it
should go something like this (I'll only show the fsarchiver bits):

{{< highlight "bash" >}}
> yum install fsarchiver
> fsarchiver savefs /media/live/rootfs.fsa /dev/mapper/lvm_name
> # re-format partition
> fsarchiver restfs /media/live/rootfs.fsa id=0,dest=/dev/mapper/lvm_name,mkfs=ext3
{{< /highlight >}}
