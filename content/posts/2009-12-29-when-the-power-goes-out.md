+++
title = "When the power goes out..."
published = 2009-12-29T00:51:00.003000-05:00
tags = ["Infrastructure", "Technology"]
+++

A little while ago, one of my friends text me because the power had gone out in
her building. She was wondering whether the power was out where I was.
Fortunately for me, the power was just fine.

So I thought to myself, "I've got the power of the Internet to look into these
things!" Off to Google, and it turns out it's pretty hard to find out about
recent power outages. What came up was lots and lots of old news about previous
outages that were already fixed. Now I'm pretty good with my Google-fu, so
maybe it was just a minor outage and thus less (or no) news.

However, I did manage to find [this little
page](https://www.hydroone.com/stormcenter/). It's a map with the location of
power outages and repairs in regions covered by
[HydroOne](https://www.hydroone.com/), the power distribution company for
Ontario. Now this is how important services should be run. The information is
available easily, and quickly. In fact, it's got its own link right on the
front page. The one problem is, if you follow the blue line, you'll see that
Toronto is excluded. That's because we have our own local power supply company
here, Toronto Hydro. So I can't find out about my friend's problem from
HydroOne.

What can I find on [Toronto Hydro's website](https://www.torontohydro.com/)?
Absolutely nothing. Well, actually, what I found was a lot of stuff I *don't*
want. There are many pages about things I might be interested in if I were a
customer. But the most important information, *the availability of the
service*, is not there. It would be great if the power never went out, but
let's face it, even with perfect equipment, someone's going to go drive into a
transformer or something that's going to disrupt service. The closest thing I
could find about service reliability on Toronto Hydro's site is to "Report a
Streetlight out". While a broken streetlight is unsafe in certain locations,
wouldn't the power being out be even worse? I guess they just figure that if
the power's out in your area, you won't be able to look up the fact that the
power's out or when it's going to be fixed.
