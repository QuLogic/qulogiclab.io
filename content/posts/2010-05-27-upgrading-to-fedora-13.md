+++
title = "Upgrading to Fedora 13"
published = 2010-05-27T22:28:00.007000-04:00
tags = ["Linux", "Fedora"]
+++

Well, this didn't go as smoothly as the live USB versions, mostly due to
[`preupgrade`](https://fedoraproject.org/wiki/PreUpgrade).

I don't have a "standard" install, you might say. This is on a laptop that's
not mine and Fedora is not the primary OS. So it's very space-constrained. I
don't even have the recommended 200MB (now the recommendation is a whopping
500MB) space for `/boot`. Personally, I think that's a ridiculous amount of
room to hold a kernel and initrd (even if they are generic), but that's the
default install size.

So the problem is that `preupgrade` downloads everything to `/boot`, but I
don't have enough room for it. The other problem is that `preupgrade` didn't
bother to tell me either. There are some bug reports for it already, but I'm
pretty sure it worked for F11-&gt;F12, so it's an annoying regression.

Fortunately, that `preupgrade` page has instructions for setting up the correct
grub command line. I had enough room for the kernel and initrd, but not the
install image. I'm not sure if it's because the downloader failed to notice it
ran out of space, but the installer wouldn't auto-download the install image. I
had to set a URL source, find a mirror, and point it to
`<mirror>/releases/13/Fedora/i386/os/`.

So I've got the installer image set up and everything's good to go, right?
Well, no. In the end, my space-constrained root didn't have enough room to
upgrade. Oh well, might as well see how nice a full install looks. I still
remember what I had installed well enough to tweak it after.

How did that go? It was pretty nice and simple, actually. The only problem I
ran into was that it didn't figure out my boot loader configuration. I don't
know why that is, but replacing it seemed to have worked. The only hiccup was I
didn't remember which partition should have been Windows. It would have been
nice if it had shown partition labels.

Anyway, after dropping stuff I didn't need (who needs all graphics drivers on a
laptop?), and installing all the stuff I could remember (like devel packages),
I ended up with 1.2G free. I even have OpenOffice installed this time. I don't
know how I managed to go from ~100M to 1.2G, but that's definitely a great
improvement.
