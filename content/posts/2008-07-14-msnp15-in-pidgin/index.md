+++
title = "MSNP15 in Pidgin"
published = 2008-07-14T03:03:00.003000-04:00
tags = ["Pidgin"]
+++

So, look at that, MSNP15 is now in [Pidgin](https://pidgin.im/)'s main branch.
Well actually, it was committed over 23 hours ago. But [unlike
John](https://theflamingbanker.blogspot.com/2008/07/state-of-msn-plugin-address.html),
I wasn't going to say it was done until the
[GObjectification](https://developer.pidgin.im/viewmtn/branch/changes/im.pidgin.gobjectification)
branch had the update as well.

While it would be great to take credit for the whole thing (but I wouldn't,
because I'm not that sort of person), there have in fact been *several*
contributors. Let's take a look at that now, shall we, with these graphs from
[ViewMTN](https://viewmtn.1erlei.de/). Yes, I realize they're kind of small,
but full size would make this really long and you can always click on them.

{{< figure src="im.pidgin.soc.2006.msnp13.png" >}}

MSNP13 started way back when on the `im.pidgin.soc.2006.msnp13` branch (though
it wasn't actually on MTN). This was done by Ma Yuan for
[GSoC](https://code.google.com/soc/2006/) in 2006. Several new and interesting
features were implemented then. Receiving OIM's was one of the first new
features. Adding contacts from the Yahoo! network was soon added, as well. One
of the most requested features, personal status messages, was added not long
after. There were also various changes made under the hood to support the new
protocol, such as SOAP processing code. Unfortunately, some of that code was in
need of cleanup, and in fact some parts went against the original MSN protocol
plugin's design. In the end, this code was not yet ready to be merged.

{{< figure src="im.pidgin.cpw.khc.msnp14.png" >}}

I don't think much happened after that until the next year.
[Richard](https://coderich.net/) and [Ka-Hing](https://www.hxbc.us/) did some
work in [ticket #148](https://developer.pidgin.im/ticket/148) to get things up
to standard. This work resulted in the new `im.pidgin.cpw.khc.msnp14` branch.
As you can tell from that graph there, it involved quite a bit of branching off
to get things cleaned up. For a month or two, this branch only really had
updates from trunk applied to it. There were some minor updates made to fix
bugs. Around May, [Stu](https://www.nosnilmot.com/) began making several
changes. These fixed a variety of bugs, and closed several tickets. Partial
updates were found to be buggy, so the full contact list would be used for the
time being.

Again, this branch sat with no commits except for propagating from trunk. There
was then yet another MSN [GSoC project for
2007](https://code.google.com/soc/2007/). This time the project was written by
Carlos Silva. Besides cleaning up the code, there were several other goals such
as direct file transfers, transfer of winks and voice clips, and the like.
There were several fixes made in this time that took care of quite a few major
bugs and tickets. In this case, the MSNP14 branch *was* propagated to trunk in
mid-September, but it did not compile by default. In this regard, it looks like
the major objective was complete, but the next biggest objective was not. Even
this merged code did not make it into the next few releases, because they were
branched from trunk earlier for some other reasons. In the background, Ka-Hing
had started work in `im.pidgin.cpw.khc.msnp14.soap` which involved much cleanup
to the SOAP code. This was merged at around November.

Some time around this point, I started hacking minor things in Pidgin. Using
[whatever documentation](https://msnpiki.msnfanatic.com/index.php/MSNP15:SSO) I
could get out of msnpiki, I [updated
libpurple](https://developer.pidgin.im/ticket/4382) to be able to login with
SSO on MSNP15. This involved a couple other
[cipher](https://developer.pidgin.im/ticket/4380)
[changes](https://developer.pidgin.im/ticket/4381) as well. While that was all
fine and dandy, it did nothing about the various other problems in the code.
Around December, [Sean](https://www.pidgin.im/%7Eseanegan/blog/) noticed these
things, and I was promoted to a "Crazy Patch Writer". While
[Gary](https://reaperworld.com/) took care of committing the cipher stuff,
Ka-Hing and I worked with my code, cleaning up some things that weren't ready
yet. Soon after, we got some help from Masca and Maiku reporting bugs and
making fixes.

{{< figure src="im.pidgin.cpw.qulogic.msn.png" >}}

At the beginning of June this year, Richard gave me push access, and I started
my own branch at `im.pidgin.cpw.qulogic.msn`. This had all the MSNP15 changes,
of course, as well as anything new I came across and decided to work on. One of
the biggest bugs was renewing ticket tokens for all the SOAP operations. It
turns out no-one had figured that out before, so I had to reverse-engineer it
myself. [oSpy](https://code.google.com/p/ospy) was a great help in achieving
this. Dimmuxx, an [Adium](https://adiumx.im/) user, also started some
[unofficial Adium
builds](https://forums.cocoaforge.com/viewtopic.php?f=10&t=17585) with code
from my branch. This turned out to be a great help in testing out the code. Not
only did Dimmuxx test out token updating by staying connected for 24+ hours,
the Adium users helped unearth quite a few bugs. If it weren't for them, I'm
sure I wouldn't have found all of them. Lately, I've been doing a lot of
cleanup. The debug messages in the MSN plugin were all prefixed with various
names, from plain old "msn", to stuff like "MSN-OIM", "msn_switchboard", to
just "msg". There was also a lot of dead code due to the MSN servers dropping
commands along the way from MSNP9 to MSNP15. Anyway, here's the final merge
(the middle hexagon), along with a propagate to the GObjectification branch to
the right.

{{< figure src="merge.png" >}}

So, with what does that leave us? Well, Pidgin now supports MSNP15, ahead of
most other MSN clients of which I know. Assuming there are no critical
problems, this code should be in 2.5.0. Of course, there are still some other
items that need to be done. Fast file transfers using direct connections
([#247](https://developer.pidgin.im/ticket/247)) do not work yet, though they
are in [msn-pecan](https://code.google.com/p/msn-pecan/) by Felipe. Winks,
sound clips, ink, and all those other items
([#393](https://developer.pidgin.im/ticket/393)) are not yet ready. There
doesn't even exist code for things like shared folders, though I am not aware
of a client that does support that fully, anyway. Nevertheless, I can't wait
for 2.5.0 to be released!
