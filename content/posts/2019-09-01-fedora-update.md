+++
title = "Fedora Update 2019 Weeks 33--34"
date = 2019-09-02T22:07:22-04:00
tags = ["Fedora"]
+++

The past two weeks have been rather simple, just catching up on the remaining
updates from release monitoring, and also those that monitoring missed. I'm
also working through some build/test failures for various reasons.

Most failures are around the Python 3.8 rebuild. Generally, upstreams are aware
of the problems, or I could have reported a bug about it. So fixing these
involve backporting fixes that are to be in the next releases. For `xtl`, I've
un-retired the package, and disabled the failing arches. I've given up on
hoping someone might figure out the gcc issue, so I'm just leaving the
arch-specific bugs ({{% rhbz 1745840 %}}, {{% rhbz 1745841 %}}) as they are.

I adopted a few *R* packages that were given up by another packager: `R-RUnit`,
`R-XML`, `R-caTools`, `R-timeDate`, and `R-xtable`.

I'm also a bit late in updating `git-lfs`, but I ran into a [test deadlock
bug](https://github.com/git-lfs/git-lfs/issues/3798). Upstream put together a
[fix](https://github.com/git-lfs/git-lfs/pull/3800) pretty quickly, so it's in
testing now. I also took advantage of upstream's new vendor customization to
add a release string to the version information:

```
$ git lfs version
git-lfs/2.8.0 (Fedora 32; linux amd64; go 1.13rc1)
```

Updated packages
================

Unfortunately, I messed up the release numbers a bit between Fedora releases
(as you can see the duplicates in the table below), but that should get cleaned
up in the next upstream version of the affected packages.

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-V8" "2.3-1" %}}
{{% fedora-update "R-chron" "2.3.54-1" %}}
{{% fedora-update "R-curl" "4.0-1" %}}
{{% fedora-update "R-dbplyr" "1.4.2-1" %}}
{{% fedora-update "R-dplyr" "0.8.3-1" %}}
{{% fedora-update "R-gargle" "0.3.1-1" "F31 and higher only" %}}
{{% fedora-update "R-ggplot2" "3.2.1-1" %}}
{{% fedora-update "R-gmailr" "1.0.0-1" "Breaking changes; F31 and higher" %}}
{{% fedora-update "R-hms" "0.5.1-1" "F31 and higher only" %}}
{{% fedora-update "R-lifecycle" "0.1.0-1" "F31 and higher only" %}}
{{% fedora-update "R-pkgbuild" "1.0.5-1" %}}
{{% fedora-update "R-progress" "1.2.2-1" %}}
{{% fedora-update "R-rematch2" "2.1.0-1" %}}
{{% fedora-update "R-rmarkdown" "1.15-1" %}}
{{% fedora-update "R-rsconnect" "0.8.15-1" %}}
{{% fedora-update "R-sys" "3.3-1" "F31 and higher only" %}}
{{% fedora-update "R-tinytest" "1.0.0-1" %}}
{{% fedora-update "R-usethis" "1.5.1-1" %}}
{{% fedora-update "R-webutils" "1.0-1" %}}
{{% fedora-update "R-xfun" "0.9-1" %}}
{{% fedora-update "git-lfs" "2.8.0-2" "F30 and below" %}}
{{% fedora-update "git-lfs" "2.8.0-4" "F31 and higher" %}}
{{% fedora-update "golang-github-dlclark-regexp2" "1.2.0-1" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.2.0-1" %}}
{{% fedora-update "golang-github-git-lfs-gitobj" "1.4.0-1" %}}
{{% fedora-update "golang-github-git-lfs-ntlm" "0-0.1.20190827gitc5056e7" %}}
{{% fedora-update "golang-github-jdkato-prose" "1.1.1-1" %}}
{{% fedora-update "golang-github-mattn-shellwords" "1.0.6-1" %}}
{{% fedora-update "golang-github-spf13-cobra" "0.0.5-1" %}}
{{% fedora-update "golang-github-stretchr-testify" "1.4.0-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.5.1-1" "F30 and below" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.5.1-3" "F31 and above" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.8-1" "F30 and below" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.8-3" "F31 and above" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.9-1" "F30 and below" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.9-3" "F31 and higher" %}}
{{% fedora-update "golang-github-tdewolff-test" "1.0.3-1" %}}
{{% fedora-update "golang-x-text" "0.3.2-5" %}}
{{% fedora-update "ocrmypdf" "9.0.1-1" "Breaking changes; F31+ only" %}}
{{% fedora-update "paperwork" "1.3.0-1" %}}
{{% fedora-update "python-geomet" "0.2.1-1" %}}
{{% fedora-update "python-geopandas" "0.5.1-1" %}}
{{% fedora-update "python-geopandas" "0.6.0-0.1.rc1"
    "Rawhide only, for testing" %}}
{{% fedora-update "python-glad" "0.1.33-1" %}}
{{% fedora-update "python-paperwork-backend" "1.3.0-1" %}}
{{% fedora-update "python-pikepdf" "1.6.1-1"
    "No Rawhide due to broken `pybind11`" %}}
{{% fedora-update "python-pikepdf" "1.6.2-1"
    "No Rawhide due to broken `pybind11`" %}}
{{% fedora-update "python-rasterio" "1.0.25-1" %}}
{{% fedora-update "python-rasterio" "1.0.26-1" "Also disables a flaky test" %}}
{{% fedora-update "python-ruffus" "2.8.3-1" %}}
{{% fedora-update "python-snuggs" "1.4.6-3" "Fix FTBFS with pyparsing 2.4" %}}
{{% fedora-update "python-tblib" "1.4.0-4" "Fix FTBFS with Python 3.8" %}}
{{% fedora-update "xtl" "0.6.5-1" %}}

New packages
============

There are only a couple of new ones, to be used by the new `R-gmailr` update.
Since that update contains breaking changes, these packages are only going in
Fedora 31, unless something else happens to need it.

* {{% rhbz 1745833 "R-gargle" %}} --- Utilities for Working with Google APIs
* {{% rhbz 1745834 "R-lifecycle" %}} --- Manage the Life Cycle of your Package
  Functions

Reviews
=======

Catching up on some *Go* reviews now that `mock` is working again.

* {{% rhbz 1743801 "golang-contrib-opencensus-exporter-aws" %}} --- OpenCensus
  Go exporters for AWS
* {{% rhbz 1743806 "golang-contrib-opencensus-integrations-o" %}} ---
  OpenCensus SQL database driver wrapper for Go
* {{% rhbz 1744238 "golang-github-frankban-quicktest" %}} --- Quick helpers for
  testing Go applications
* {{% rhbz 1744282 "libspng" %}} --- Simple, modern libpng alternative
* {{% rhbz 1744429 "waypipe" %}} --- Wayland forwarding proxy
* {{% rhbz 1746215 "golang-github-mmcloughlin-geohash" %}} --- Golang geohash
  library
* {{% rhbz 1747623 "golang-github-vultr-govultr" %}} --- Vultr Go API client
* {{% rhbz 1747624 "golang-github-nrdcg-namesilo" %}} --- Go library for
  accessing the Namesilo API
