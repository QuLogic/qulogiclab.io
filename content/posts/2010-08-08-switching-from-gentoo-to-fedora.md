+++
title = "Switching from Gentoo to Fedora"
published = 2010-08-08T22:19:00.003000-04:00
tags = ["Linux", "Gentoo", "Fedora"]
+++

Round about last week, I switched my desktop from
[Gentoo](https://www.gentoo.org/) to [Fedora](https://fedoraproject.org/). It
took a few days to get everything the way I wanted. But it only took that long
because I had lots of data to backup and (selectively) restore, and I only
worked in the evenings. Fortunately, a "re-install" is way less painful than
Windows, since all you really need to keep is your home directory. I just don't
know how Windows users live through it, especially without all their programs
in a convenient package manager. I can happily say I have *never* re-formatted
Windows for any reason (and that's not because I don't used it).

I guess there are a few reasons for the switch. I'd say it basically boils down
to the following three:

1. Gentoo wasn't updating fast enough for things I wanted. I can't say whether
   this is a general trend, but at least for packages I wanted, it was slow,
   and I could see that Fedora was getting those updates.
2. My computer is getting older and packages are getting bigger. It's not been
   too bad (except for god-awful C++ programs that use Boost), but I don't want
   to work my system so hard as it gets older.
3. And the **most** important: I got lazier. Not lazy enough to install
   [Ubuntu](https://www.ubuntu.com/) though!

I guess I'll see how well Fedora fairs. I've just got an audio bug or two to
fix (already reported) and it should be good. Eventually, I should go see if my
cx18 works, too.
