+++
title = "Fun at the GSoC Mentors Summit"
published = 2010-10-27T22:34:00-04:00
tags = ["GSoC", "Pidgin"]
+++

Last weekend was the [GSoC Mentors
Summit](https://gsoc-wiki.osuosl.org/gsoc-wiki.osuosl.org/index.php/2010.html).
As a [mentor]({{< ref "2010-05-03-mentoring-a-student-for-gsoc" >}}) for the
Pidgin, Finch and libpurple project, I attended for the first time this year.

It was pretty interesting and a lot of fun, but I have to say I didn't really
feel like much of a geek there! Everyone's either got an iPhone or a Droid, and
they've all done awesome stuff. Can you say you've worked on
[WordPress](https://wordpress.org/), used by millions of websites, or
[Apache](https://www.apache.org/), serving even more websites, or
[RTEMS](https://www.rtems.com/), running several space exploration instruments
and other consumer products, or who knows what else?

Nevertheless, it wasn't like I was totally unknown. There were quite a few
people who use Pidgin, even one or two using Finch. Oh, and some Mac users
using Adium (using libpurple), too.

The flight was fine, but the weather was a bit disappointed. It would have been
nicer to explore a bit more, but at least I got to eat a ton of chocolate. I'll
try and write a longer re-cap later.
