+++
title = "Christmas Trees"
published = 2009-12-26T23:42:00.003000-05:00
tags = ["Electronics"]
+++

So, I have this mini Christmas tree, with some fibre optics, that pulses red
and white. A while back, it stopped working and I planned to fix it, but never
got around to it. Until now!

I managed to cobble together a simple circuit based on a 555 timer. Here's a
shot of the test on the breadboard. Basically, it's the 555 in astable mode,
with a couple capacitors to smooth out the transitions.

{{< figure src="IMG_2424.JPG" >}}

The test had reds, but I put alternating red and yellow on the final circuit
because yellow looked pretty good. Each side has a different size capacitor
because it just pulses better that way. I managed to put this together on a
little board. The wires on the right go to the LEDs.

{{< figure src="IMG_2425.JPG" >}}

With a little knife-work, I managed to size it up right. The wires are a little
stiff, but a bit of jiggling got them in there.

{{< figure src="IMG_2426.JPG" >}}
