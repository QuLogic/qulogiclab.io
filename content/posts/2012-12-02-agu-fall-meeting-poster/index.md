+++
title = "AGU Fall Meeting Poster"
published = 2012-12-02T07:42:00-05:00
tags = []
+++

It's been a busy week, but my poster for AGU is finally printed and ready to
go. Here's a small copy of it:

{{< figure src="poster.png" >}}

Note, ImageMagick doesn't seem to convert to colours exactly correct, so you
may want to check out the PDF. If you want to have a look at the full PDF,
[check out the ePoster at the
AGU](http://fallmeeting.agu.org/2012/eposters/eposter/s41a-2419/). That page
has the abstract, and you can click "View ePoster" for the poster.

This poster was the result of a monster-combination of
[Scribus](http://www.scribus.net/), [LaTeX](http://www.latex-project.org/),
[GMT](http://gmt.soest.hawaii.edu/), and even a bit of
[Inkscape](https://inkscape.org/). But at least I didn't use PowerPoint! I was
planning to write some information about the process, but that's turning into a
rather large post. I want to post this one before AGU and I'll write separate
posts about the process later.
