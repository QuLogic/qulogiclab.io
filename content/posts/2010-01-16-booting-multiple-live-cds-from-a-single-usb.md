+++
title = "Booting multiple live CD's from a single USB"
published = 2010-01-16T22:34:00.012000-05:00
tags = ["Linux", "LiveCD", "Multiboot"]
+++

Recently, I got a 1TB USB drive. I was planning on using this just for backups,
but quite frankly, it's so big there's no need to limit it to just that. So I
decided I'd put a few live CD images on it somehow to get a sort of universal
test drive. I set aside about 30G which is way overkill, but in GiB, it meant
the remaining was a round 900 (darn GB vs GiB thing).

First thing's first, what to put on it? I figure
[Ubuntu](https://www.ubuntu.com/)'s a good choice for a beginner Linux that
would probably boot anywhere (or try to). Along with that, [Linux
Mint](https://www.linuxmint.com/) seems to be a great looking option, too. Plus
it's green! Also, [Fedora](https://fedoraproject.org/) since I'm using that on
the laptop. Maybe Ubuntu Netbook Remix and something similar like
[Moblin](http://moblin.org/). And since I'm a bit crazy, there'd have to be the
64-bit versions too. Finally, a few recovery things like
[GParted](https://gparted.sourceforge.io/) or similar.

Now that I have some idea what to get, I have to figure out how to get it to
boot. Since I need to get a few live CD's working at the same time, I figure I
can't just extract them at the root of the drive (like you would if you wanted
a regular LiveUSB). So the question is how I can get them to boot straight from
the ISO. First, I need a bootloader that will understand ISO files for me. I
saw a few patches to GRUB to do this, but it looks like [GRUB
2](https://www.gnu.org/software/grub/) proper also supports loading from an
ISO, though it's still experimental. Since it's unstable in Gentoo, I had to
unmask it first. I also set the `multislot` USE flag on GRUB so I wouldn't lose
the stable version.

{{< highlight "bash" >}}
> # Unmask GRUB 2
> echo "=sys-boot/grub-1.97.1 **" >> /etc/portage/package.keywords
> echo "sys-boot/grub multislot" >> /etc/portage/package.use
> emerge -va =sys-boot/grub-1.97.1
> # My USB drive is on /dev/sdf
> # and the partition is labelled "LiveLinux"
> grub2-install --no-floppy --root-directory=/media/LiveLinux /dev/sdf
{{< /highlight >}}

Next I'll go over the results of trying to get each LiveCD to boot from ISO.
