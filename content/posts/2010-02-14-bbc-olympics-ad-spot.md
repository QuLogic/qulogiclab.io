+++
title = "BBC Olympics Ad Spot"
published = 2010-02-14T22:33:00.006000-05:00
tags = ["BBC", "Ads", "Olympics"]
+++

I really like this one...

{{< youtube "E_nxcZ7Cwl8" >}}

It's all black & white and in that great sort of style like [Samurai
Jack](https://en.wikipedia.org/wiki/Samurai_Jack) (normally in colour, but
sometimes there's a black&white sequence *or* whole episode). What's also great
about the ad is that it tells a story, interweaving various winter sports into
it.

I also like the music. I've been wondering where I heard it before, and it's
actually from the beginning of [Michael Bublé](http://www.michaelbuble.com/)'s
version of Cry Me a River. I'm not actually sure how well it fits in with
*that* song, but it's great in the ad.
