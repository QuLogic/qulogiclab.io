+++
title = "Bye Bye Thesis!"
published = 2010-04-12T22:14:00.010000-04:00
tags = ["Skule", "LaTeX", "Thesis"]
+++

Soo, I guess Thesis is finally done!

Last Friday I handed it in to the EngSci Office to get put in the "special"
envelope, and today I dropped it off to my professor. Here it is in its
wrapped-up glory (which is, in all honesty, not that exciting.)

{{< figure src="IMG_2618a.JPG" >}}

Silly me, though, I didn't take a picture of the final blue-cover version. It
must have been the relief of getting it printed and bound with 5 minutes to go.
Maybe I can get some 1T1 to borrow it from the library some time.

Now, I guess I'd have to say, in all honesty, it probably doesn't say much,
really. Sure, I managed some results, but they *definitely* could have been
better. *However*, one thing that I do know, is that it *looks* awesome thanks
to [LaTeX](http://www.latex-project.org/). Just check out this lovely (inside)
title page and first chapter (fonts are slightly off since I used
[convert](https://imagemagick.org/script/convert.php) to make these PNGs):

{{< figure src="thesis1-1.png" >}}
{{< figure src="thesis1-8.png" >}}
