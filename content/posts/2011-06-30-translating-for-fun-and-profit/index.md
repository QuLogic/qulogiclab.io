+++
title = "Translating for Fun and Profit"
published = 2011-06-30T16:58:00-04:00
tags = ["MyPaint", "Open Source", "Translation"]
+++

Okay, so maybe I'm not entirely sure about the second one, but it certainly
could be fun depending on your interests. Writing translations is probably one
of the simplest ways to contribute to an open-source project, especially if you
don't know/like coding.

There are a couple of things you'll need to do:

1. Get the stuff to translate.
2. Translate the stuff.
3. Send it back and get it merged.

You'll also need some software. I leave it up to you to figure out how to
install it, if you need it (it's easy in any Linux distro!)

1. I'm going to use [git](https://git-scm.com/), although it may not be
   necessary depending on the project.
2. A translation editor. You can use any plain text editor, but using
   translation software provides a few conveniences.

Choosing a Project
------------------

No matter your likes, it shouldn't be too difficult to find some open source
software that you find interesting. Just take a look on
[SourceForge](https://sourceforge.net/) or in any
[Linux](https://apps.fedoraproject.org/packages/)
[package](https://www.debian.org/distrib/packages)
[database](https://packages.ubuntu.com/). Of course, you'll also need to know
another language, especially the technical terms, which may not be so common.

For some reason or other, I recently installed [MyPaint](http://mypaint.org/).
It's pretty fun to draw stuff, though I'm not yet familiar with all the
brushes. And it would be easier if I had a tablet instead of a mouse!

Getting the Text
----------------

Obviously, the first thing you need is the text that needs to be translated.
MyPaint, like many other open-source projects, uses [GNU
gettext](https://www.gnu.org/software/gettext/) to translate from one locale to
another. A locale defines the language and country. For example, "pt" is
Portuguese, while "pt_BR" is Brazilian Portuguese.

MyPaint uses git on [Gitorious](https://www.gitorious.org/) which makes it
pretty easy to get what you need to work on a translation. (Maybe not as easy
as translating on [LaunchPad](https://translations.launchpad.net/) or
[Transifex](https://www.transifex.com/), but I can't let you go off without
learning something).

There are two ways to get the MyPaint source: via git, or via the source
package. The MyPaint developers prefer git merge requests, but they'll also
accept whole translations directly from the package.

To go the first route, you'll need an account on Gitorious (you can login with
OpenID or various other accounts.) Once that's done, you can go to the [MyPaint
git page](https://www.gitorious.org/mypaint/mypaint). Click the `Clone
Repository` button (see screenshot).

{{< figure src="mypaint-repo.png" >}}

You will then have a *personal* copy of the code on Gitorious, where you can
save all your changes. Using the personal clone URL, you can get a copy of
everything with the following command (use your own URL):

> git clone git://gitorious.org/~qulogic/mypaint/qulogic-mypaint.git

This will give you a `mypaint` directory with the source of MyPaint.

Make the Translation
--------------------

In MyPaint, the translations are in the `po` directory, which is the common
location for many gettext-based projects. These files (`*.po`) are pretty
simple, with a small header at the top describing the language and then the
translations. Each translation is given by a `msgid` and a `msgstr` which are
the original and translated strings, respectively. MyPaint includes a couple
commands to start you off.

> scons translate=pot  
> scons translate=&lt;language code&gt;

You'll have to find the [correct code for your
language](https://www.gnu.org/software/gettext/manual/gettext.html#Language-Codes)
from gettext. Once you run the above commands, you will get a `<language
code>.po` file into which the translations will go. First, fill out the
translation information at the top of the file for the new language. Then just
write out the translations of the `msgid` in the `msgstr` sections. Of course,
if you have a translation editor, then it'll indicate what you've translated so
far, etc., and generally make things easier.

Merging the Translation
-----------------------

Once you've finished writing the translation, there are a couple more steps to
getting it merged into the project. Now, assuming you're using git like I
started above, you'll need to do the following:

1. Commit the translations to your repository.
2. Push the changes to gitorious.
3. Request a merge from the original project.

### Committing translations

The first thing after writing your translation is to commit the result to your
repository. I'm not going to explain all of git for you, but basically, as a
distributed version control system, you commit changes to your own copy and can
share that with everyone else. To commit your changes, you first 'add' the
changed files and then 'commit' them.

> git add &lt;language code&gt;.po  
> git commit

### Push changes

Now that git has saved your changes as a commit, you need to send those changes
to gitorious. This is a simple command to push changes to a remote repository.

> git push

### Request a Merge

Now that the code is on gitorious, you can request a merge from the upstream
project. On gitorious, you just have to click the `Request merge` button on the
right side.

{{< figure src="mypaint-merge.png" >}}

You will get to a page where you can fill in what your changes entail. Just
fill in the fields, follow the instructions there, and send off the request.

Now it's just a matter of waiting for [your
request](https://www.gitorious.org/mypaint/mypaint/merge_requests/5) to be
merged!
