+++
title = "Skule Nite 0T9"
published = 2009-03-22T23:26:00.002000-04:00
tags = ["Skule"]
+++

Yes, another year for Skule Nite... And again, quite fun as usual.

The big acts were mostly all based on Disney movie songs. Though I'm not too
sure where the last few were from. But no matter, "Google Man" was still a
great skit. The "Lord of the ECF" were great, and the bits on 24 were funny,
too. At first, I didn't really like the Obama skit (probably because I didn't
recognize the song), but the 4 Years skit was a spot on parody of 4 Minutes.

But of course, the best part of the show was the Vision Quest with the great
blacklighting effects. People totally freaked out when that stick-man started
moving (I kind of figured it would happen, though). They definitely managed to
top last year's blacklight sketch. This part alone was worth the admission.
:wink:

You know, I saw they were filming it; I wonder what they actual do with that.

So yep, definitely another great year. But I do have one question: where is the
EngSci club hiding that jacuzzi?
