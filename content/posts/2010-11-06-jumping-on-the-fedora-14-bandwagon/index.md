+++
title = "Jumping on the Fedora 14 bandwagon"
published = 2010-11-06T02:11:00.006000-04:00
tags = ["Linux", "Fedora"]
+++

Fedora 14 was released just a day or two ago. I figured I'd be cutting-edge and
upgrade on pretty much the day it came out.

Of course, as with [last time]({{< ref "2010-05-27-upgrading-to-fedora-13"
>}}), I'm trying to do so on a laptop with little room for any more stuff. This
time though, I tried as hard as I could to get
[preupgrade](https://fedoraproject.org/wiki/PreUpgrade) to work instead of
doing a new install.

The Free Space Issue
--------------------

### /boot

The first issue was, of course, the lack of free space, on both `/boot` and
`/`. `/boot` is slightly less than 100MB, meaning I wouldn't be able to fit the
install image on it. Fortunately this time, it correctly realized I didn't have
room, and set up anaconda to download it after booting.

### /var/cache/yum

The RPMs get downloaded to `/var/cache/yum` before being installed, but I
didn't really have enough room for that. What I did have was a 4GB USB key. So
I edited `/etc/fstab` to overlay the key there. I was a bit concerned that
anaconda wouldn't mount it when I rebooted, but it did so correctly, so I
didn't run into any trouble there.

Now, preupgrade seems to download a separate directory for each repository,
then link the files into one large thing. Since the USB key was FAT32, it
couldn't really do that linking. Since preupgrade is written in Python, it was
easy for me to hack it to copy instead of linking. That was a bit slower and
naturally took up double the space, but there was enough on the key for it.

### /usr

But then there's the more pressing issue of the space on `/usr`.

{{< figure src="Screenshot.png" >}}

Clearly I'm really pushing it on the free space front here. With the help of
[Baobab](https://help.gnome.org/users/baobab/stable/), it was pretty clear
where I could find the most "dead" space. These locale files are huge, and a
pretty small portion is actually in my usual locale, en_CA. So of course, that
was the first thing I axed.

{{< figure src="Screenshot-1.png" >}}

Everything seemed peachy, but it turns out that {{% rhbz 629814
"preupgrade sucks at estimating required disk space" %}}. I then removed
OpenOffice and kdelibs, and anaconda stopped complaining. However, it *still*
ran out of space, though I managed to clean up some stuff and restart it.

The Download Issue
------------------

Preupgrade indicates that it will resume from where it was if you cancel it.
While this may be true of the RPMs, it's not of the metadata, kernel image, or
installer image. I don't really know why it needs to update the metadata all
the time, and I don't really know what it is. But I do know it shouldn't have
to download the image every time. The {{% rhbz 626767 "problem" %}} is that it
wants to check the file size, and then downloads the whole file just to do so.
This is to ensure that you have enough space to hold the images on `/boot`. So
if there's enough room, it downloads the file again to place it on the drive!

Since I didn't have enough room on `/boot`, it had to download the installer
image whenever I started upgrade process. Obviously, I didn't want to do that
too many times after it failed the first time. So I downloaded the image and
placed it on the USB key. Then it was a simple matter of modifying the
kickstart file in `/boot` just before rebooting so that it would specify the
right location. The anaconda installer took care of the rest.

The Cleanup
-----------

After rebooting, yum thought I had several fc13 packages still installed. I
tried rebuilding the database but that really didn't help. I tried writing a
small script to pick out the extras and remove them, but then I realized that
`package-cleanup` would do that and do it better. A run with `--cleandupes`
took care of the problem. It also took care of some phantom missing storage
space.

Why bother?
-----------

So I'm sure you're all wondering why I even bothered, instead of just
installing fresh. I'll admit that would have been easier, as with last time.
Mostly, I just wanted to see if I could. But arguably more importantly, I was
able to see whether I could do the upgrade via a USB key.

In other words, one could get preupgrade to download everything to the USB key
and install from there. It wouldn't be too difficult, probably just requiring
an option to tell anaconda where the package cache was. It may already exist in
the kickstart configuration, but I haven't looked into it. You can already
place the installer image on a USB key, as I did above.
