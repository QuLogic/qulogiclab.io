+++
title = "Look! There! A New Graduate!"
published = 2010-06-16T16:32:00.003000-04:00
tags = ["Skule"]
+++

So right about now, I should have just received my diploma. Which means this
post is coming to you via a new-fangled technology that creates posts directly
from my brain.

Or, well, maybe that's not a reality just yet. It's more of a thing called
scheduled posting.

Anyway, back to the real topic. Convocation is today! Four + PEY years of
studying and sleeplessness have come to an end. Sure, it was painful at times,
but I still think it was a lot of fun. But now, it's time to move onward to
bigger and better things, I hope.

We'll just have to see whether that comes true, won't we...
