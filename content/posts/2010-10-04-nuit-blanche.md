+++
title = "Nuit Blanche"
published = 2010-10-04T22:28:00.003000-04:00
tags = ["Toronto"]
+++

I probably should have gotten around to finishing this post a while ago, but
anyway...

So [Nuit Blanche](http://scotiabanknuitblanche.ca/) was this weekend. I don't
think it was as fun this year as the last time. Of course, the cold was not
much fun, but the other time was a bit chilly, too.

I think the main problem was that we didn't plan a route and basically went to
whatever seemed close to us. Which turned out to be the more "popular" ones,
i.e., those in the downtown core. And it turns out those are not necessarily
the best.

Take for example, the synchronizing bug light thing. Strings of lights that
supposedly sync up and can be perturbed by the audience. Except it didn't seem
to do much, and I've seen it [done
better](https://tinkerlog.com/2008/07/27/synchronizing-fireflies-ng/).

I think the biggest problem in such crowded areas is the high chance of running
into smokers. Obviously, since I don't smoke, I'm not really a fan of
second-hand smoke either. And there was way too much of it. That and smoke from
stuff that's not a cigarette, if you know what I mean. :wink:
