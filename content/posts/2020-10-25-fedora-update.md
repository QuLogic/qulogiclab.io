+++
title = "Fedora Update Weeks 25--42"
date = 2020-08-25T03:04:54-04:00
tags = ["Fedora"]
+++

With Fedora 33 just around the corner (and such a long time since the last
update), I thought it would be best to post a new update today. Since the last
update, there has been a Mass Rebuild for Fedora 33, both Beta and Final
Freezes, and Fedora 33 will be officially out tomorrow. 

The Mass Rebuild introduced many failures, but several of these were transient.
There was a break in `gcc`/`annobin`, which once fixed, meant a simple rebuild
afterwards fixed most of my packages. I think about 75% of all my failures were
fixed this way. A small number of my packages were affected by the [CMake macro
Change](https://fedoraproject.org/wiki/Changes/CMake_to_do_out-of-source_builds),
but these were easily fixed (noted below).

Another major Change in Fedora 33 is the enabling of [Link Time Optimization by
default](https://fedoraproject.org/wiki/LTOByDefault). Overall, this did not
cause too much trouble for me. The only package which required me to disable
LTO was `python-pyfastnoisesimd`, and because of which I found and reported a
{{% rhbz 1877652 "bug in the way LTO flags are saved in RPM macros" %}}.

In the end, it came a bit close to the wire, but I was able to fix all FTBFS
packages before Fedora 33 was released. In two cases (`python-pyfastnoisesimd`
and `tinygo`), I had to skip tests on i686, but I don't think this will be a
big deal. In one more case (`python-pypillowfight`), I had to skip some tests
until upstream can make their own fixes. Again, `tinygo` was a larger
endeavour, requiring patched support for both *Go* 1.15 and LLVM 11.
Fortunately, upstream and I were able to get this done some time before Fedora
33 final.

Aside from fixing FTBFS, due to a request on the mailing lists, I've added
explicit `BuildRequires` on `setuptools` to `python-click-plugins`,
`python-descartes`, `python-heapdict`, `python-kiwisolver`,
`python-octave-kernel`, `python-partd`, `python-pep8-naming`, `python-tblib`,
and `python-zict`, as well as minor cleanup to those files. They have not been
rebuilt though, and likely won't until they need to be (via updates or Mass
Rebuilds).

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-AsioHeaders" "1.16.1-1" %}}
{{% fedora-update "R-Cairo" "1.5.12.2-1" %}}
{{% fedora-update "R-DT" "0.16-1" %}}
{{% fedora-update "R-IRkernel" "1.1.1-1" %}}
{{% fedora-update "R-R.methodsS3" "1.8.1-1" %}}
{{% fedora-update "R-R.oo" "1.24.0-1" %}}
{{% fedora-update "R-R.rsp" "0.44.0-1" %}}
{{% fedora-update "R-R.utils" "2.10.1-1" %}}
{{% fedora-update "R-RPostgres" "1.2.1-1" %}}
{{% fedora-update "R-RcppCCTZ" "0.2.8-1" %}}
{{% fedora-update "R-RcppCCTZ" "0.2.9-1" %}}
{{% fedora-update "R-V8" "3.2.0-3" "Fix checks for pointer compression" %}}
{{% fedora-update "R-XML" "3.99.0.5-1" %}}
{{% fedora-update "R-ape" "5.4-1" %}}
{{% fedora-update "R-ape" "5.4.1-1" %}}
{{% fedora-update "R-ascii" "2.3-1" %}}
{{% fedora-update "R-ascii" "2.4-1" %}}
{{% fedora-update "R-backports" "1.1.10-1" %}}
{{% fedora-update "R-backports" "1.1.8-1" %}}
{{% fedora-update "R-backports" "1.1.9-1" %}}
{{% fedora-update "R-bit" "4.0.3-1" %}}
{{% fedora-update "R-bit" "4.0.4-1" %}}
{{% fedora-update "R-bit64" "0.9.7.1-1" %}}
{{% fedora-update "R-bit64" "4.0.2-1" %}}
{{% fedora-update "R-bit64" "4.0.4-1" %}}
{{% fedora-update "R-bit64" "4.0.5-1" %}}
{{% fedora-update "R-bookdown" "0.21-1" %}}
{{% fedora-update "R-broom" "0.7.0-1" %}}
{{% fedora-update "R-broom" "0.7.1-1" %}}
{{% fedora-update "R-broom" "0.7.2-1" %}}
{{% fedora-update "R-callr" "3.4.4-1" %}}
{{% fedora-update "R-callr" "3.5.0-1" %}}
{{% fedora-update "R-callr" "3.5.1-1" %}}
{{% fedora-update "R-chron" "2.3.56-1" %}}
{{% fedora-update "R-cli" "2.1.0-1" %}}
{{% fedora-update "R-cliapp" "0.1.1-1" %}}
{{% fedora-update "R-clipr" "0.7.1-1" %}}
{{% fedora-update "R-coda" "0.19.4-1" %}}
{{% fedora-update "R-covr" "3.5.1-1" %}}
{{% fedora-update "R-cpp11" "0.2.1-1" %}}
{{% fedora-update "R-cpp11" "0.2.2-1" %}}
{{% fedora-update "R-cpp11" "0.2.3-1" %}}
{{% fedora-update "R-data.table" "1.13.0-1" %}}
{{% fedora-update "R-data.table" "1.13.0-3"
    "Fix FTBFS by avoiding `/etc/localtime` not being a symlink in koji" %}}
{{% fedora-update "R-data.table" "1.13.2-1" %}}
{{% fedora-update "R-dbplyr" "1.4.4-1" %}}
{{% fedora-update "R-deldir" "0.1.28-1" %}}
{{% fedora-update "R-deldir" "0.1.29-1" %}}
{{% fedora-update "R-devtools" "2.3.1-1" %}}
{{% fedora-update "R-devtools" "2.3.2-1" %}}
{{% fedora-update "R-diffobj" "0.3.2-1" %}}
{{% fedora-update "R-doParallel" "1.0.16-1" %}}
{{% fedora-update "R-downlit" "0.2.0-1" %}}
{{% fedora-update "R-dplyr" "1.0.2-1" %}}
{{% fedora-update "R-foghorn" "1.2.3-1" %}}
{{% fedora-update "R-foghorn" "1.3.1-1" %}}
{{% fedora-update "R-foreach" "1.5.1-1" %}}
{{% fedora-update "R-fs" "1.4.2-1" %}}
{{% fedora-update "R-fs" "1.5.0-1" %}}
{{% fedora-update "R-future" "1.18.0-1" %}}
{{% fedora-update "R-future" "1.19.1-1" %}}
{{% fedora-update "R-gamlss.dist" "5.1.7-1" %}}
{{% fedora-update "R-ggplot2" "3.3.2-1" %}}
{{% fedora-update "R-gh" "1.1.0-1" %}}
{{% fedora-update "R-globals" "0.13.0-1" %}}
{{% fedora-update "R-globals" "0.13.1-1" %}}
{{% fedora-update "R-glue" "1.4.2-1" %}}
{{% fedora-update "R-gplots" "3.0.4-1" %}}
{{% fedora-update "R-gplots" "3.1.0-1" %}}
{{% fedora-update "R-haven" "2.3.1-1" "Fedora 33+ only" %}}
{{% fedora-update "R-htmltools" "0.5.0-1" %}}
{{% fedora-update "R-httpuv" "1.5.4-3"
    "Fix websocket handshake on big-endian systems" %}}
{{% fedora-update "R-httr" "1.4.2-1" %}}
{{% fedora-update "R-igraph" "1.2.6-1" %}}
{{% fedora-update "R-import" "1.2.0-1" %}}
{{% fedora-update "R-iterators" "1.0.13-1" %}}
{{% fedora-update "R-jsonlite" "1.7.0-1" %}}
{{% fedora-update "R-jsonlite" "1.7.1-1" %}}
{{% fedora-update "R-knitr" "1.29-1" %}}
{{% fedora-update "R-knitr" "1.30-1" %}}
{{% fedora-update "R-labeling" "0.4.2-1" %}}
{{% fedora-update "R-lmtest" "0.9.38-1" %}}
{{% fedora-update "R-lokern" "1.1.8.1-1" %}}
{{% fedora-update "R-lubridate" "1.7.9-1" %}}
{{% fedora-update "R-magick" "2.5.0-1" %}}
{{% fedora-update "R-mnormt" "2.0.1-1" %}}
{{% fedora-update "R-mnormt" "2.0.2-1" %}}
{{% fedora-update "R-nanotime" "0.3.2-1" %}}
{{% fedora-update "R-openssl" "1.4.2-1" %}}
{{% fedora-update "R-openssl" "1.4.3-1" %}}
{{% fedora-update "R-pdftools" "2.3.1-4" "Re-enable full checks" %}}
{{% fedora-update "R-pillar" "1.4.6-1" %}}
{{% fedora-update "R-pillar" "1.4.6-2" "Re-enable checks" %}}
{{% fedora-update "R-pingr" "2.0.1-1" %}}
{{% fedora-update "R-pkgbuild" "1.1.0-1" %}}
{{% fedora-update "R-pkgcache" "1.1.1-1" %}}
{{% fedora-update "R-pkgdown" "1.6.1-1" %}}
{{% fedora-update "R-prettydoc" "0.4.0-1" %}}
{{% fedora-update "R-processx" "3.4.3-1" %}}
{{% fedora-update "R-processx" "3.4.4-1" %}}
{{% fedora-update "R-profvis" "0.3.6-6" "Re-bundle js-jquery1" %}}
{{% fedora-update "R-ps" "1.3.4-1" %}}
{{% fedora-update "R-ps" "1.4.0-1" %}}
{{% fedora-update "R-readr" "1.4.0-1" %}}
{{% fedora-update "R-remotes" "2.2.0-1" %}}
{{% fedora-update "R-rgdal" "1.5.12-3" "Backport fix for `CXXFLAGS`" %}}
{{% fedora-update "R-rgdal" "1.5.15-1" %}}
{{% fedora-update "R-rgdal" "1.5.16-1" %}}
{{% fedora-update "R-rgdal" "1.5.17-1" %}}
{{% fedora-update "R-rgdal" "1.5.18-1" %}}
{{% fedora-update "R-rgeos" "0.5.5-1" %}}
{{% fedora-update "R-rlang" "0.4.7-1" %}}
{{% fedora-update "R-rlang" "0.4.8-1" %}}
{{% fedora-update "R-rmarkdown" "2.3-1"
    "Fix *jQuery* bundling and font unbundling; switch `ycssmin` to `gominify`"
    %}}
{{% fedora-update "R-rmarkdown" "2.4-1" %}}
{{% fedora-update "R-roxygen2" "7.1.1-1" %}}
{{% fedora-update "R-rsvg" "2.1-4"
    "Enable full checks with now-available `magick`" %}}
{{% fedora-update "R-rvest" "0.3.6-1" %}}
{{% fedora-update "R-servr" "0.19-1" %}}
{{% fedora-update "R-servr" "0.20-1" %}}
{{% fedora-update "R-shiny" "1.4.0.2-3" "Re-bundle `showdown`" %}}
{{% fedora-update "R-shiny" "1.5.0-1" %}}
{{% fedora-update "R-showtext" "0.9-1" %}}
{{% fedora-update "R-showtextdb" "3.0-1" %}}
{{% fedora-update "R-sp" "1.4.4-1" %}}
{{% fedora-update "R-spelling" "2.2-1" %}}
{{% fedora-update "R-stringdist" "0.9.6-1" %}}
{{% fedora-update "R-stringdist" "0.9.6.3-1" %}}
{{% fedora-update "R-stringi" "1.5.3-1" %}}
{{% fedora-update "R-svglite" "1.2.3.2-1" %}}
{{% fedora-update "R-systemfonts" "0.3.0-1" %}}
{{% fedora-update "R-systemfonts" "0.3.1-1" %}}
{{% fedora-update "R-systemfonts" "0.3.2-1" %}}
{{% fedora-update "R-testit" "0.12-1" %}}
{{% fedora-update "R-tibble" "3.0.3-1" %}}
{{% fedora-update "R-tibble" "3.0.4-1" %}}
{{% fedora-update "R-tidyr" "1.1.1-1" %}}
{{% fedora-update "R-tidyr" "1.1.2-1" %}}
{{% fedora-update "R-tikzDevice" "0.12.3.1-1" %}}
{{% fedora-update "R-tinytest" "1.2.2-1" %}}
{{% fedora-update "R-tinytest" "1.2.3-1" %}}
{{% fedora-update "R-tinytex" "0.25-1" %}}
{{% fedora-update "R-tinytex" "0.26-1" %}}
{{% fedora-update "R-tufte" "0.7-1" %}}
{{% fedora-update "R-usethis" "1.6.1-1" %}}
{{% fedora-update "R-usethis" "1.6.3-1" %}}
{{% fedora-update "R-vctrs" "0.3.2-1" %}}
{{% fedora-update "R-vctrs" "0.3.2-2" "Re-enable checks" %}}
{{% fedora-update "R-vctrs" "0.3.3-1" %}}
{{% fedora-update "R-vctrs" "0.3.4-1" %}}
{{% fedora-update "R-waldo" "0.2.1-1" %}}
{{% fedora-update "R-waldo" "0.2.2-1" %}}
{{% fedora-update "R-websocket" "1.3.1-1" %}}
{{% fedora-update "R-withr" "2.3.0-1" %}}
{{% fedora-update "R-xfun" "0.16-1" %}}
{{% fedora-update "R-xfun" "0.18-1" %}}
{{% fedora-update "cppzmq" "4.6.0-3" "Fix FTBFS due to CMake change" %}}
{{% fedora-update "cppzmq" "4.7.1-1" %}}
{{% fedora-update "fzf" "0.22.0-1" %}}
{{% fedora-update "fzf" "0.23.0-1" %}}
{{% fedora-update "fzf" "0.23.1-1" %}}
{{% fedora-update "git-lfs" "2.12.0-1"
    "Add option to not modify the git filter config; remove redundant `docs/man` directory" %}}
{{% fedora-update "golang-bug-serial-1" "1.1.1-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.11-1" %}}
{{% fedora-update "golang-github-daviddengcn-villa" "0-0.5.20190712git3f35da8"
    "Add patch to fix build with *Go* 1.15" %}}
{{% fedora-update "golang-github-daviddengcn-villa" "0-0.6.20200812git68107af" %}}
{{% fedora-update "golang-github-fsnotify" "1.4.9-1" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.3.0-4"
    "Add explicit `ncurses` dependency" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.4.0-1" %}}
{{% fedora-update "golang-github-gobwas-ws" "1.0.4-1" %}}
{{% fedora-update "golang-github-hashicorp-version" "1.2.1-1" %}}
{{% fedora-update "golang-github-marcinbor85-gohex" "0-0.6.20200729gitbaab252" %}}
{{% fedora-update "golang-github-nicksnyder-i18n-2" "2.1.1-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "1.3.0-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "1.3.1-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "1.3.2-1" %}}
{{% fedora-update "golang-github-sanity-io-litter" "1.3.0-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.4.4-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.8.0-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.0-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.1-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.3-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.4-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.5-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.7-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.9.9-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.0-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.1-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.2-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.3-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.4-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.5.5-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.7.7-1" %}}
{{% fedora-update "golang-github-willf-bitset" "1.1.11-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.33-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.2.0-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.2.1-1" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.12.20200818git345b294" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.13.20200818git345b294"
    "Simplify LLVM dependency" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.14.20200919git70c5585" %}}
{{% fedora-update "htmltest" "" %}}
{{% fedora-update "libinsane" "1.0.7-1" %}}
{{% fedora-update "libinsane" "1.0.8-1" %}}
{{% fedora-update "libxls" "1.5.3-1" %}}
{{% fedora-update "libxls" "1.6.1-1" %}}
{{% fedora-update "mingw-json-glib" "1.5.2-1" %}}
{{% fedora-update "mingw-json-glib" "1.6.0-1" %}}
{{% fedora-update "ocrmypdf" "10.2.0-1" "Fedora 33+ only" %}}
{{% fedora-update "ocrmypdf" "10.3.0-1" %}}
{{% fedora-update "ocrmypdf" "10.3.1-1" %}}
{{% fedora-update "ocrmypdf" "10.3.3-1" %}}
{{% fedora-update "ocrmypdf" "11.0.1-1" %}}
{{% fedora-update "ocrmypdf" "11.0.2-1" %}}
{{% fedora-update "ocrmypdf" "11.1.0-1" %}}
{{% fedora-update "ocrmypdf" "11.1.1-1" %}}
{{% fedora-update "ocrmypdf" "11.1.2-1" %}}
{{% fedora-update "ocrmypdf" "11.2.1-1" %}}
{{% fedora-update "ocrmypdf" "9.8.2-1" %}}
{{% fedora-update "python-cligj" "0.6.0-1" "Enable tests; fix license" %}}
{{% fedora-update "python-cligj" "0.7.0-1" %}}
{{% fedora-update "python-contextily" "1.0.1-1" %}}
{{% fedora-update "python-dask" "2.20.0-1" %}}
{{% fedora-update "python-dask" "2.21.0-1" %}}
{{% fedora-update "python-dask" "2.22.0-1" %}}
{{% fedora-update "python-dask" "2.23.0-1" %}}
{{% fedora-update "python-dask" "2.24.0-1" %}}
{{% fedora-update "python-dask" "2.25.0-1" %}}
{{% fedora-update "python-dask" "2.26.0-1" %}}
{{% fedora-update "python-dask" "2.27.0-1" %}}
{{% fedora-update "python-dask" "2.28.0-1" %}}
{{% fedora-update "python-dask" "2.30.0-1" %}}
{{% fedora-update "python-fsspec" "0.8.0-1" %}}
{{% fedora-update "python-fsspec" "0.8.1-1" %}}
{{% fedora-update "python-fsspec" "0.8.2-1" %}}
{{% fedora-update "python-fsspec" "0.8.3-1" %}}
{{% fedora-update "python-geopandas" "0.8.0-1" %}}
{{% fedora-update "python-geopandas" "0.8.1-1" %}}
{{% fedora-update "python-geopandas" "0.8.1-2"
    "Fix alternate architecture patch" %}}
{{% fedora-update "python-glad" "0.1.34-1" %}}
{{% fedora-update "python-gpxpy" "1.4.2-1" %}}
{{% fedora-update "python-jupyter-console" "6.2.0-1" %}}
{{% fedora-update "python-libpysal" "4.3.0-1" %}}
{{% fedora-update "python-mapclassify" "2.3.0-1" %}}
{{% fedora-update "python-matplotlib" "3.2.2-1" %}}
{{% fedora-update "python-matplotlib" "3.3.0-1" "Fedora 33+ only" %}}
{{% fedora-update "python-matplotlib" "3.3.1-1" "Fedora 33+ only" %}}
{{% fedora-update "python-matplotlib" "3.3.2-1" "Fedora 33+ only" %}}
{{% fedora-update "python-mercantile" "1.1.5-1" %}}
{{% fedora-update "python-mercantile" "1.1.6-1" %}}
{{% fedora-update "python-mplcairo" "0.3-2"
    "Update macros; backport fixes for new Matplotlib" %}}
{{% fedora-update "python-mplcursors" "0.3-5"
    "Backport fixes to work with Matplotlib 3.3" %}}
{{% fedora-update "python-numcodecs" "0.7.0-1" %}}
{{% fedora-update "python-numcodecs" "0.7.1-1" %}}
{{% fedora-update "python-numcodecs" "0.7.2-1" %}}
{{% fedora-update "python-pep8-naming" "0.11.1-1" %}}
{{% fedora-update "python-pikepdf" "1.15.1-1" %}}
{{% fedora-update "python-pikepdf" "1.16.1-1" %}}
{{% fedora-update "python-pikepdf" "1.17.0-1" %}}
{{% fedora-update "python-pikepdf" "1.17.1-1" %}}
{{% fedora-update "python-pikepdf" "1.17.2-1" %}}
{{% fedora-update "python-pikepdf" "1.17.3-1" %}}
{{% fedora-update "python-pikepdf" "1.18.0-1" %}}
{{% fedora-update "python-pikepdf" "1.19.0-1" %}}
{{% fedora-update "python-pikepdf" "1.19.1-1" %}}
{{% fedora-update "python-pikepdf" "1.19.3-1" %}}
{{% fedora-update "python-pycountry" "20.7.3-1" %}}
{{% fedora-update "python-pyshtools" "4.6.2-4" %}}
{{% fedora-update "python-pytest-tornado" "0.8.1-1" %}}
{{% fedora-update "python-rasterio" "1.1.8-1" %}}
{{% fedora-update "python-xarray" "0.15.1-3"
    "Fix FTBFS in tests and alternate architectures" %}}
{{% fedora-update "python-xarray" "0.16.0-1" %}}
{{% fedora-update "python-xarray" "0.16.1-1" %}}
{{% fedora-update "python-xmp-toolkit" "2.0.1-8"
    "Backport fixed `GIF89a` test file for new exempi" %}}
{{% fedora-update "python-zarr" "2.5.0-1" %}}
{{% fedora-update "tinygo" "0.13.1-3" "Add patch allowing *Go* 1.15" %}}
{{% fedora-update "tinygo" "0.15.0-1" %}}
{{% fedora-update "visidata" "2.0.1-1" "Rawhide only" %}}
{{% fedora-update "xeus" "0.24.0-1" %}}
{{% fedora-update "xeus" "0.24.1-1" %}}
{{% fedora-update "xeus" "0.24.1-3" "Fix FTBFS due to CMake change" %}}
{{% fedora-update "xeus" "0.24.2-1" %}}
{{% fedora-update "xtl" "0.6.15-1" %}}
{{% fedora-update "xtl" "0.6.16-1" %}}
{{% fedora-update "xtl" "0.6.17-1" %}}
{{% fedora-update "xtl" "0.6.18-1" %}}
{{% fedora-update "xtl" "0.6.19-1" %}}
{{% fedora-update "xtl" "0.6.20-1" %}}

New packages
============

* {{% rhbz 1839451 "R-servr" %}} --- Simple HTTP Server to Serve Static Files
  or Dynamic Documents
* {{% rhbz 1839456 "R-filelock" %}} --- Portable File Locking
* {{% rhbz 1839474 "R-DBItest" %}} --- Testing DBI Backends
* {{% rhbz 1862855 "R-crosstalk" %}} --- Inter-Widget Interactivity for HTML
  Widgets
* {{% rhbz 1862859 "R-DT" %}} --- Wrapper of the JavaScript Library
  'DataTables'
* {{% rhbz 1862862 "R-formattable" %}} --- Create 'Formattable' Data Structures
* {{% rhbz 1862881 "R-covr" %}} --- Test Coverage for Packages
* {{% rhbz 1862895 "R-magick" %}} --- Advanced Graphics and Image-Processing in
  R
* {{% rhbz 1862902 "R-tesseract" %}} --- Open Source OCR Engine
* {{% rhbz 1862919 "R-isoband" %}} --- Generate Isolines and Isobands from
  Regularly Spaced Elevation Grids
* {{% rhbz 1865739 "R-profmem" %}} --- Simple Memory Profiling for R
* {{% rhbz 1866143 "R-RPostgres" %}} --- Rcpp Interface for PostgreSQL
* {{% rhbz 1866165 "R-bookdown" %}} ---  Authoring Books and Technical
  Documents with R Markdown
* {{% rhbz 1866166 "R-keyring" %}} --- Access the System Credential Store from
  R
* {{% rhbz 1867329 "R-RcppDate" %}} --- 'date' C++ Header Library for Date and
  Time Functionality
* {{% rhbz 1867353 "R-RMariaDB" %}} --- Database Interface and 'MariaDB' Driver
* {{% rhbz 1867407 "R-tmvnsim" %}} --- Truncated Multivariate Normal Simulation
* {{% rhbz 1867423 "R-ragg" %}} --- Graphic Devices Based on AGG
* {{% rhbz 1867464 "R-cpp11" %}} --- A C++11 Interface for R's C Interface
* {{% rhbz 1869164 "R-odbc" %}} --- Connect to ODBC Compatible Databases (using
  the DBI Interface)
* {{% rhbz 1873772 "R-waldo" %}} --- Find Differences Between R Objects
* {{% rhbz 1875273 "golang-github-git-lfs-gitobj-2" %}} --- Gitobj reads and
  writes Git objects
* {{% rhbz 1876184 "R-lobstr" %}} --- Visualize R Data Structures with Trees
* {{% rhbz 1876189 "R-bench" %}} --- High Precision Timing of R Expressions
* {{% rhbz 1876720 "R-brio" %}} --- Basic R Input Output
* {{% rhbz 1876726 "R-downlit" %}} --- Syntax Highlighting and Automatic
  Linking
* {{% rhbz 1876736 "ghc-http-client-restricted" %}} --- Restricting the servers
  that http-client will use
* {{% rhbz 1876737 "ghc-git-lfs" %}} --- Git-lfs protocol
* {{% rhbz 1882970 "R-presser" %}} --- Lightweight Web Server for Testing
* {{% rhbz 1882978 "R-pkgcache" %}} --- Cache 'CRAN'-Like Metadata and R
  Packages
* {{% rhbz 1882999 "R-lpSolve" %}} --- Interface to Lp_solve to Solve
  Linear/Integer Programs
* {{% rhbz 1883047 "R-pak" %}} --- Another Approach to Package Installation

Reviews
=======

* {{% rhbz 1795526 "ghc-cborg" %}} --- Concise Binary Object Representation
* {{% rhbz 1801519 "golang-github-google-licenseclassifier" %}} --- A License
  Classifier
* {{% rhbz 1803348 "ghc-parsers" %}} --- Parsing combinators
