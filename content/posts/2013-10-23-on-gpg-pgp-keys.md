+++
title = "On GPG/PGP Keys..."
published = 2013-10-23T19:28:00-04:00
tags = ["GSoC", "GPG"]
+++

Way back in 2010, I attended the GSoC Mentors Summit. I thought that would be a
perfect time to get a new GPG key signed by a lot of people. *Unfortunately*, I
decided to do this the night before flying out to Mountain View. With the lack
of sleep, long flight, and busy weekend, I promptly forgot the passphrase to
that key.

For quite a while, I tried to remember or somehow guess the passphrase, to no
avail. I even wrote a Python script to try out combinations of fragments I
thought might be in it. Alas, I had no success.[^1]

Since I just attended another Mentors Summit this past weekend, I had no choice
but to come up with a new key. The new key ID is `0x8D86E7FA E5EB0C10` and
eventually it will accumulate some signatures from the event as I go through
them. The old key, `0xACA35FE6`, really didn't have too many signatures[^2], so
I guess it's no great loss.

[^1]: Okay, I realize you have no way of trusting this statement if you've
      already trusted the old key, but I can't really do anything about that
      anymore.

[^2]: At least you can check this statement on the keyservers, though.
