+++
title = "First Day of Work"
published = 2008-10-28T22:53:00.002000-04:00
tags = ["PEY"]
+++

Well, first day at Broadcom, that is. Not too big of a deal. Most things were
the same. Except we were on a new network setup. Which means no access to the
AMD company intranet, and more importantly, no access to Office Communicator.
Whatever will I do when I need to talk to one of you?

I also wasted some of the day trying to figure out my email password. We got
new Broadcom-y emails, for which we set the password a few weeks ago. Normally,
this would be fine, but that day really sucked, sleep-wise, so I didn't
remember what I set at all. Tried calling the IT desk, but it didn't seem to
work. We thought it was just busy from everyone forgetting their passwords, but
actually there was some long-distance code we needed to enter for it to dial.

Anyway, that's about all that happened on my second "first day". Of course, I
did some real work, too. But I don't think that's as interesting.
