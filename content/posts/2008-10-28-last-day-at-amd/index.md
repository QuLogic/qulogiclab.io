+++
title = "Last Day at AMD!"
published = 2008-10-28T08:30:00-04:00
tags = ["PEY"]
+++

Yep, that's right. Today was my last day of work at
[AMD](https://www.amd.com/). Now, don't go jumping to conclusions that I quit
PEY or anything. Like I said before, the [DTV division was sold]({{< ref
"2008-08-26-dtv-sold" >}}) to [Broadcom](https://www.broadcom.com/). Well,
here's to my first day at the new company tomorrow. It doesn't look like a
whole lot will change just yet anyway. Sure, there's a new network, new email
address, new badge, but it's still the same building, and I'll still go to the
cafeteria for lunch.

A couple of weeks ago, we got our offer package. There sure were a lot of forms
to fill out. It was just like starting a new job entirely. Fun thing though,
they stickied the locations for the signatures. Just take a look at all these
stickers!

{{< figure src="IMG_8575a.JPG" >}}

On the other hand, now I have a whole bunch of "Sign Here" stickers. There were
*thirteen* signatures necessary. I wonder what I'll do with them.

{{< figure src="IMG_8576a.JPG" >}}

To round this post out, I've got one more photo from AMD. A little sign from
outside the 1<sup>st</sup> floor offices in 33CV. I guess I can't exactly go
randomly wandering around there. If you didn't know, only 2<sup>nd</sup> and
3<sup>rd</sup> floors are for DTV. So I guess now we aren't supposed to go to
the offices or the production area downstairs.

{{< figure src="IMG_8609a.JPG" >}}
