+++
title = "Missing Library in Rhythmbox on F17"
published = 2012-06-13T22:56:00.001000-04:00
tags = ["Rhythmbox", "Fedora"]
+++

One of the bugs I encountered after upgrading to Fedora 17 is that my music
library seemed to have disappeared in Rhythmbox. In fact, everything appeared
in the Missing Files view. The strange thing is that all the files appeared
exactly where Rhythmbox thought they should be. In fact, I could even play them
directly from the Missing Files view!

Anyway, after a bit of searching, it appears that I am facing [this
bug](https://bugzilla.gnome.org/show_bug.cgi?id=635041). You see, on my laptop,
I encrypt the `/home` directory. Back on Fedora 16, some bug in gvfs or udisks
caused things to think that that directory was a removable partition. Something
must have been fixed in Fedora 17, because it's no longer marked that way
(which is good, because I hate seeing it in nautilus.) However, Rhythmbox still
thinks those files should be on a separate partition and doesn't correctly
update itself (the aforementioned bug.)

This is pretty easy to fix, in fact. Since `/home` is now no longer considered
a separate mount point, you just have to remove those indicators from
Rhythmbox's config. The config is an XML file that's been pretty-printed, so
the offending information appears on a single line (per file entry). That makes
fixing it a pretty simple thing to do:

`$ cd ~/.local/share/rhythmbox/$ cp rhythmdb.xml rhythmdb.xml.backup$ sed -i -e '/<mountpoint>/d' rhythmdb.xml`
