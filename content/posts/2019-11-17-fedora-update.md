+++
title = "Fedora Update 2019 Weeks 39--45"
date = 2019-11-17T18:42:34-05:00
tags = ["Fedora"]
+++

Somehow, my semi-weekly updates turned into monthly things. Mostly, updates per
week have been rather light and stable, so it always seemed that there was no
need to write an update. Of course, that ends up meaning one really large
update after a long time. This past week was pretty busy, so I thought it best
to finally write up a post.

One small changeset was removing automated `Suggests` from *R* packages when
they do not exist in Fedora yet. This is due to [legal
concerns](https://pagure.io/packaging-committee/issue/914#comment-607130) on
the F31 Change for automated *R* dependencies. So far, I've fixed mine, and
intend to fix others' soon.

On the Python 2 front, aside from dropping unused subpackages from Fedora 32,
I've also ported `git-cinnabar`'s test running from `nose` to `unittest`. This
makes it easier to get the [Python 2
exception](https://pagure.io/fesco/issue/2271). Since upstream is working on
Python 3 support, I expect that this exception won't need to be in place for
long.

One final interesting pursuit is trying to get `R-data.table` to compile on all
architectures again. On s390x, a few tests were failing, and all appeared to be
using `R-bit64`. Looking at _that_ package, tests were disabled on s390x with
no indication as to why. Investigating further, it seemed as if `llroundl` was
returning garbage, e.g., input of 2\*\*x would return some small integer. After
filing a {{% rhbz 1763127 "bug report" %}}, Florian pointed out that the issue
was not in `glibc`, but that `bit64.so` must be linked with `libm`. This is a
strange consequence of `glibc` splitting out math functions. There _is_ a copy
of `llroundl` in `glibc`, but it does _not_ support `long double`, while the
one in `libm` does. Adding the correct linkage fixes both `R-bit64` tests and
`R-data.table` on s390x. Unfortunately, there are still some more
floating-point issues to solve, so `R-data.table` is not yet rebuilt.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-R.devices" "2.16.1-1" %}}
{{% fedora-update "R-R.oo" "1.23.0-1" %}}
{{% fedora-update "R-R.rsp" "0.43.2-1" %}}
{{% fedora-update "R-ascii" "2.1-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-backports" "1.1.5-1" %}}
{{% fedora-update "R-bit64" "0.9.7-8"
    "Link against libm for llroundl, fixing s390x" %}}
{{% fedora-update "R-farver" "2.0.1-1" %}}
{{% fedora-update "R-future" "1.15.0-1" %}}
{{% fedora-update "R-gamlss.dist" "5.1.5-1" %}}
{{% fedora-update "R-gargle" "0.4.0-1" %}}
{{% fedora-update "R-gdtools" "0.2.1-1" %}}
{{% fedora-update "R-gee" "4.13.20-1" %}}
{{% fedora-update "R-glue" "1.3.1-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-gtable" "0.3.0-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-haven" "2.2.0-1" "Fedora 31+ only" %}}
{{% fedora-update "R-hexbin" "1.27.3-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-hexbin" "1.28.0-1" %}}
{{% fedora-update "R-hms" "0.5.2-1" %}}
{{% fedora-update "R-htmltools" "0.4.0-1" %}}
{{% fedora-update "R-htmlwidgets" "1.5.1-1" %}}
{{% fedora-update "R-jpeg" "0.1.8.1-1" %}}
{{% fedora-update "R-knitr" "1.25-2"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-knitr" "1.26-1" %}}
{{% fedora-update "R-later" "1.0.0-1" %}}
{{% fedora-update "R-lintr" "2.0.0-1" %}}
{{% fedora-update "R-ncdf4" "1.17-1" %}}
{{% fedora-update "R-pingr" "1.2.0-1" %}}
{{% fedora-update "R-pingr" "2.0.0-1" %}}
{{% fedora-update "R-pkgbuild" "1.0.6-1" %}}
{{% fedora-update "R-pkgdown" "1.4.1-2"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-plyr" "1.8.4-13"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-promises" "1.1.0-1" %}}
{{% fedora-update "R-purrr" "0.3.3-1" %}}
{{% fedora-update "R-rgdal" "1.4.6-1" %}}
{{% fedora-update "R-rgdal" "1.4.7-1" %}}
{{% fedora-update "R-rgeos" "0.5.2-1" %}}
{{% fedora-update "R-rlang" "0.4.1-1" %}}
{{% fedora-update "R-rmarkdown" "1.16-1" %}}
{{% fedora-update "R-rmarkdown" "1.17-1" %}}
{{% fedora-update "R-roxygen2" "6.1.1-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-roxygen2" "7.0.0-1" "Fedora Rawhide only" %}}
{{% fedora-update "R-rvest" "0.3.5-1" %}}
{{% fedora-update "R-shiny" "1.4.0-1" %}}
{{% fedora-update "R-sp" "1.3.2-1" %}}
{{% fedora-update "R-stringdist" "0.9.5.3-1" %}}
{{% fedora-update "R-stringdist" "0.9.5.5-1" %}}
{{% fedora-update "R-stringr" "1.4.0-4"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-styler" "1.2.0-1" %}}
{{% fedora-update "R-testit" "0.10-1" %}}
{{% fedora-update "R-testit" "0.11-1" %}}
{{% fedora-update "R-tinytest" "1.1.0-1" %}}
{{% fedora-update "R-tinytex" "0.17-1" %}}
{{% fedora-update "R-viridisLite" "0.3.0-6"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-xfun" "0.10-1" %}}
{{% fedora-update "R-xfun" "0.11-1" %}}
{{% fedora-update "R-xml2" "1.2.2-2"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "R-xtable" "1.8.4-3"
    "Exclude Suggests for unavailable packages" %}}
{{% fedora-update "cppzmq" "4.5.0-1" %}}
{{% fedora-update "git-annex" "7.20191009-1" %}}
{{% fedora-update "git-annex" "7.20191017-1" %}}
{{% fedora-update "git-annex" "7.20191024-1" %}}
{{% fedora-update "git-annex" "7.20191106-1" %}}
{{% fedora-update "git-cinnabar" "0.5.2-3"
    "Switch to unittest for running tests" %}}
{{% fedora-update "golang-github-alecthomas-chroma" "0.6.7-1" %}}
{{% fedora-update "golang-github-alecthomas-chroma" "0.6.8-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.1-1" %}}
{{% fedora-update "golang-github-disintegration-gift" "1.2.1-1"
    "Enable tests on all arches" %}}
{{% fedora-update "golang-github-frankban-quicktest" "1.5.0-1" %}}
{{% fedora-update "golang-github-google-cmp" "0.3.1-1" %}}
{{% fedora-update "golang-github-jinzhu-now" "1.1.1-1" %}}
{{% fedora-update "golang-github-lucasb-eyer-colorful" "1.0.3-1" %}}
{{% fedora-update "golang-github-nicksnyder-i18n" "1.10.1-1"
    "Downgrade to latest v1 release" %}}
{{% fedora-update "golang-github-niklasfasching-org" "0.1.5-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "0.1.6-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "0.1.7-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "0.1.8-1" %}}
{{% fedora-update "golang-github-protobuf" "1.3.2-4" "Fedora 31+ only" %}}
{{% fedora-update "golang-github-rwcarlsen-goexif" "0-0.3.20191017git9e8deec" %}}
{{% fedora-update "golang-github-sanity-io-litter" "1.2.0-1" %}}
{{% fedora-update "golang-github-spf13-fsync" "0.9.0-1" %}}
{{% fedora-update "golang-gocloud" "0.17.0-2"
    "Backport fix for 32-bit tests" %}}
{{% fedora-update "golang-x-net" "0-0.54.20191018gitda9a3fd" %}}
{{% fedora-update "hugo" "0.58.3-1" %}}
{{% fedora-update "hugo" "0.59.1-1" %}}
{{% fedora-update "libinsane" "1.0.2-1" %}}
{{% fedora-update "libxls" "1.5.2-1" %}}
{{% fedora-update "numpy" "1.17.4-2"
    "Backport fix for s390x failures and enable non-broken tests on ppc64le" %}}
{{% fedora-update "ocrmypdf" "9.0.5-1" %}}
{{% fedora-update "python-dask" "2.5.2-1" "Fedora 31+ only" %}}
{{% fedora-update "python-dask" "2.6.0-1" "Fedora 31+ only" %}}
{{% fedora-update "python-dask" "2.7.0-1" "Fedora 31+ only" %}}
{{% fedora-update "python-fiona" "1.8.11-1" %}}
{{% fedora-update "python-fiona" "1.8.8-1" "Fedora 30" %}}
{{% fedora-update "python-fiona" "1.8.8-3"
    "Fedora 31 and up (accidental release bump)" %}}
{{% fedora-update "python-fiona" "1.8.9-3" %}}
{{% fedora-update "python-fsspec" "0.5.2-1" "Fedora 31+ only" %}}
{{% fedora-update "python-geopandas" "0.6.0-1" "Fedora 31+ only" %}}
{{% fedora-update "python-kiwisolver" "1.1.0-4" "Drop Python 2 subpackage" %}}
{{% fedora-update "python-numcodecs" "0.6.4-1" %}}
{{% fedora-update "python-pikepdf" "1.6.5-1" %}}
{{% fedora-update "python-pikepdf" "1.7.0-1" %}}
{{% fedora-update "python-pytest-mpl" "0.11-1" %}}
{{% fedora-update "python-rasterio" "1.1.0-2" %}}
{{% fedora-update "python-tblib" "1.5.0-1" %}}
{{% fedora-update "xtl" "0.6.8-1" %}}

New packages
============

* {{% rhbz 1758034 "R-cyclocomp" %}} --- Cyclomatic Complexity of R Code
* {{% rhbz 1758035 "R-xmlparsedata" %}} --- Parse Data of R Code as an XML Tree
* {{% rhbz 1762153 "R-fastmap" %}} --- Fast Implementation of a Key-Value Store
* {{% rhbz 1762230 "golang-github-nicksnyder-i18n-2" %}} --- Translate your Go
  program into multiple languages
* {{% rhbz 1762258 "golang-github-gobuffalo-flect" %}} --- Inflection engine
  for Golang

Reviews
=======

* {{% rhbz 1723052 "python-geopy" %}} --- A Python client for several popular
  geocoding web services
* {{% rhbz 1767187 "golang-github-direnv-dotenv" %}} --- Go dotenv parsing
  library for direnv
* {{% rhbz 1767235 "direnv" %}} --- Per-directory shell configuration tool
* {{% rhbz 1767752 "ghc-text-zipper" %}} --- A text editor zipper library
* {{% rhbz 1768186 "python-anytree" %}} --- Powerful and Lightweight Python
  Tree Data Structure
