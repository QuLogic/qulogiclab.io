+++
title = "Fedora Update 2019 Weeks 25--30"
date = 2019-08-04T00:54:18-04:00
tags = ["Fedora"]
+++

It has been quite a long time since the last post, unfortunately, but I'm not
gone yet. I was fairly busy for a couple of weeks, and then taking a little
break while waiting for the Fedora 31 Mass Rebuild to finish. But here we go
with what's been taking place.

The major bit of work leading up to the Mass Rebuild was getting all *Go*
packages up do date to the [newly approved
Guidelines](https://docs.fedoraproject.org/en-US/packaging-guidelines/Golang/).
This involved several refreshes of packages, newly created packages, and other
general cleanup, implemented almost entirely by Robert-André Mauchin
(**eclipseo**) for all the *Go* libraries. Unfortunately, this missed adding
`Obsoletes` to the renamed packages, so I (manually) tracked commit
notifications and wrote a script to add these in. There were nearly 200 of
these, which is too many to list here, but you can [find them on
datagrepper](https://apps.fedoraproject.org/datagrepper/raw?topic=org.fedoraproject.prod.git.receive&start=1562284520&end=1562738774&user=qulogic).

I also tried to finish up some of this work by updating the applications as
well. I believe all of mine are ready, plus several others, but there are still
many more to go that are not part of the *Go* SIG or have strong vendoring that
makes it difficult to unbundle and build against our packages.

The other thing I tried to finish before the Mass Rebuild was getting any FTBFS
packages working again. This meant fixing tests for various updated packages,
e.g., [`asv` against latest
Chrome](https://github.com/airspeed-velocity/asv/pull/857),
[`python-octave-kernel` against Octave
5](https://github.com/Calysto/octave_kernel/pull/160), and [`R-sysfonts` for
new Liberation font
paths](https://src.fedoraproject.org/rpms/R-sysfonts/c/8daee5b67a9113e35402eb3e133ab12e88a131d5).
As part of {{% rhbz 1570421 %}}, I wrote a few patches too remove the use of
`github.com/kardianos/osext`, which is deprecated upstream and now provided by
the *Go* standard library.

Otherwise, updates were fairly light. I'm pretty sure release monitoring is
broken though, so I'll need to do some manual checks again. I did take a slight
break while waiting for the Mass Rebuild to finish as well. So soon I'll get
back on things, but it's been rather relaxing otherwise.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-callr" "3.3.0-1" %}}
{{% fedora-update "R-callr" "3.3.1-1" "Second update" %}}
{{% fedora-update "R-coda" "0.19.3-1" %}}
{{% fedora-update "R-deldir" "0.1.22-1" %}}
{{% fedora-update "R-ellipsis" "0.2.0.1-1" "Rawhide only" %}}
{{% fedora-update "R-future" "1.14.0-1" %}}
{{% fedora-update "R-git2r" "0.26.1-1" %}}
{{% fedora-update "R-haven" "2.1.1-1" %}}
{{% fedora-update "R-hms" "0.5.0-1" "Rawhide only" %}}
{{% fedora-update "R-pillar" "1.4.2-1" "Rawhide only" %}}
{{% fedora-update "R-processx" "3.4.0-1" %}}
{{% fedora-update "R-processx" "3.4.1-1" "Second update" %}}
{{% fedora-update "R-remotes" "2.1.0-1" %}}
{{% fedora-update "R-rlang" "0.4.0-1" %}}
{{% fedora-update "R-sysfonts" "0.8-2"
    "Update Liberation font paths to new locations (Rawhide only)" %}}
{{% fedora-update "R-tinytex" "0.14-1" %}}
{{% fedora-update "R-vctrs" "0.2.0-1" "Rawhide only" %}}
{{% fedora-update "asv" "0.4.1-2"
    "Fix tests against latest Chrome webdriver" %}}
{{% fedora-update "exercism" "3.0.12-1" %}}
{{% fedora-update "git-annex" "7.20190626-1" %}}
{{% fedora-update "git-cinnabar" "0.5.2-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.0-1" %}}
{{% fedora-update "golang-github-bugsnag" "1.5.1-3"
    "Remove deprecated `github.com/kardianos/osext` (Rawhide only)" %}}
{{% fedora-update "golang-github-bugsnag-panicwrap" "1.2.0-4"
    "Remove deprecated `github.com/kardianos/osext` (Rawhide only)" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.1.4-1" "Rawhide only" %}}
{{% fedora-update "golang-k8s-kubernetes" "?"
    "Remove deprecated `github.com/kardianos/osext` (Rawhide only)" %}}
{{% fedora-update "golang-rsc-qr" "0.2.0-1" %}}
{{% fedora-update "ocrmypdf" "8.3.2-1" %}}
{{% fedora-update "python-dask" "2.1.0-1" "Rawhide only" %}}
{{% fedora-update "python-glad" "0.1.31-1" %}}
{{% fedora-update "python-libpysal" "4.1.0-1" %}}
{{% fedora-update "python-numcodecs" "0.6.3-3" %}}
{{% fedora-update "python-octave-kernel" "0.31.0-2"
    "Fix tests with Octave 5 (Rawhide only)" %}}
{{% fedora-update "python-partd" "1.0.0-1" %}}
{{% fedora-update "python-pikepdf" "1.5.0-1" %}}
{{% fedora-update "python-xarray" "0.12.3-1" %}}
{{% fedora-update "python-zict" "1.0.0-1" %}}
{{% fedora-update "tinygo" "0.7.0-1" "Second update (see below)" %}}

New packages
============

There are two new interesting applications that have finally been built:

* {{% rhbz 1720563 "tinygo" %}} --- The Go compiler for small places
* {{% rhbz 1713843 "htmltest" %}} --- Test generated HTML for problems

and the libraries to support them, which were mentioned in earlier posts.

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "tinygo" "0.6.0-1" %}}
{{% fedora-update "htmltest" "0.10.3-1" "Rawhide only" %}}
{{% fedora-update "golang-github-daviddengcn-algs" "0-0.1.20190610gitfe23fab"
    "Rawhide only" %}}
{{% fedora-update "golang-github-daviddengcn-assert" "0-0.1.20190610gitba7e68a"
    "Rawhide only" %}}
{{% fedora-update "golang-github-daviddengcn-villa" "0-0.1.20190610git3f35da8"
    "Rawhide only" %}}
{{% fedora-update "golang-github-golangplus-bytes" "0-0.1.20190610git45c989f"
    "Rawhide only" %}}
{{% fedora-update "golang-github-golangplus-fmt" "0-0.1.20190609git2a5d6d7"
    "Rawhide only" %}}
{{% fedora-update "golang-github-golangplus-sort" "0-0.1.20190610git8253da0"
    "Rawhide only" %}}
{{% fedora-update "golang-github-golangplus-testing" "0-0.1.20190609gitaf21d9c"
    "Rawhide only" %}}
{{% fedora-update "golang-gopkg-seborama-govcr-2" "2.4.2-1" "Rawhide only" %}}
{{% fedora-update "golang-rsc-qr" "0.2.0-1" %}}

Reviews
=======

* {{% rhbz 1723052 "python-geopy" %}} --- A Python client for several popular
  geocoding web services
* {{% rhbz 1723103 "python-imagehash" %}} --- A Python perceptual image hashing
  module
* {{% rhbz 1730904 "golang-github-jsimonetti-rtnetlink" %}} --- Low-level
  access to the Linux rtnetlink API
* {{% rhbz 1730907 "golang-github-mikioh-ipaddr" %}} --- Basic functions for
  the manipulation of IP address prefixes
* {{% rhbz 1731568 "python-pytest-sugar" %}} --- Change the default look and
  feel of pytest
* {{% rhbz 1731592 "python-netpyne" %}} --- Develop, simulate and analyse
  biological neuronal networks in NEURON (blocked due to dependencies)
* {{% rhbz 1731594 "python-matplotlib-scalebar" %}} --- Artist for matplotlib
  to display a scale bar
