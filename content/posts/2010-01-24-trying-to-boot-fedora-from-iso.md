+++
title = "Trying to boot Fedora from ISO"
published = 2010-01-24T23:24:00.004000-05:00
tags = ["Linux", "LiveCD", "Fedora", "Multiboot"]
+++

Last time I managed to boot Ubuntu and Linux Mint from ISO pretty easily.
Unfortunately, Fedora does not work so simply. There is no `iso-scan` parameter
that can be used for it.

Fedora uses [Dracut](https://fedoraproject.org/wiki/Dracut) to create its
initramfs infrastructure. From that page, I found the `rdshell` parameter that
could be added to the kernel command line to get into a shell before switching
to the real root and init. Once there, I was able to mount the USB drive, then
the ISO, and finally the actual live image. So I knew that it should be
possible to boot from ISO, but the initramfs doesn't have any parameter to look
for it. And useful for later, I found the version of dracut was
002-13.4.git8f397a9b.fc12.

I went looking at the Dracut pages a little more, and it turns out that support
for booting off an ISO was added, and is in dracut-004. Which is great, except
of course, it means it's not on the existing image. The
[commit](https://github.com/dracutdevs/dracut/commit/651f3f3d1a3f58d58fd365962154e555017bd7ae)
was easy to find, and seemed self-contained, so I figured I might be able to
patch the existing initramfs myself. It would probably be better to just build
it from scratch, but I don't think I have everything installed for it and it
didn't look like too big of a change to do manually. Modifying the initramfs
was simple:

{{< highlight "bash" >}}
> mount -o loop Fedora-12-i686-Live.iso /mnt/cdrom
> zcat /mnt/cdrom/isolinux/initrd0.img | cpio -iV
> # Modify files here
> find . | cpio -oVH newc | gzip > /media/LiveLinux/boot/F12-i686-initrd.img
{{< /highlight >}}

Now for that modify step, you need to look at the `install` script of the
`90dmsquash-live` module to see where dracut would be installing the modified
files. Anyway, that seemed to work up until getting past dracut. I saw plymouth
start up, but then the kernel complained about the invalid root. Obviously, the
ISO was not actually mounted in the end. I even booted to the dracut shell and
found no trace of the USB drive. I'm waiting to see if the comments on {{% rhbz
557426 %}} will help explain something.
