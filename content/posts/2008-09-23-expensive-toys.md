+++
title = "Expensive Toys"
published = 2008-09-23T21:34:00-04:00
tags = ["PEY"]
+++

So I'm sure you've seen that I have those 6 monitors and maybe a TV or two to
play with at work. We all know how much those cost, a few hundred per monitor,
maybe a thousand or two for a TV.

Anyway, a couple days ago, I got this card to set up in a computer. It's a
modulator card that takes some video file input and has the ability to output
in various modulation schemes, like 8VSB, DVB, QAM64, and a few others. I was
told it was expensive, but I didn't think it was too much. It's pretty cool,
because I can set up whatever type of input signal I want. So I happened to
find the packing slip with the box.

The packing slip also had the bill: the card cost as much as **3 or 4**
(regular) TV's!! Good thing it's safe in the box... :wink:
