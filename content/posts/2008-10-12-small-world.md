+++
title = "Small World"
published = 2008-10-12T02:22:00.001000-04:00
tags = []
+++

So, the [Association of Part-time Undergraduate
Students](https://www.apus.utoronto.ca/) just sent me some mail. However, the
name was certainly not for anyone in our house. My dad was about to send it
back when I looked at it. And as it turns out, I did know the person to whom it
was addressed. It had [Kirill](https://vancouver365.wordpress.com/)'s name, and
my address.

Out of all those thousands of part-time students (not just PEY's), that little
glitch happened to pick someone I knew. Small world, eh?
