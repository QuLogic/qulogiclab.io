+++
title = "They said it would be around now..."
published = 2008-09-12T23:18:00-04:00
tags = ["PEY"]
+++

Can you believe it's been 4 months that we've been on PEY?

Those PEY office guys say we'd really start to notice it around now. All our
friends would be starting class, and we wouldn't. But you know, I don't think
I've quite noticed it yet.

Normally over the summer, I usually don't see a lot of people (besides a few
random gatherings). So in fact, since I see so many PEY students at AMD, it's
completely not like a regular summer. Anyway, it's been a week of school now.
There's been enough dinners and things so far to keep me from noticing.

I admit though, it was a bit weird not going to university on that very first
morning, or for F!rosh week. But after being on campus a couple times lately, I
noticed that there really aren't a lot of people there (that I know). Perhaps
it's just that a lot of people are on PEY, or it's so early in the year.
Normally, I'd run into a lot more people on campus.

I wonder when it'll really sink in...
