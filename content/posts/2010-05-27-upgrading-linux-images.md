+++
title = "Upgrading Linux images..."
published = 2010-05-27T03:00:00.003000-04:00
tags = ["Linux", "Ubuntu", "LiveCD", "Fedora", "Multiboot", "Linux Mint"]
+++

So there's been quite a few new releases of distributions recently. Last month,
there's been [Ubuntu 10.04](https://www.ubuntu.com/) and [Linux Mint
9](https://www.linuxmint.com/). This month it's [Fedora
13](https://fedoraproject.org/).

Anyway, I just updated my live boot drive with all of those and everything
works pretty well.
