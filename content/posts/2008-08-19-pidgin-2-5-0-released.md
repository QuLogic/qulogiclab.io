+++
title = "Pidgin 2.5.0 Released!"
published = 2008-08-19T02:05:00.003000-04:00
tags = ["Pidgin"]
+++

Look, [Pidgin 2.5.0](https://pidgin.im/) is out!

And yes, we have MSNP15 support. That means offline message support, sending
custom smileys, server-side aliasing, buddy embleming and a whole bunch of
other goodies. Hylke made some nice updates to the icon theme that looks
cleaner, I think. Though I'm not sure whether I like the new pigeon yet. I hope
with all that testing those Adium guys did, there won't be too many problems.
Some people aren't as optimistic, but we'll see how it goes. :stuck_out_tongue:

Oh, and guess what? [I'm a
developer](https://developer.pidgin.im/viewmtn/revision/info/a85113159ac3ecd4bef1ea184fd8fc2e0378e426),
too!
