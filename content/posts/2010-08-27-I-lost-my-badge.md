+++
title = "I lost my badge!"
published = 2010-08-27T23:29:00.001000-04:00
tags = ["Work"]
+++

And by that, I mean I gave it back to my manager. Of course, what I really mean
there is that work is over!

I'd say it was an interesting experience. So writing documentation is probably
not what I'd want to do for a living, but it was OK for a summer job to pass
the time.

It's really a Windows shop there, which wasn't great. On the other hand, I
don't have a problem with Office. It's really quite nice, especially if you're
trying to script it. Don't even ask how to do any scripting in OpenOffice.
Conversely, the ribbon was not amazing. It requires too many clicks to do
anything and its layout changing is annoying.

So I didn't really say much. I'm just happy to be done work!
