+++
title = "Nothing's Happening!"
published = 2009-02-28T01:29:00.002000-05:00
tags = []
+++

Here, I mean... As some people have mentioned, "tomorrow" should have happened
a loonnngggg time ago. I probably won't talk about the AGO, though. You should
have gone when it was free!

Anyway, there's been some interesting things going on. A few months of working
at this new company. Which as I guessed, is ending up not all that different. I
might write a little bit more about that later. (I'm not promising any more
tomorrows, though!)

Christmas/New Year's vacation was quite good. I'm sure most of you know
already, but I went on a vacation to Goa. It was wayyy too short at ~2.5 weeks,
and that made it jam-packed with things to do. But all in all, it was pretty
fun.

Anyway, hope to get back to writing here a bit more often...
