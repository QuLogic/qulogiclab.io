+++
title = "Coming soon, to a Pidgin near you..."
published = 2010-05-21T16:55:00.001000-04:00
tags = ["Pidgin"]
+++

Direct connections for MSN! What do I mean? Just take a look at the screenshot
below.

{{< figure src="FileTransfers.png" >}}

What's that? You don't see it? Just take a look at how fast it's going. Yes,
that's right; it's not a pitiful 10KiB/s, but about an order of magnitude
faster.

It all started with [this little patch](https://developer.pidgin.im/ticket/247)
on trac. But it required a lot of work to get it going. I must have made at
least 60 commits just fixing things and getting it to cooperate with aMSN and
the official client. I *think* it should be good now, but you can always
disable it in the Account options. It even makes buddy icon and custom emoticon
loading faster.

Anyway, I promised I would say something about 2.7.1, and here you go. Direct
connections will be in 2.7.1. In fact, I'm just about to push all the
revisions.
