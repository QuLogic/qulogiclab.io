+++
title = "Feeling a little unsteady?"
published = 2010-06-23T14:10:00.002000-04:00
tags = ["Toronto", "Earthquake", "Twitter", "Geophysics", "Technology"]
+++

That is to say, did you notice the *earthquake*?

I almost didn't recognize it. Since I'm working from home due to the G20, I
don't have the benefit of a tall building that would react to the movement (if
you want to call it that.)

Anyway, thanks to [Elizabeth](http://elizabethhan.com/)'s favourite Web 2.0
app, [Twitter](https://www.twitter.com/), check out this cool USGS site, [over
here](https://earthquake.usgs.gov/earthquakes/map/). Realtime data about
earthquakes all over the globe. What's really cool is this [Did You Feel It?
section](https://earthquake.usgs.gov/data/dyfi/) where people can report what
they felt, and it graphs it over the region.

Now if only they could fix the text encoding in their database. The website
appears correctly in UTF-8, but some accented characters from some fields are
incorrect. Ah, well...
