+++
title = "College Puzzle Challenge"
published = 2008-11-10T01:46:00.002000-05:00
tags = ["Microsoft", "CPC"]
+++

Before I forget, this weekend was the Microsoft [College Puzzle
Challenge](https://www.collegepuzzlechallenge.com/). It was a lot of fun, like
last year. I think our best score was fourth here at UofT.

{{< figure src="IMG_8639.JPG" >}}

Can you figure out our name?

There were a couple of puzzles with answers that just didn't make sense. Like
this [Drop
Quote](https://www.collegepuzzlechallenge.com/Puzzles/ViewPuzzle.ashx?id=97)
one. With a drop quote puzzle, you need to drop the letters to form words. But
here you have to do funky add and subtract before you get a message out of
it... Actually, it doesn't seem so hard written out. :P Another annoying one
was [Hospital
Visit](https://www.collegepuzzlechallenge.com/Puzzles/ViewPuzzle.ashx?id=98).
I mean, finding random medical abbreviations from those descriptions is a
little crazy. How are we supposed to figure out what "must take her medicine
before eating" is in abbreviated form? Not only that, but the patient names in
the answer aren't even the same as in the question.

Anyway, ended up twenty-first here, which isn't too bad. That's
133<sup>rd</sup> overall. Like last year, we really dropped in the last hour or
so, but what can you do. I guess I can be happy that we're the first team with
the score of 170.
