+++
title = "Fedora Update 2020 Weeks 6--9"
date = 2020-03-01T19:23:31-05:00
tags = ["Fedora"]
+++

It's been around a month since the last packaging update post, and a thing of
major note is that Fedora 32 was branched from Rawhide last week. This means
one extra branch on which builds must be made, but for now it's mostly in sync
with Rawhide. Fedora 32 is also in Beta Freeze, so updates don't really flow
out right now either.

Leading up the Branch point, I went on a little run to try and get in as many
updates as possible. I generally prioritized breaking changes so those would
get in without breaking our [Updates
policy](https://docs.fedoraproject.org/en-US/fesco/Updates_Policy/). So I
caught up on a backlog of all my *R* packages, plus several other packages in
the *Go* SIG. This involved writing a bit of *Python* scripting to build and
submit quickly, which I'm hoping to turn around into something more automated
eventually.

Following on from the fixes I made to correct FTBFS earlier, I was finally able
to get the new dependencies for `tinygo` reviewed. Not only can `tinygo` be
installed on Fedora 31 again, but I was also able to enable the AVR and RISCV
backends (or at least their tests.) Unfortunately, just before the Fedora 32
Mass Rebuild, both Go 1.14 and LLVM 10 landed, and `tinygo` is not yet ready
for either. That means it's not yet rebuilt for Fedora 32.

Three other remaining FTBFS were `asv`, `python-cartopy`, and
`python-pycountry`. For `asv`, I had to re-bundle `flot` as the separate
package was orphaned and retired. I unfortunately don't know enough JavaScript
packaging to pick that one up. For `python-pycountry`, I was able to find a fix
upstream that could be backported. For `python-cartopy`, I tagged a beta
release upstream that was then built in Fedora. This was convenient for several
other packagers since the existing Cartopy release did not support Proj >= 6.
I'm now down to 3 FTBFS packages, for which I've filed some upstream issues or
am investigating personally. Hopefully it will not be too long before those are
fixed.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-IRkernel" "1.1-1" %}}
{{% fedora-update "R-R.methodsS3" "1.8.0-1" %}}
{{% fedora-update "R-Rmpfr" "0.8.1-1" %}}
{{% fedora-update "R-V8" "3.0.1-2" %}}
{{% fedora-update "R-XML" "3.99.0.3-1" %}}
{{% fedora-update "R-ape" "5.3-6"
    "Add bootstrap setup to build without `igraph`" %}}
{{% fedora-update "R-bit" "1.1.15.2-1" %}}
{{% fedora-update "R-broom" "0.5.4-1" %}}
{{% fedora-update "R-broom" "0.5.5-1" %}}
{{% fedora-update "R-caTools" "1.18.0-1" %}}
{{% fedora-update "R-callr" "3.4.2-1" %}}
{{% fedora-update "R-chron" "2.3.55-1" %}}
{{% fedora-update "R-cli" "2.0.2-1" %}}
{{% fedora-update "R-date" "1.2.39-1" %}}
{{% fedora-update "R-deldir" "0.1.25-1" %}}
{{% fedora-update "R-dplyr" "0.8.4-1" %}}
{{% fedora-update "R-dtplyr" "1.0.1-1" %}}
{{% fedora-update "R-farver" "2.0.3-1" %}}
{{% fedora-update "R-foreach" "1.4.8-1" %}}
{{% fedora-update "R-future" "1.16.0-1" %}}
{{% fedora-update "R-future" "1.16.0-2"
    "Rebuild against now-packaged `RhpcBLASctl`" %}}
{{% fedora-update "R-gamlss.dist" "5.1.6-1" %}}
{{% fedora-update "R-gmp" "0.5.13.6-1" %}}
{{% fedora-update "R-gplots" "3.0.3-1" %}}
{{% fedora-update "R-gss" "2.1.12-1" %}}
{{% fedora-update "R-hexbin" "1.28.1-1" %}}
{{% fedora-update "R-jsonlite" "1.6.1-1" %}}
{{% fedora-update "R-knitr" "1.28-1" %}}
{{% fedora-update "R-lintr" "2.0.1-1" %}}
{{% fedora-update "R-mapproj" "1.2.7-1" %}}
{{% fedora-update "R-mime" "0.9-1" %}}
{{% fedora-update "R-mnormt" "1.5.6-1" %}}
{{% fedora-update "R-modelr" "0.1.6-1" %}}
{{% fedora-update "R-processx" "3.4.2-1" %}}
{{% fedora-update "R-ps" "1.3.2-1" %}}
{{% fedora-update "R-remotes" "2.1.1-1" %}}
{{% fedora-update "R-repr" "1.1.0-1" %}}
{{% fedora-update "R-rlang" "0.4.4-1" %}}
{{% fedora-update "R-rlang" "0.4.5-1" %}}
{{% fedora-update "R-rmarkdown" "2.1-1" %}}
{{% fedora-update "R-rstudioapi" "0.11-1" %}}
{{% fedora-update "R-sfsmisc" "1.1.5-1" %}}
{{% fedora-update "R-sp" "1.4.0-1" %}}
{{% fedora-update "R-sp" "1.4.1-1" %}}
{{% fedora-update "R-stringi" "1.4.6-1" %}}
{{% fedora-update "R-svglite" "1.2.3-1" %}}
{{% fedora-update "R-tidyr" "1.0.2-1" %}}
{{% fedora-update "R-tidyselect" "1.0.0-2" %}}
{{% fedora-update "R-timeSeries" "3062.100-1" %}}
{{% fedora-update "R-tinytex" "0.19-1" %}}
{{% fedora-update "R-tinytex" "0.20-1" %}}
{{% fedora-update "R-unix" "1.5.1-1" %}}
{{% fedora-update "R-uuid" "0.1.4-1" %}}
{{% fedora-update "R-vctrs" "0.2.3-1" %}}
{{% fedora-update "R-xfun" "0.12-1" %}}
{{% fedora-update "R-yaml" "2.2.1-1" %}}
{{% fedora-update "asv" "0.4.1-7" "Re-bundle `flot` to fix FTBFS" %}}
{{% fedora-update "caddy" "1.0.3-2"
    "Rebuilt for [GHSA-jf24-p9p9-4rjh](https://github.com/gorilla/websocket/security/advisories/GHSA-jf24-p9p9-4rjh)" %}}
{{% fedora-update "cppzmq" "4.6.0-1" %}}
{{% fedora-update "etcd" "3.3.12-5.20190413gitf29b1ad"
    "Rebuilt for [GHSA-jf24-p9p9-4rjh](https://github.com/gorilla/websocket/security/advisories/GHSA-jf24-p9p9-4rjh)" %}}
{{% fedora-update "git-cinnabar" "0.5.4-1" %}}
{{% fedora-update "git-lfs" "2.10.0-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.2-1" %}}
{{% fedora-update "golang-github-andybalholm-cascadia" "1.1.0-1" %}}
{{% fedora-update "golang-github-bep-gitmap" "1.1.1-1" %}}
{{% fedora-update "golang-github-bep-golibsass" "0.6.0-1" %}}
{{% fedora-update "golang-github-bep-golibsass" "0.6.0-1" %}}
{{% fedora-update "golang-github-briandowns-spinner" "1.9.0-1" %}}
{{% fedora-update "golang-github-cosmos72-gomacro"
    "2.7-10.20200224git30d4a5c" %}}
{{% fedora-update "golang-github-frankban-quicktest" "1.7.2-1" %}}
{{% fedora-update "golang-github-gobuffalo-logger" "1.0.3-1" %}}
{{% fedora-update "golang-github-gobuffalo-packd" "1.0.0-1" %}}
{{% fedora-update "golang-github-godbus-dbus" "5.0.3-1" %}}
{{% fedora-update "golang-github-google-cmp" "0.4.0-1" %}}
{{% fedora-update "golang-github-google-wire" "0.4.0-1" %}}
{{% fedora-update "golang-github-gorilla-csrf" "1.6.2-1" %}}
{{% fedora-update "golang-github-gorilla-handlers" "1.4.2-1" %}}
{{% fedora-update "golang-github-gorilla-mux" "1.7.4-1" %}}
{{% fedora-update "golang-github-gorilla-websocket" "1.4.1-1" %}}
{{% fedora-update "golang-github-hashicorp-lru" "0.5.4-1" %}}
{{% fedora-update "golang-github-julienschmidt-httprouter" "1.3.0-1" %}}
{{% fedora-update "golang-github-klauspost-compress" "1.10.0-1" %}}
{{% fedora-update "golang-github-klauspost-cpuid" "1.2.3-1" %}}
{{% fedora-update "golang-github-lib-pq" "1.3.0-1" "Enable tests" %}}
{{% fedora-update "golang-github-libgit2-git2go" "0.28.4-1" %}}
{{% fedora-update "golang-github-mattn-isatty" "0.0.12-1" %}}
{{% fedora-update "golang-github-mattn-runewidth" "0.0.8-1" %}}
{{% fedora-update "golang-github-mattn-shellwords" "1.0.10-1" %}}
{{% fedora-update "golang-github-montanaflynn-stats" "0.6.1-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "1.0.0-1"
    "Fedora 32+ only" %}}
{{% fedora-update "golang-github-olekukonko-tablewriter" "0.0.4-1" %}}
{{% fedora-update "golang-github-pebbe-zmq4" "1.1.1-1" %}}
{{% fedora-update "golang-github-pelletier-toml" "1.6.0-1" %}}
{{% fedora-update "golang-github-pierrec-lz4" "3.2.1-1" %}}
{{% fedora-update "golang-github-schollz-progressbar-2" "2.15.0-1" %}}
{{% fedora-update "golang-github-sergi-diff" "1.1.0-1" %}}
{{% fedora-update "golang-github-spf13-cast" "1.3.1-1" %}}
{{% fedora-update "golang-github-spf13-viper" "1.6.2-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.7.2-1" %}}
{{% fedora-update "golang-gopkg-src-d-billy-4" "4.3.2-1" %}}
{{% fedora-update "golang-gopkg-yaml-2" "2.2.8-1" %}}
{{% fedora-update "golang-mvdan-xurls" "2.1.0-1" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.8.20200208git1ff21df" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.9.20200208git1ff21df"
    "Add support for LLVM 10" %}}
{{% fedora-update "golang-uber-atomic" "1.5.1-1" %}}
{{% fedora-update "golang-uber-multierr" "1.4.0-1" %}}
{{% fedora-update "golang-vitess" "3.0-4.20190701git948c251"
    "Rebuilt for [GHSA-jf24-p9p9-4rjh](https://github.com/gorilla/websocket/security/advisories/GHSA-jf24-p9p9-4rjh)" %}}
{{% fedora-update "golang-x-image" "0-0.12.20200218git58c2397" %}}
{{% fedora-update "golang-x-xerrors" "0-0.3.20200218git9bdfabe" %}}
{{% fedora-update "htmltest" "0.12.1-1" %}}
{{% fedora-update "hugo" "0.55.6-2"
    "Rebuilt for [GHSA-jf24-p9p9-4rjh](https://github.com/gorilla/websocket/security/advisories/GHSA-jf24-p9p9-4rjh)" %}}
{{% fedora-update "hugo" "0.65.2-1" %}}
{{% fedora-update "hugo" "0.65.3-1" %}}
{{% fedora-update "ocrmypdf" "9.6.0-1" %}}
{{% fedora-update "python-branca" "0.4.0-1" %}}
{{% fedora-update "python-cartopy" "0.18.0~b1-1" "Update to latest beta" %}}
{{% fedora-update "python-dask" "2.10.1-1" %}}
{{% fedora-update "python-dask" "2.11.0-3" %}}
{{% fedora-update "python-geopandas" "0.6.3-1" %}}
{{% fedora-update "python-geopandas" "0.7.0-1" "Fedora 32+ only" %}}
{{% fedora-update "python-geoplot" "0.4.0-1" %}}
{{% fedora-update "python-libpysal" "4.2.2-1" %}}
{{% fedora-update "python-pikepdf" "1.10.1-1" %}}
{{% fedora-update "python-pikepdf" "1.10.2-1" %}}
{{% fedora-update "python-pycountry" "19.8.18-4"
    "Backport iso-codes 4.4 fix" %}}
{{% fedora-update "python-rasterio" "1.1.3-1" %}}
{{% fedora-update "python-xarray" "0.15.0-1" "Fedora 32+ only" %}}
{{% fedora-update "python-zict" "2.0.0-1" "Fedora 32+ only" %}}
{{% fedora-update "tinygo" "0.11.0-2" "Fedora 31 only" %}}
{{% fedora-update "tinygo" "0.9.0-1" "Fedora 30 only" %}}
{{% fedora-update "xeus" "0.23.5-1"
    "Re-enable armv7hl and ppc64le for Fedora 32+" %}}
{{% fedora-update "xtl" "0.6.12-1" %}}
{{% fedora-update "xtl" "0.6.12-2"
    "Re-enable armv7hl and ppc64le for Fedora 32+" %}}

New packages
============

The first two *Go* packages are new dependencies for `tinygo`, the
`goldmark-highlighting` and `golibsass` packages are for `hugo`, and the
remaining are mostly for `git-lfs`. The *Python* packages are new dependencies
for `python-geoplot`, and the *R* packages are the usual march to the
`tidyverse`.

* {{% rhbz 1722393 "R-devtools" %}} --- Tools to Make Developing R Packages
  Easier
* {{% rhbz 1763145 "golang-bug-serial-1" %}} --- Cross-platform serial library
  for Golang
* {{% rhbz 1763147 "golang-github-creack-goselect" %}} --- Select(2)
  implementation in Go
* {{% rhbz 1789637 "R-modelr" %}} --- Modelling Functions that Work with the
  Pipe
* {{% rhbz 1802541 "python-mercantile" %}} --- Web mercator XYZ tile utilities
* {{% rhbz 1802926 "python-mapclassify" %}} --- Classification Schemes for
  Choropleth Maps
* {{% rhbz 1802933 "python-contextily" %}} --- Context geo-tiles in Python
* {{% rhbz 1804120 "golang-github-yuin-goldmark-highlighting" %}} --- A Syntax
  highlighting extension for the goldmark markdown parser
* {{% rhbz 1804142 "golang-github-subosito-gotenv" %}} --- Load environment
  variables from `.env` or `io.Reader` in Go
* {{% rhbz 1805555 "golang-gopkg-jcmturner-aescts-1" %}} --- AES CBC Ciphertext
  Stealing mode for Go
* {{% rhbz 1805556 "golang-gopkg-jcmturner-dnsutils-1" %}} --- DNS utilities
  for Go
* {{% rhbz 1805557 "golang-gopkg-jcmturner-goidentity-2" %}} --- Go interface
  for authenticated identities and attributes
* {{% rhbz 1805559 "golang-gopkg-jcmturner-rpc-0" %}} --- Remote Procedure Call
  libraries
* {{% rhbz 1805562 "golang-github-jcmturner-gofork" %}} --- Forked Go standard
  library packages with work arounds
* {{% rhbz 1805564 "golang-gopkg-jcmturner-gokrb5-5" %}} --- Pure Go Kerberos
  library for clients and services
* {{% rhbz 1805566 "golang-github-dpotapov-spnego" %}} --- Cross-platform HTTP
  calls with Kerberos authentication
* {{% rhbz 1805580 "golang-github-bep-golibsass" %}} --- Easy to use Go
  bindings for LibSass
* {{% rhbz 1807409 "R-RhpcBLASctl" %}} --- Control the Number of Threads on
  BLAS

Reviews
=======

I went on a little review spree, so there are quite a few this past month.
Several more are waiting on submitter updates as well.

* {{% rhbz 1795045 "golang-github-nrdcg-dnspod" %}} --- DNSPod Go API client
* {{% rhbz 1795047 "golang-github-liquidweb" %}} --- Golang API client for
  Liquid Web's Storm API
* {{% rhbz 1795056 "golang-gopkg-h2non-gock-1" %}} --- Expressive HTTP traffic
  mocking and testing made easy in Go
* {{% rhbz 1795076 "golang-github-anacrolix-log" %}} --- Logging library for Go
* {{% rhbz 1795078 "golang-github-benbjohnson-immutable" %}} --- Immutable
  collections for Go
* {{% rhbz 1795292 "python-pytest-astropy-header" %}} --- pytest plugin to add
  diagnostic info
* {{% rhbz 1795797 "golang-github-azure-amqp" %}} --- AMQP 1.0 client library
  for Go
* {{% rhbz 1797130 "golang-github-google-jsonnet" %}} --- Implementation of
  Jsonnet in pure Go
* {{% rhbz 1797135 "golang-github-influxdata-cron" %}} --- Fast,
  zero-allocation cron parser in ragel and golang
* {{% rhbz 1797140 "golang-github-influxdata-httprouter" %}} --- High
  performance HTTP request router that scales well
* {{% rhbz 1797145 "golang-github-mileusna-useragent" %}} --- Go parser for
  user agent strings
* {{% rhbz 1797271 "ghc-filepath-bytestring" %}} --- Library for manipulating
  RawFilePaths in a cross platform way
* {{% rhbz 1798393 "ghc-lens-family-core" %}} --- Haskell 98 Lens Families
* {{% rhbz 1798513 "nanovna-saver" %}} --- A tool for reading, displaying and
  saving data from the NanoVNA
* {{% rhbz 1798786 "golang-github-creack-pty" %}} --- PTY interface for Go
* {{% rhbz 1800352 "golang-github-krishicks-yaml-patch" %}} --- Library to
  apply YAML versions of RFC6902 patches
* {{% rhbz 1800355 "golang-github-pires-proxyproto" %}} --- Go library
  implementation of the PROXY protocol, versions 1 and 2
* {{% rhbz 1800357 "golang-github-z-division-zookeeper" %}} --- Native
  ZooKeeper client for Go
* {{% rhbz 1801516 "golang-github-google-monologue" %}} --- Monitor that checks
  that Certificate Transparency Logs
* {{% rhbz 1801522 "golang-github-otiai10-mint" %}} --- The very minimum
  assertion for Golang testing framework
* {{% rhbz 1801843 "golang-github-antihax-optional" %}} --- Optional parameters
  for Go
* {{% rhbz 1801889 "golang-github-benlaurie-gds-registers" %}} --- Go API for
  GDS registers
* {{% rhbz 1801893 "golang-github-mohae-deepcopy" %}} --- Deep copy things
* {{% rhbz 1802337 "golang-github-marten-seemann-qpack" %}} --- A (minimal)
  QPACK implementation in Go
* {{% rhbz 1802343 "golang-github-alangpierce-forceexport" %}} --- Access
  unexported functions from other packages
* {{% rhbz 1802357 "golang-github-viant-toolbox" %}} --- Go utility library
* {{% rhbz 1803320 "ghc-prettyprinter-ansi-terminal" %}} --- ANSI terminal
  backend for the »prettyprinter« package
* {{% rhbz 1804636 "ghc-rio" %}} --- A standard library for Haskell
* {{% rhbz 1805482 "golang-github-muesli-reflow" %}} --- Reflow lets you
  word-wrap strings or entire blocks of text.
