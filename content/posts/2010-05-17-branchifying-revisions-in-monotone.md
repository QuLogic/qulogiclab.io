+++
title = "Branchifying Revisions in Monotone"
published = 2010-05-17T23:55:00.004000-04:00
tags = ["Pidgin", "Monotone"]
+++

Have you ever started committing things in [Monotone](https://www.monotone.ca/)
and then realized "This should probably go in a separate branch"? It's somewhat
difficult to fix that. You can add a new branch cert, but the old one won't go
away.

I wrote a little script based on a [mailing list
message](https://www.mail-archive.com/monotone-devel@nongnu.org/msg02725.html) I
saw. It removes the old branch cert, and adds the new one, moving forward along
revisions. It even changes merges with "the rest" into propagate messages
instead. There are limitations, though. It only works with linear history (but
if you've been working alone, that shouldn't be a problem) and it requires mtn
0.45 (since it uses the binary cert format.) Of course, it goes without saying
that if you've shared any of your changes with someone else, then this is
pointless (or you'd have to change everyone's database.)

It's pretty short. Make sure those SQL statements don't break up when you copy
it.

{{< highlight "bash" >}}
#!/bin/bash

set -e

DB=db.mtn
KEY='someone@domain.com'
START=0123456789abcdef0123456789abcdef01234567

OLD_BRANCH='com.domain.main'
NEW_BRANCH='com.domain.fancy-stuff'

prev=
curr=$START
while [ -n "$curr" ]; do
    echo "Changing revision $curr"

    mtn --db=$DB db execute \
      "DELETE FROM revision_certs WHERE hex(revision_id) = upper('$curr') AND name = 'branch';"
    mtn --db=$DB -k$KEY cert $curr branch $NEW_BRANCH

    parents=`mtn --db=$DB automate parents $curr`
    if [ `echo "$parents" | wc -l` -eq 2 ]; then
        # Fix a merge
        echo "Fixing merge for $curr"
        mtn --db=$DB db execute \
          "DELETE FROM revision_certs WHERE hex(revision_id) = upper('$curr') AND name = 'changelog';"
        other=`echo "$parents" | sed -e "/$prev/d"`
        mtn --db=$DB -k$KEY cert $curr changelog \
"propagate from branch '$OLD\_BRANCH' (head $other)
            to branch '$NEW\_BRANCH' (head $prev)"
    fi

    prev=$curr
    curr=`mtn --db=$DB automate children $curr`
done
{{< /highlight >}}
