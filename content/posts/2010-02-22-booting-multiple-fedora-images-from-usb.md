+++
title = "Booting multiple Fedora images from USB"
published = 2010-02-22T21:11:00.006000-05:00
tags = ["Linux", "LiveCD", "Fedora", "Multiboot"]
+++

Well, I've been trying to [boot Fedora from ISO]({{< ref
"2010-01-24-trying-to-boot-fedora-from-iso" >}}), but haven't exactly been
successful. On {{% rhbz 557426 "the bug" %}}, you can see a possible fix, but
it's a bit complicated and requires messing with the image from an existing
install.

Instead, I figured out a way to do it that uses the extracted files. It's
pretty simple. First, mount the ISO and extract three files:

1. the LiveOS image: `squashfs.img` and possibly `osmin.img`,
2. the compressed kernel: `vmlinuz0`, and;
3. the initial ramdisk `initrd0.img`

In case you were wondering, the copies in the `EFI` and `isolinux` directories
are the same. You can then place all these files in a subdirectory on the
drive. Then you add an entry to GRUB pointing to the files. In this case, I
placed the files in `/boot/F12-$arch` and the drive's label is `LiveLinux`.

{{< highlight "clike" >}}
menuentry "Fedora 12 32bit" {
    linux /boot/F12-i686/vmlinuz0 root=live:LABEL=LiveLinux \
        live_dir=boot/F12-i686 rootfstype=auto ro liveimg quiet rhgb
    initrd /boot/F12-i686/initrd0.img
}
{{< /highlight >}}
