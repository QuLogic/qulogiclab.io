+++
title = "Adventures in GNOME3 Land"
published = 2011-05-11T16:02:00.005000-04:00
tags = ["GNOME3"]
+++

Over the past few weekends I've been trying out GNOME3 via the [Fedora 15
Beta](https://fedoraproject.org/). I've already read many a blog post in
opposition to various things, so I was not overly surprised when I tried it
out. Fortunately, I have a pretty boring setup because I only tested on my
desktop, meaning I didn't have to worry about external monitors or suspending.

In fact, with respect to working with the whole thing, it wasn't a terrible
experience. Granted, I made a couple tweaks right off the bat since I knew
about them. I still like my menus and buttons with icons, and the default
hinting is terrible on an LCD. I didn't really care about the min/max buttons
though.

Because I've been using [Compiz](http://www.compiz.org/) and not
[Metacity](https://blogs.gnome.org/metacity/), the transition to
[Mutter](https://gitlab.gnome.org/GNOME/mutter/) and [GNOME
Shell](https://wiki.gnome.org/Projects/GnomeShell) was pretty easy. One thing I
miss is the "Put" plugin, which allowed moving a non-maximized window to any of
9 positions on the screen using the keypad. And the window switching is a bit
boring now.

I even reported a {{% rhbz 699105 "bug" %}} or {{% rhbz 702772 "two" %}}. These
are pretty minor, but everyone's reported the major problems by now, I'm sure.
It's the little things that people are going to pick out anyway (like the
[uncanny valley](https://en.wikipedia.org/wiki/Uncanny_valley) in robotics/CGI.)
Overall, I found the experience pleasant, except for two somewhat annoying
problems.

Firstly, the default theme is, well... I was going to say terrible, but on
second look, it's not that bad, but still not to my taste. To be honest, I've
never been crazy about dark themes. But this theme is some weird conglomerate
of dark and light that doesn't mesh well at all. The icons are either black and
white (I'm not colour blind!) or sometimes washed out. Take, for example, gedit
in the screenshot below. Odd transition from menubar to toolbar, the disabled
icons/text are hard to see... It just doesn't look good to me.

{{< figure src="gnome3-gedit.png" >}}

Secondly, GNOME3 has koumpounophobia. That's a fear of buttons, for those who
don't know (not that I did, and it most likely refers to clothing, really.) I
don't live in crazy 1990s Mac land; all my mouse have at least two buttons. My
desktop mouse identifies itself as having 12 buttons! (though half are for
scrolling, technically) Yet for some unknown reason, right-clicking is totally
useless on GNOME Shell. You can't right-click any of the applets on the top
panel. It's more likely people will right-click it than hold down Alt as a
guess, but that "Shut Down" item is hidden behind the "Suspend" item with an
Alt-key. A little lower in the stack, GTK+3 decided to drop
scroll-on-notebook-changes-tabs. What's even more annoying is if you have a
GTK+2 app, it will scroll nicely, but the "new" apps don't. Unless you try in
gnome-terminal, where they implemented it themselves. Inconsistency is never
good.

So this Fedora 15 Beta is on a separate external drive. I'm not sure if I'll be
upgrading straight away once F15 goes gold. I certainly need to find a real
theme first, and I suspect when it's released, there'll be a lot more people
with criticism that may nudge things along (as opposed to some knee-jerk
reactions by a few early-adopters).
