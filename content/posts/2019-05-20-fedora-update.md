+++
title = "Fedora Update 2019 Week 17--19"
date = 2019-05-20T17:26:10-04:00
tags = ["Fedora"]
+++

I meant to post this last weekend, but was too busy then. There were a _lot_ of
updates this past week, mostly in *R* and *Python*. These were mostly routine,
and I did not run into many issues except for a few new (aka, to-be-packaged)
dependencies.

I found a bug in the `R-IRkernel` package, where the Jupyter kernel spec was
encoding the path to the internal R executable. This executable path is
arch-specific, while the package was not. Surprisingly, this was not caught by
the usual noarch checks. I fixed this by hardcoding the path to be `/usr/bin/R`
(rather than making the package archful).

Someone sent me an email that [paperwork](https://openpaper.work/) was freezing
while running orientation detection. I had not seen such a thing when I
initially tested it out on Fedora 29. This bug turned out to be specific to
Fedora 30+, as it's the first release to have
[tesseract-ocr](https://github.com/tesseract-ocr/tesseract) 4. That version
appears to bug out when orientation detection is requested, but the `osd` files
are not available. To fix, I just added a requirement of `tesseract-osd` to
paperwork.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-Bessel" "0.6.0-1" %}}
{{% fedora-update "R-IRkernel" "1.0.1-2"
  "Fixes above-mentioned bug with arch-y paths" %}}
{{% fedora-update "R-diffobj" "0.2.3-1" %}}
{{% fedora-update "R-fs" "1.3.1-1"
  "Also works around a [bug in libuv](https://github.com/libuv/libuv/issues/2262)" %}}
{{% fedora-update "R-future" "1.13.0-1" %}}
{{% fedora-update "R-gamlss.dist" "5.1.4-1" %}}
{{% fedora-update "R-hexbin" "1.27.3-1" %}}
{{% fedora-update "R-knitr" "1.23-1" %}}
{{% fedora-update "R-processx" "3.3.1-1" %}}
{{% fedora-update "R-progress" "1.2.1-1" %}}
{{% fedora-update "R-quadprog" "1.5.7-1" %}}
{{% fedora-update "R-rversions" "2.0.0-1" %}}
{{% fedora-update "R-rvest" "0.3.4-1" %}}
{{% fedora-update "R-sfsmisc" "1.1.4-1" %}}
{{% fedora-update "R-svglite" "1.2.2-1" %}}
{{% fedora-update "R-tinytex" "0.13-1" %}}
{{% fedora-update "R-tufte" "0.5-1" %}}
{{% fedora-update "R-xfun" "0.7-1" %}}
{{% fedora-update "git-cinnabar" "0.5.1-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.5.0-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.7-1" %}}
{{% fedora-update "hugo" "0.55.5-1" %}}
{{% fedora-update "hugo" "0.55.6-1" "(Second bug fix)" %}}
{{% fedora-update "ocrmypdf" "8.3.0-1" %}}
{{% fedora-update "paperwork" "1.2.4-3" 
  "Fixes above-mentioned dependency bug" %}}
{{% fedora-update "python-kiwisolver" "1.1.0-1" %}}
{{% fedora-update "python-octave-kernel" "0.30.2-1" %}}
{{% fedora-update "python-octave-kernel" "0.30.3-1" "(Second bug fix)" %}}
{{% fedora-update "python-pikepdf" "1.3.0-1" %}}
{{% fedora-update "python-pytest-tornado" "0.8.0-1" %}}
{{% fedora-update "python-rasterio" "1.0.23-1"
  "Only in Fedora 30; still investigating some bugs elsewhere" %}}
{{% fedora-update "python-snuggs" "1.4.6-1"
  "Only in Fedora <30; needs some patching for pyparsing 2.4.0" %}}
{{% fedora-update "python-tblib" "1.4.0-1" %}}

New packages
============

Due to updates in some *R* package dependencies, I've had to package a few more
things. I'm also slowly progressing towards the rest of the *tidyverse*. This
past week saw several new packages written and sent for review.

* {{% rhbz 1710187 R-zeallot %}} --- Multiple, Unpacking, and Destructuring
  Assignment
* {{% rhbz 1710207 R-clisymbols %}} --- Unicode Symbols at the R Prompt
* {{% rhbz 1710208 R-ini %}} --- Read and Write '.ini' Files
* {{% rhbz 1710215 R-xopen %}} --- Open System Files, 'URLs', Anything
* {{% rhbz 1710219 R-rappdirs %}} --- Determine Where to Save Data, Caches, and
  Logs
* {{% rhbz 1710223 R-parsedate %}} --- Recognize and Parse Dates in Various
  Formats
* {{% rhbz 1711154 R-gh %}} --- GitHub API
* {{% rhbz 1711175 R-vctrs %}} --- Vector Helpers
* {{% rhbz 1711184 R-foghorn %}} --- Summarize CRAN Check Results in the
  Terminal
* {{% rhbz 1711203 R-haven %}} --- Import and Export 'SPSS', 'Stata' and 'SAS'
  Files

This application allows you to use any Jupyter kernel you like in an
*IPython*-like terminal application:

* {{% rhbz 1708057 python-jupyter-console %}} --- Jupyter terminal console

I also packaged this one because it was interesting:

* {{% rhbz 1707258 golang-rsc-qr %}} --- Golang generator of QR codes

Reviews
=======

I managed to squeeze in a few reviews as well. These were approved by me:

* {{% rhbz 1703477 recorder %}} --- A lock-free, real-time flight recorder for
  C or C++ programs
* {{% rhbz 1707795 nutty %}} --- Simple utility for network information

I started reviewing {{% rhbz 1706659 ensmallen %}} as well, but it's currently
crashing for me.
