+++
title = "Fedora Update 2019 Weeks 31--32"
date = 2019-08-19T01:10:12-04:00
tags = ["Fedora"]
+++

It's been a couple of weeks, so it's time for another Fedora packaging update.
There were two major things over that time: 1) Fedora 31 was branched from
Rawhide, and 2) release monitoring started working again.

The branch point also meant that the Change Code Complete deadline was passed.
As part of the Go SIG, I was one of the packagers behind the [Adopt new Go
Packaging
Guidelines](https://fedoraproject.org/wiki/Changes/Adopt_new_Go_Packaging_Guidelines)
Change. As mentioned in the last post, this was mostly handled by **@eclipseo**
and the {{% rhbz 1715534 "tracker bug" %}} was marked complete for it just
earlier. I am also behind the [Automatic R runtime
dependencies](https://fedoraproject.org/wiki/Changes/Automatic_R_runtime_dependencies)
Change. As part of this Change, I initiated a mini-rebuild last week of all
affected R packages. I will write about that in a separate post. That {{% rhbz
1732111 "tracker bug" %}} is now Code Complete, though there are a couple FTBFS
to fix up.

With release monitoring working again, that meant a slew of new bug reports
about new package versions being available. This happened just last Friday, so
I haven't had much chance to update everything. I did manage to go through
almost all the *R* packages, except for a few with new dependencies. I also
updated one or two *Go* and *Python* packages as well.

I also finally finished the PR updating Matplotlib to 3.1.1. As I didn't want
to have another release miss an update, I opted to skip the failing-on-ppc64le
wx test, and opened a {{% rhbz 1738752 "bug report" %}} so that someone else
could investigate. This PR was also open so long, I needed to backport a new
NumPy 1.17 fix which wasn't originally necessary. But Fedora 31 should once
again be in sync with Matplotlib (for some time.)

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R" "3.6.0-5"
    "Removes dead `macros.R` and script (Rawhide/F31 only)" %}}
{{% fedora-update "R-IRkernel" "1.0.2-1" %}}
{{% fedora-update "R-RcppCCTZ" "0.2.6-1" %}}
{{% fedora-update "R-clipr" "0.7.0-1" %}}
{{% fedora-update "R-curl" "4.0-1" "Rawhide only" %}}
{{% fedora-update "R-deldir" "0.1.23-1" %}}
{{% fedora-update "R-doParallel" "1.0.15-1" %}}
{{% fedora-update "R-foreach" "1.4.7-1" %}}
{{% fedora-update "R-httr" "1.4.1-1" %}}
{{% fedora-update "R-iterators" "1.0.12-1" %}}
{{% fedora-update "R-knitr" "1.24-1" %}}
{{% fedora-update "R-markdown" "1.1-1" %}}
{{% fedora-update "R-openssl" "1.4.1-1" %}}
{{% fedora-update "R-pkgbuild" "1.0.4-1" %}}
{{% fedora-update "R-plyr" "1.8.4-12" "Disable extra tests (too flaky)" %}}
{{% fedora-update "R-prettydoc" "0.3.0-1" %}}
{{% fedora-update "R-reprex" "0.3.0-3" "Fixes tests with pandoc 2.5" %}}
{{% fedora-update "R-reticulate" "1.13-1" %}}
{{% fedora-update "R-rgeos" "0.5.1-1" %}}
{{% fedora-update "R-rmarkdown" "1.14-1" %}}
{{% fedora-update "R-systemfit" "1.1.22-4"
    "Removes dead macro `%_R_make_search_index`" %}}
{{% fedora-update "R-tikzDevice" "0.12.3-1" %}}
{{% fedora-update "R-tinytex" "0.15-1" %}}
{{% fedora-update "R-xml2" "1.2.2-1" "Rawhide/F31 only" %}}
{{% fedora-update "glava" "1.6.3-2" "Fix FTBFS" %}}
{{% fedora-update "golang-github-dlclark-regexp2" "1.2.0-1" %}}
{{% fedora-update "golang-github-gdamore-tcell" "1.2.0-1" %}}
{{% fedora-update "golang-github-git-lfs-gitobj" "1.4.0-1" %}}
{{% fedora-update "golang-github-jdkato-prose" "1.1.1-1" %}}
{{% fedora-update "golang-github-lucasb-eyer-colorful" "1.0.2-1"
    "Added F30/F29" %}}
{{% fedora-update "golang-github-mattn-shellwords" "1.0.6-1" %}}
{{% fedora-update "golang-github-spf31-cobra" "0.0.5-1" %}}
{{% fedora-update "golang-github-stretchr-testify" "1.4.0-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.5.1-1" %}}
{{% fedora-update "golang-github-tdewolff-parse" "2.3.8-3" %}}
{{% fedora-update "golang-github-tdewolff-test" "1.0.3-3" %}}
{{% fedora-update "golang-x-text" "0.3.2-5" %}}
{{% fedora-update "python-matplotlib" "3.1.1-1" %}}
{{% fedora-update "python-pycountry" "19.8.18-1" "Rawhide/F31 only" %}}
{{% fedora-update "tinygo" "0.7.1-1" "No Rawhide/F31 yet due to Go 1.13" %}}

New packages
============

There's not too much here; it's mostly new dependencies for existing updates.

* {{% rhbz 1743045 "golang-github-git-lfs-ntlm" %}} --- NTLM Implementation for
  *Go*
* {{% rhbz 1743048 "golang-github-niklasfasching-org" %}} --- Org mode parser
  with html & pretty printed org rendering

Reviews
=======

Unfortunately, `mock` broke after the Fedora 31 branching, so I haven't been
able to do many reviews, though there are several *Go* packages requests I'd
like to review.

* {{% rhbz 1739750 "python-listparser" %}} --- Parse OPML, FOAF, and iGoogle
  subscription lists
