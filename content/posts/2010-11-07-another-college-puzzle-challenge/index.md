+++
title = "Another College Puzzle Challenge!"
published = 2010-11-07T23:32:00.003000-05:00
tags = ["Microsoft", "CPC"]
+++

{{< figure src="IMG_4021_modified.JPG" >}}

So this weekend was another [College Puzzle
Challenge](https://www.collegepuzzlechallenge.com/). Just as fun as [last
year]({{< ref "2008-11-10-college-puzzle-challenge" >}}), but slightly
different.

There were no waves, so we got all the puzzles at the beginning. I guess that
has its advantages and its disadvantages. It's somewhat daunting to have so
many puzzles to decipher. On the other hand, you're almost never at a loss for
something to do. The biggest problem is that it lets some teams finish in three
hours!

One upside from last year was that even though the requirements stated
Silverlight 3, we didn't really need it. License issues aside, the Moonlight 3
preview is just too crashy to be useful as far as I can tell. For some reason,
they got rid of the date&time of last submission, which I found nice before.
They *did* manage to reduce the website issues, even while adding auto-refresh
to many pages. It wasn't perfect, but definitely better than last year (though
it could be because they actually gave us multiple copies of each puzzle
again).

But the best part this year is that we actually solved the metapuzzle. Nowhere
near first, but maybe about two hours before the end. Then we even finished all
the rest, too! It's too bad they don't show the time, because we really did
solve the last one at exactly 11PM, the absolute end of the challenge.

{{< figure src="Screenshot-4.png" >}}

I don't know if this was different from last year, but once you solved the
metapuzzle, any further solves didn't seem to count for anything. Since the
metapuzzle is the "clincher", I guess that makes sense. So there we are, eighth
at UT, and 96<sup>th</sup> overall. Moving up from last year!
