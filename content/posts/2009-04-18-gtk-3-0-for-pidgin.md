+++
title = "GTK+ 3.0 for Pidgin"
published = 2009-04-18T23:48:00.002000-04:00
tags = ["GTK+", "Pidgin"]
+++

A little while ago, grim posted a [link about preparations for GTK+
3.0](https://www.debian-news.net/2009/04/06/preparing-for-gtk-30-and-gnome-3/).
I started a bit of work on it in a [separate
branch](https://developer.pidgin.im/viewmtn/branch/shortchanges/im.pidgin.cpw.qulogic.gtk3).

So far, it hasn't been too much trouble. I managed to fix single includes in
the main branch, and all deprecated functions in libpurple core too. Finch was
super-easy and required almost no work. What was left was the Pidgin UI.

The main Pidgin UI required a bit more work. Replacing GtkOptionMenu with
GtkComboBox was pretty simple, just a couple preprocessor checks needed. Using
GtkTooltip instead of GtkTooltips was also pretty painless, but there's still
one matter left. The headline text that goes in the buddy list for mail
notification used to do a hack with the tooltip to colour it correctly. At the
moment, it ends up grey because I'm not sure how to get it working.

There are still a couple big changes left. I still really don't know how to
write UI Manager stuff, and it's hard to find good examples. It only has to be
done twice, for the buddy list and for the conversation window, but I'm not
sure how much that really means. Fortunately, we are not heavily GNOME
dependant, so that's probably the biggest change.
