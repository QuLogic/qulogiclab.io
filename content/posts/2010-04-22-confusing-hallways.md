+++
title = "Confusing hallways..."
published = 2010-04-22T20:18:00.005000-04:00
tags = ["Skule"]
+++

Have you ever been in a building and just got totally confused? Like where in
the world am I going through this hallway?

I don't mean that it's a little weird. Sure,
[Bahen](https://www.greatspaces.utoronto.ca/projects/bahen.htm) is a bit weird,
but only because it's basically a building on top of another one. It's not
really all that confusing. MedSci's also a bit weird, with that extra lecture
hall (that we had ECE253 in) in a separate half for some reason.

No, what I'm talking about is the
[Benson](https://en.wikipedia.org/wiki/File:Clara_Benson_Building.JPG)
building. We just had an exam in there a couple days ago. I don't really get
that building. If you've ever had an exam there, you'll know what I mean.

There are two sets of doors, but you can only enter (for an exam) through the
ones closer to the intersection. When you get in, there are stairs on the left,
just in front of you. But if you go forward just a little, there are more
stairs to the right in a nearby hallway. Eventually you can get upstairs after
going through several floors and half-floors and what not.

Finding a washroom isn't all that easy either. On the third floor, the women's
washroom is about halfway down the hall from the gym, but the men's is hidden.
You go all the way to the end of the hallway and there's some sort of cafeteria
in front of you. Immediately to the left of the hallway are more stairs (that
go in the opposite direction). The men's washroom is in the little nook just a
bit farther left between the end of the building and the stairs. It's confusing
just trying to describe it. Anyway, I missed it once as I thought that was
where the elevator would be (but apparently there isn't one). So I went down
the stairs instead and all that was there was the second set of doors. And
apparently, all you can do is exit!

Yep, I just don't get it.
