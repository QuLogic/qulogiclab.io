+++
title = "The Fedora 17 \"Beefy Miracle\""
published = 2012-06-08T00:08:00-04:00
tags = ["Linux", "GNOME3", "Fedora"]
+++

{{< figure src="beefy-miracle.png" >}}

So, a new release of [Fedora](https://fedoraproject.org/) was set free just
last week, the so-called "Beefy Miracle". A couple of releases ago (or maybe
last release), I had really thought I might wait at least a month after a
release before upgrading. Turns out my curiosity got the best of me, and I went
and upgraded my laptop mere days after the release.

Fortunately, this release has been much much better right out of the box.
Having read the release notes, I was prepared for the one major bug, namely the
kernel being stuck at an old version. Since it only affected shutdown, it
wasn't too big of a deal, just a bit annoying.

Being the third release with GNOME3, it's *finally* starting to come together
into something usable. With the right extensions, it's even better than GNOME2
was. I can't say exactly what changes went into it, but I guess it's a lot of
behind the scenes changes to make things Just Work.

There are still a few things that need work, of course. I'm still not a big fan
of the grey theme, but at least the window decoration is not huge anymore. It's
still a bit difficult to find good *complete* themes that fix that. Also, it
seems they introduced a fade-out of windows that are not in focus. It's sort of
like it's disabled-in-appearance-only. I'm sort of yes-no on this change. It
seems like it might be good for accessibility, but they don't seem to have
tried this out all too much. Sometimes widgets remain faded even when the rest
of the application is back in focus. Using some dialogs (like gedit's Replace)
do weird things.

One last point is going to be about the wallpaper. I know I don't see it all
too much, but the last three releases have had wallpapers that are really quite
nice. Fedora's wallpapers have been traditionally mostly blue, the trademark
colour. This one deviates a bit with a splash of pink, but I think it's just
enough to make it pop. And who could not like fireworks?
