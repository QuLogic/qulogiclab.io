+++
title = "Goan Idol"
published = 2008-08-03T02:09:00.005000-04:00
tags = ["Goa"]
+++

So, about that International Goan Convention... I didn't go to any of the
daytime workshops because of work, but we did go to the evening events.

The first evening was the Goan Idol. This was held at UTM (they got a deal or
something) in one of the lecture halls. It's too bad, because there was no
stage or spotlights or anything. And it's really hard to take pictures in low
light without flash. Not to mention everyone in the Mando troupe wanted a
picture or video, which I had to take. All in all, there were about 4 or 5
cameras I had to use. Sometimes at the same time!  

There were 10 contestants, with varying degrees of singing talent. Actually, I
didn't think any of them were that bad. Anyway, some pictures...

{{< figure src="IMG_7594.JPG" caption="The ones who didn't win..." >}}

{{< figure src="IMG_7656.JPG" caption="Third Place" >}}

{{< figure src="IMG_7645.JPG" caption="Second Place" >}}

{{< figure src="IMG_7636.JPG" caption="First Place" >}}
