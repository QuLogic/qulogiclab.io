+++
title = "Pidgin 2.7.0 Released!"
published = 2010-05-12T23:14:00.002000-04:00
tags = ["Pidgin"]
+++

So it seems like I only post about minor releases (as opposed to micro), but I
promise that I'll definitely say something about 2.7.1.

Anyway, [Pidgin](https://pidgin.im/) 2.7.0 should be released pretty soon now.
You may or may not know that the release versions are dictated by API/ABI
requirements. A minor release means we've added API. I wouldn't say there are
tons of new user-visible features in this release, but definitely some
developer-related things.

Most importantly, we dropped support for old GTK+ and GLib, requiring 2.10.0
and 2.12.0 respectively. Visibly, that means we can't support Windows 98
anymore, but I think we can say that's an acceptable loss by now. For
development purposes, though, that means a ton of code that can be removed. It
means a lot less code that needs to be maintained. We also were able to upgrade
some things like from EggTrayIcon to GtkStatusIcon, which is better at
integrating into the notification area.

Don't worry; it wasn't all development changes. We've also got integrated Mood
setting. This interfaces to ICQ and
[XMPP](https://xmpp.org/extensions/xep-0107.html), so I don't know too much
about it. More in my area, MSN gained support for file transfer previews, which
show up in the request window and the conversation.

There are quite a few other changes that I don't really even know about, so you
might want to check the
[ChangeLog](https://developer.pidgin.im/wiki/ChangeLog).
