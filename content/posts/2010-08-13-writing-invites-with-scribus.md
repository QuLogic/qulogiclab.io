+++
title = "Writing Invites with Scribus"
published = 2010-08-13T14:18:00.001000-04:00
tags = ["Linux", "Scribus"]
+++

So I recently had to create a whole bunch of invites. Not a huge number like a
thousand, but more than enough that I didn't want to make each one
individually. Anyway, I decided to make them in
[Scribus](https://www.scribus.net/). It's semi-professional, and has grade-A
output. After the fact, I'd say that it was a pretty good choice, except for a
few annoying wrinkles.

The Good
--------

On the good side, Scribus is really stringent about printing issues (though
that wasn't as much of a concern this time). It's really good at getting you to
use proper fonts. And I really love the ability to use math in any entry field.
OK, this doesn't seem like a lot, but overall it's pretty good, and I don't
want to go listing out everything!

The Bad
-------

The reason why I say it's only *semi*-professional is because of these
wrinkles. There was one annoying crash, but fortunately, it went away after an
upgrade. The real problems are mostly non-intuitiveness, where things don't
happen how you'd like. I'm having a hard time remembering them as I think I've
grown accustomed to it all. Actually, one thing I remember is that the style
editor acts very oddly. If you're editing styles, the style information never
stays in sync with the style you have selected. Nine times out of ten (I made
that stat up), stuff will be disabled as if you've selected a built-in
non-editable style even when you haven't. Oh, and the undo stack is pretty
weird. Why does Scribus seem to think I've edited the document when I just
saved and Scribus isn't even in focus!

The Ugly
--------

However, out of all things, I think the most disappointing was the scripting
API. I mean, automating stuff was one of the things I hoped to do. I did
eventually get that done, but it was not fun getting there, and took way too
long.

Originally, I planned to create a template, and then write a script which
created a Scribus file for each invite. This idea didn't work out at all as the
importing broke stuff. That's OK, because then I had the better idea of using a
Master Page, with each invite as a separate page. Scribus has the ability to
export each page separately, so this worked out well. I could put everything on
the Master Page except the small bit with the name which would be different for
each page.

The problems really arise from the fact that the scripting API is not well
thought-out. It may use [Python](https://www.python.org/), but it's not at all
Pythonic. It's not even self-consistent. When you change an object's
properties, you can specify its name, or work with the mysterious "selection".
That selection could be a whole object, or it could be some text within the
object. Sometimes, after calling a function, the selection is gone! Exporting
to a PDF involves a `PDFfile` object, while nowhere else is a class used.

Scribus files seem to be XML, but the scripting API does not take any advantage
of that. All you get is a handful of top-level functions, and stuff to modify
that mysterious selection. It's really too bad, because with a proper object
model, I'm sure you could do some really cool stuff.

The End
-------

In the end, I did end up with a script that worked. It was based of the
[mail-merge script](https://wiki.scribus.net/index.php/Poor_man%27s_mail_merge)
on the Scribus wiki. It didn't do what I originally wanted, but it did get the
job done. And of course, the invites did turn out awesome.

But after trying that, going back to VBA in MS Office was a bit of a relief.
