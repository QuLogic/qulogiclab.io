+++
title = "Fixing the Toaster"
published = 2011-07-09T00:04:00.008000-04:00
tags = ["DIY"]
+++

Sometimes, things just break. Maybe it's planned obsolescence; I don't really
know. But I don't want to be wasting things just because they "seem" to not
work anymore.

Speaking of things not working, our toaster-oven (a Bravetti) recently stopped
turning on. (You didn't see that coming, did you?) Turning on the timer didn't
light the lamp, and didn't electrify the elements. A bit of a problem when you
want to toast things (pizza in a microwave just isn't the same!)

Taking Things Apart
-------------------

{{< figure src="IMG_8364.JPG" >}}

As it turns out, the same thing has happened before. That made this work pretty
easy as I knew exactly where to look for the problem. As a plus, this is an
easy fix and if it must fail, I'd pick this over some other problem.

First, you need to take out all the screws around the top cover. On this model,
they're on the back and on the bottom along the feet. Remove the cover and you
can see all the wiring as in the picture.

The top control is temperature, middle is heating mode, and bottom is the
timer. Our trouble arises in the timer control. You need to pull off the front
dial. It's got some goop in it to hold it in place, so I wedge it out with a
flathead screwdriver. Once it's off, take out the two screws holding the timer
control onto the panel. Pull out the electrical contacts, and you're good.

{{< figure src="IMG_8365.JPG" >}}

So now you have the timer control; what to do with it? Open it up, of course!
It's locked up using small bent metal tabs. Simply straighten them up with some
pliers and you should be able to pull off the top bit. You need to take all of
the shell off, as below.

{{< figure src="IMG_8371.JPG" >}}
{{< figure src="IMG_8366.JPG" >}}

In the picture to the left are the separated parts of the timer control. At
the left is the outer shell with the bell. Then the timer mechanism, a cover
and another mounting cover.

{{< figure src="IMG_8367.JPG" >}}

In this picture we have the back view of the timer mechanism. Keep in mind the
little black arm as that interfaces with the silver arm in the bell.

Cleaning the Contacts
---------------------

That black plastic bit at the top is the site of all the trouble. Take a look
at the contact below, which has become black. I don't know whether this is some
kind of galvanic corrosion, but it means the switch just doesn't close.

{{< figure src="IMG_8369.JPG" >}}

I can't quite remember what I did the last time, but it may have involved
scratching at it with a screwdriver. This time, I used a cone sanding bit on a
rotary tool to clean it up entirely. It's a bit difficult to see below, but
it's much cleaner now. Note, I didn't clean the mount around the contact, so
you can see the difference between before and after there.

{{< figure src="IMG_8370.JPG" >}}

Finishing Up
------------

Now that the contacts are clean, it's just a matter of putting everything back
together. It's important to get the post on the timer aligned with the lever in
the bell, or it won't ring when finished. Then reattach all the casing, bend
the tabs, and screw the rest all together. And the final test, of course, is
to turn it on and toast some bread!

{{< figure src="IMG_8372.JPG" >}}
