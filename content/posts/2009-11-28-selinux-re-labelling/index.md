+++
title = "SELinux Re-labelling"
published = 2009-11-28T22:32:00.003000-05:00
tags = ["SELinux", "Linux", "Fedora"]
+++

So yesterday, I downgraded from ext4 to ext3. I think I forgot to set SELinux
to permissive, so a couple things ended up wrong. I re-labelled everything.
This is what it looked like:

{{< figure src="IMG_2134.JPG" >}}

Now, there are a few things wrong there. First, the text really doesn't doesn't
fit in. I think this is because KMS sets up the blue background, but isn't
quite connected to the virtual console nicely. There's always work going on
there, but it's not quite right yet. It's always getting better though.

The second thing, you might not notice at first. But if you look between the
first paragraph and the lines of stars, then you'll notice some random
characters. Those were typed by me. You see, nothing happens between printing
the first two lines and the first asterisk. I think I read somewhere that each
one corresponds to a few thousand files, but it took so long, I almost thought
something had frozen. Fortunately, I let it continue and it finished correctly.
But I really think there should be a better indication of activity in between
asterisks.
