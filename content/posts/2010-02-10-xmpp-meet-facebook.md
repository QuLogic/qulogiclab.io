+++
title = "XMPP, meet Facebook..."
published = 2010-02-10T18:55:00.005000-05:00
tags = ["XMPP", "Pidgin", "Facebook"]
+++

So, it seems the [Facebook](https://www.facebook.com/) has *finally* added
support for logging in to their Chat via [XMPP](https://xmpp.org/). This is, of
course, awesome so people stop asking about it, but also terrible since it took
them so darn long. That meant a lot of horrible
[hacks](https://code.google.com/p/pidgin-facebookchat/) just to get it working
in the interim. (No offence Eion, I just mean the scraping, not the code.)

This announcement comes straight from the [developer
blog](https://developers.facebook.com/news.php?blog=1&story=361). You need to
have set up a username (which you can [check
here](https://www.facebook.com/sitetour/chat.php)). Casey's blog has [all the
screenshots you need to figure it
out](http://blog.caseyho.com/2010/02/how-to-enable-facebook-chat-in-pidgin.html).
The only downside is it doesn't use encrypted streams.

The biggest problem, though? Having to organize a few hundred buddies into
their correct contacts.
