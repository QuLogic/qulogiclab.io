+++
title = "Got a Bus Pirate!"
published = 2010-08-15T00:26:00.003000-04:00
tags = ["Linux", "Bus Pirate", "Fedora"]
+++

{{< figure src="IMG_3162a.JPG" >}}

**Update:** The ModemManager rule should really only apply to the USB device,
not the TTY. I updated the rule below to be a bit more specific.

Around the end of July, I ordered a [Bus
Pirate](https://code.google.com/p/the-bus-pirate/) from [Seeed
Studio](https://www.seeedstudio.com/). It took about a week to ship, which is
pretty quick, I think. I picked it up from the post office on Tuesday.

After opening the package and plugging it in, I was greeted with nice blinky
lights, and my computer *didn't* start smoking, so that was a good sign.
However, I seemed to run into a bug, where every few lines or so, the display
would be screwed up. I started with
[minicom](https://salsa.debian.org/minicom-team/minicom), until I found out
that I could talk to the bus using
[screen](https://www.gnu.org/software/screen/) instead. But the output was
still not working. Even if I did something simple like press Enter repeatedly,
after 5 or so lines, the prompt would show incorrectly. That was kind of
disappointing, since Seeed had put a sticker on it for passing QC, so I thought
I had a bad cable or set up something wrong.

But today, I finally hit a stroke of luck. One time, upon connecting, I noticed
*something* writing to the port. The text started with `AT...` and I of course
recognized those as modem commands. I remembered a bug report or blog post
about ModemManager messing with (things that look like) serial ports.

[ModemManager](https://mail.gnome.org/archives/networkmanager-list/2008-August/msg00004.html)
works alongside NetworkManager to provide support for connecting through GSM
and CDMA modems. The problem is that a lot of modems suck and ModemManager
needs to probe serial ports to see if it's really a modem. Since I'm on a
desktop and I probably wouldn't ever use a modem, I could just remove it, but
then what would I do on the laptop?

So I set out to find how to blacklist a serial port from ModemManager. I
couldn't just get ModemManager to ignore all Bus Pirates, because it identifies
itself as a generic FTDI USB-to-serial converter. While I can let [Dangerous
Prototypes](http://dangerousprototypes.com/) go for that since it's a little
hacking board, I'm sure there are *actual* companies using an FTDI chip who
don't identify their modem devices uniquely. Thus, blacklisting the entire FTDI
series was out.

All was not lost however, as ModemManager's blacklist basically works through
[udev](https://www.freedesktop.org/software/systemd/man/udev.html). Because USB
device numbers are not static, I was already using a udev rule to give me a
static device name for the Bus Pirate (from the [Arch Linux
wiki](https://wiki.archlinux.org/index.php/Bus_pirate)). After a quick look at
the ModemManager [blacklist
rules](https://cgit.freedesktop.org/ModemManager/ModemManager/tree/src/77-mm-usb-device-blacklist.rules),
I found that it uses `ENV{ID_MM_DEVICE_IGNORE}="1"` to signal to ModemManager
to ignore a device.

So, to sum it all up, you need to create a udev rule in
`/etc/udev/rules.d/98-bus-pirate.rules`:

{{< highlight "clike" >}}
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403",\
    ATTRS{idProduct}=="6001", ATTRS{serial}=="...",\
    GROUP="users", MODE="0660", SYMLINK+="buspirate"

ACTION=="add|change", SUBSYSTEM=="usb",\
    ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="0403",\
    ATTRS{idProduct}=="6001", ATTRS{serial}=="...",\
    ENV{ID_MM_DEVICE_IGNORE}="1"
{{< /highlight >}}

Check the Arch wiki for how to get the proper serial number. The first rule
sets up a `/dev/buspirate` device node link so you have a memorable name to use
(instead of `/dev/ttyUSB#`). It also sets permissions so that it's accessible
to the `users` group. Make sure to add your user to that group and you won't
need to be root to use the Bus Pirate. The second rule sets a variable on the
USB device so that ModemManager doesn't attempt to probe it.

To connect, just use screen. I set up an alias to run `screen /dev/buspirate
115200 8N1` and it works great. Now I just need to get something to hack on
with it.
