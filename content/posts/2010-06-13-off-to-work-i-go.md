+++
title = "Off to work I go..."
published = 2010-06-13T23:27:00.001000-04:00
tags = ["Work"]
+++

Yep, that's right. Work. In the summer!

Well actually, I started on last Wednesday, though I haven't really done too
much yet. They don't have a proper laptop for me yet, so I can't login and
don't have email. Which turns out to be pretty limiting, actually. The
temporary computer doesn't have access to most of the (internal) network, for
stuff like printers and such. Basically, I've just been reading random
documents in the meantime.

I'm not doing anything too glamorous, though. Pretty much writing documents.
Apparently, they seem to think I was good at that.

Oh yes, I forgot to mention where! At [Merrill Lynch](https://www.ml.com/).
