+++
title = "\"Video\" Editing in Linux"
published = 2012-05-31T22:03:00-04:00
tags = ["Kdenlive", "OpenShot", "PiTiVi"]
+++

Video editing is one of those domains for which there are some expensive
options and
*[several](https://fortintam.com/blog/2011/07/15/the-glorious-history-of-floss-video-editors/)*
attempts at open source alternatives. Unfortunately, few efforts have even
gotten past the first few months.

A couple months ago, I had to do some "video" editing. I put that in quotes
because in fact, it was more of a slideshow. I had done something similar a few
years ago in PowerPoint. After the pain of that experience, I was quite certain
I didn't want to do it that way again. I looked for some alternatives, but
pretty much knew what my list of prerequisites was:

1. Background music and voiceover audio
2. Easy positioning of audio clips (Possibly meaning multi-track editing)
3. Good set of transitions
4. HD output, if possible, since all my photos were high resolution already

Weighing the Options
--------------------

I know there are dedicated slideshow tools out there, like
[Imagination](https://imagination.sourceforge.io/), but I wasn't sure how to
get points 1 and 2 taken care of. I was pretty much certain that I'd have to
use a video editor of some sort. There are really only about three or four I'd
expect to work.

I was originally thinking of [OpenShot](https://openshot.org/), but annoyingly,
you can't drag multiple clips from the Project to the Timeline. This might seem
like a small issue, but for a slideshow of a few hundred images, it becomes a
bit tedious. That's one of the reasons I didn't like PowerPoint, since you had
to create a slide per picture and then add it. Overall, OpenShot just seemed to
be a bit too simplified to be usable. On the upside, it did seem to have a good
selection of transition effects.

The next option was [PiTiVi](http://www.pitivi.org/). A long time ago, when I
had to settle for PowerPoint, I had looked at PiTiVi. It was really just not
ready to use for much of anything then. Just loading files used to cause a
caught-in-abrt-but-not-totally-fatal exception. Nowadays, it's become much more
polished. The UI looks much nicer with a good layout of controls.
Unfortunately, it's limited in the transition aspect. Basically, you've got
crossfading and that's about it. With no transition wipes, that pretty much cut
out PiTiVi, although I really do like the direction it's going.

A possible contender?
---------------------

The final choice was [Kdenlive](https://kdenlive.org/). Kdenlive uses the same
video processing toolkit as OpenShot ([MLT](https://www.mltframework.org/)),
but has been around since before OpenShot and PiTiVi. I can't say whether this
longer development time is the sole reason or not, but it's certainly turned
out to be the best of the three at this time. It's obviously multi-track
(points 1 and 2), there's a good selection of transitions (point 3), and it'll
render almost whatever resolution you want (point 4).

Now, does editing real video have its issues? I can't exactly say, but for what
I had to do, I found Kdenlive to be extremely stable. In the course of this
project, I saw maybe two crashes. That's two crashes too many, but video
editing seems to be a tricky art (likely due to there being too many video
specs, with poor descriptions, and poorer implementations). Actually, I did
have to insert two real video clips, and they didn't cause any trouble.

Kdenlive was certainly up to the task for this slideshow/video project. It was
perhaps of a moderate size, a few hundred images, two video clips, a dozen
music tracks, plus 15-20 audio voice clips, resulting in about 25 minutes of
final video.

Alas, it's not quite perfect...
-------------------------------

Let's not say that Kdenlive doesn't still have its rough edges. It did appear
to use a large amount of RAM, but I think that's because the project was 1080p,
and I wasn't using proxies. Randomly (but thankfully not too many times), an
invisible spacer would insert itself on a track and shift all the subsequent
clips. Eventually, I did find the Lock Track button, and then could shift the
incorrect track back. Once, a clip became half-deleted, getting in the way of
other clips, but staying immovable and un-deletable until I re-opened the file.

While there are many to choose from, there are twenty transitions that are just
named numerically, meaning you have to pick a point in the middle of a
transition, look at the result, and guess which direction it's going (it
doesn't auto-preview the whole thing, for example). What would be really useful
is a "base" transition set, with an editor to tweak a few settings. For
example, if you had a expanding star, the editor could enable you to position
that star over some important object instead of dead-centre.

The other transitions that were missing were some good 3D effects. I tried
following [these
instructions](https://forum.kde.org/viewtopic.php?f=270&t=116618) to create a
page curl effect, but ended up keying every frame of the overlay position just
to make sure it looked good. For another effect, I used a rotate about Z to
create a sort of full page flip, but the Z rotate doesn't seem to cause any
perspective changes (the far end does not shrink in height and the near end
does not grow in height). It didn't look too bad in the end, but it could have
been easier to do.

Wrapping up
-----------

In the end, I managed to finish my project relatively easily with Kdenlive.
OpenShot is really just not up there at this time, although it does have a nice
set of 3D titles in Blender. PiTiVi is also moving along quite nicely, and if
it wasn't for the lack of transitions, I might have used it. Kdenlive seems to
be the best choice right now. But everything's under rapid development, so this
might change any time soon.
