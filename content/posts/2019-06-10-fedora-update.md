+++
title = "Fedora Update 2019 Week 20--22"
date = 2019-06-10T01:03:18-04:00
tags = ["Fedora"]
+++

Oops, again a bit late, but the past two weekends were fairly busy. I decided
to post this today so that it wouldn't slip another full week. So this probably
looks a bit larger than usual, but I hope I didn't miss anything. Two weeks ago
was rather busy with many updates. Not just new releases, but I also spent a
little time going over old updates that I've missed and ignored due to missing
dependencies. Some of these dependencies could be skipped, so I did in order to
get the update in.

Last week was a bit calmer with the updates. Instead, I spent the past week
preparing several new *Go* packages that are dependencies for
[htmltest](https://github.com/wjdp/htmltest), an interesting tool for testing
HTML files. I'm hoping these will get some reviews soon.

Last week was also spent trying to figure out several upstream bugs. Pillow has
been having {{% rhbz 1706450 "issues on s390x" %}}. This turned out to be a bug
in the types passed via varargs between Python and C code. Because of
disagreement on argument size, this generally ends up being problematic on
big-endian systems. I'm still waiting for the [upstream
PR](https://github.com/python-pillow/Pillow/pull/3880) to be accepted, but
we've used the patch and been able to rebuild several other dependent packages
with it now applied.

With [python-zarr](https://apps.fedoraproject.org/packages/python-zarr), I've
been running into {{% rhbz 1695525 "failures with LMDB on 32-bit systems" %}}.
This is generally annoying since it requires rebuilds whenever it got built on
one of those systems. Basically, the LMDB store opens a very large file mapping
and even though there's enough RAM to do so, it fails. Thanks to some
[discussion on the devel mailing
list](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/TSVKWFDDKQJ74GOCVDVJLFEF4EBSEZJS/)
I was pointed in the right direction to fix it. The tests rely on old store
being garbage collected, and so a lot of old LMDB mappings are still around
causing later ones to fail. I've opened a [pull request
upstream](https://github.com/zarr-developers/zarr/pull/442) to explicitly close
these stores, which reduces the overall memory requirement and fixes the tests.

I've backported [numpydoc 0.9
fixes](https://src.fedoraproject.org/rpms/python-pandas/pull-request/5) for
`python-pandas`, but waiting on the maintainer to merge that one. Even though
the pandas package does not build docs, it has knock-on effects to any packages
that use it.

Updated packages
================

I've finished
[work](https://src.fedoraproject.org/rpms/python-matplotlib/pull-request/18) on
updating `python-matplotlib` to 3.1.0, but there are some issues in
dependencies that prevent building it.

For `R-rmarkdown`, I've tried to improve some bits of the unbundling. I've
switched to using `%autosetup -S git` instead of manually deleting bundled
items and started re-minifying the CSS to make it simpler to patch.

Also, somewhat trivial, but I added `gcc-c++` to the Requires of the *Go* devel
packages that need it, instead of leaving it to the final leaf package to
include it. This just makes using them a little bit less annoying.

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-evaluate" "0.14-1" %}}
{{% fedora-update "R-gss" "2.1.10-1" %}}
{{% fedora-update "R-gtable" "0.3.0-1" %}}
{{% fedora-update "R-markdown" "1.0-1" %}}
{{% fedora-update "R-measurements" "1.4.0-1" %}}
{{% fedora-update "R-nanotime" "0.2.4-1" %}}
{{% fedora-update "R-openssl" "1.4-1" %}}
{{% fedora-update "R-pillar" "1.4.0-1" %}}
{{% fedora-update "R-pillar" "1.4.1-1" %}}
{{% fedora-update "R-repr" "1.0.1-1" %}}
{{% fedora-update "R-reprex" "0.3.0-1" %}}
{{% fedora-update "R-rgdal" "1.4.4-1" %}}
{{% fedora-update "R-rmarkdown" "1.13-1" %}}
{{% fedora-update "R-rmarkdown" "1.13-2" "Fixups noted above" %}}
{{% fedora-update "R-statnet.common" "4.3.0-1" %}}
{{% fedora-update "R-stringdist" "0.9.5.2-1" "Needed new R-tinytest" %}}
{{% fedora-update "R-tibble" "2.1.2-1" %}}
{{% fedora-update "R-tibble" "2.1.3-1" "(Second bug fix)" %}}
{{% fedora-update "asv" "0.4-1" %}}
{{% fedora-update "asv" "0.4.1-1" %}}
{{% fedora-update "golang-github-wellington-libsass" "0.9.2-3"
  "Add gcc-c++ Requires" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.3.20190604git0713e35"
  "Add gcc-c++ Requires" %}}
{{% fedora-update "ocrmypdf" "8.3.0-1" "Needed pillow fix" %}}
{{% fedora-update "proj-datumgrid-europe" "1.3-1" %}}
{{% fedora-update "python-cartopy" "0.17.0-5"
  "Fix build with FreeType 2.10 in Rawhide" %}}
{{% fedora-update "python-pikepdf" "1.3.0-1" "Needed pillow fix" %}}
{{% fedora-update "python-pykdtree" "1.3.1-3"
  "Re-cythonize sources from @cstratak" %}}
{{% fedora-update "python-rasterio" "1.0.24-1" %}}
{{% fedora-update "python-zarr" "2.3.2-1" %}}
{{% fedora-update "python-zarr" "2.3.2-2"
  "Fix LMDB test issues on 32-bit systems" %}}

New packages
============

There are some new packages for *R* and *Python* dependencies:

* {{% rhbz 1713257 "R-shiny" %}} --- Web Application Framework for R
* {{% rhbz 1713287 "R-rsvg" %}} --- Render SVG Images into PDF, PNG,
  PostScript, or Bitmap Arrays
* {{% rhbz 1714049 "R-miniUI" %}} --- Shiny UI Widgets for Small Screens
* {{% rhbz 1714051 "R-styler" %}} --- Non-Invasive Pretty Printing of R Code
* {{% rhbz 1715296 "libinsane" %}} --- Cross-platform access to image scanners
* {{% rhbz 1718122 "R-tinytest" %}} --- Lightweight but Feature Complete Unit
  Testing Framework

For the *Go* macros, I've finally finished up
[golist](https://pagure.io/golist) 0.10.0 and built a package for it. This
release fixes several issues we've had with it supporting all the things we
need to get the macros working. Hopefully we can move forward with the rebuild
against the new macros soon.

* {{% rhbz 1714090 "golist" %}} --- A tool to analyse the properties of a Go
  (Golang) codebase

And here are the new ones for [htmltest](https://github.com/wjdp/htmltest):

* {{% rhbz 1713833 "golang-github-golangplus-fmt" %}} --- Plus to standard fmt package
* {{% rhbz 1713834 "golang-github-golangplus-testing" %}} --- Plus to the standard testing package
* {{% rhbz 1713835 "golang-github-golangplus-bytes" %}} --- Plus to the standard "bytes" package
* {{% rhbz 1713836 "golang-github-golangplus-sort" %}} --- Plus to standard sort package
* {{% rhbz 1713837 "golang-github-daviddengcn-villa" %}} --- Priority queue and slice wrappers for Go
* {{% rhbz 1713838 "golang-github-daviddengcn-algs" %}} --- Maxflow and edit-distance algorithms in Go
* {{% rhbz 1713839 "golang-github-daviddengcn-assert" %}} --- Testing utils for Go
* {{% rhbz 1713841 "golang-gopkg-seborama-govcr-2" %}} --- HTTP mock for Golang
* {{% rhbz 1713843 "htmltest" %}} --- Test generated HTML for problems

These new packages mentioned here and in the last post were imported and built
(some only in Rawhide):

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-clisymbols" "0.1.0-1" %}}
{{% fedora-update "R-foghorn" "1.1.0-1" "Rawhide only" %}}
{{% fedora-update "R-gh" "1.0.1-1" "Rawhide only" %}}
{{% fedora-update "R-haven" "2.1.0-1" %}}
{{% fedora-update "R-ini" "0.1.0-1" %}}
{{% fedora-update "R-miniUI" "0.1.1.1-1" "Rawhide and 30 only" %}}
{{% fedora-update "R-parsedate" "0.1.0-1" %}}
{{% fedora-update "R-rappdirs" "0.1.0-1" %}}
{{% fedora-update "R-rsvg" "1.3-1" "Rawhide and 30 only" %}}
{{% fedora-update "R-shiny" "1.3.2-1" "Rawhide and 30 only" %}}
{{% fedora-update "R-sodium" "1.1-1" %}}
{{% fedora-update "R-styler" "1.1.1-1" %}}
{{% fedora-update "R-tinytest" "0.9.4-1" %}}
{{% fedora-update "R-vctrs" "0.1.0-1" %}}
{{% fedora-update "R-xopen" "0.1.0-1" %}}
{{% fedora-update "R-zeallot" "0.1.0-1" %}}
{{% fedora-update "golist" "0.10.0-1" "Rawhide only for now" %}}
{{% fedora-update "libinsane" "1.0-1" %}}

Reviews
=======

I started reviewing this one, but as it was a bit flaky, the packager decided
to hold off on completing it:

* {{% rhbz 1714432 "golang-github-robfig-cron" %}} --- Cron library for go

This one mentioned last week is approved now:

* {{% rhbz 1706659 "ensmallen" %}} --- header-only C++ library for efficient
  mathematical optimization
