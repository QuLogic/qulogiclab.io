+++
title = "Deconstructing Facebook spam"
published = 2010-07-10T00:29:00.005000-04:00
tags = ["Spam", "Facebook"]
+++

*I split this into a couple posts because it's a bit long.*

The Pitch
---------

So someone (let's not point any fingers) "suggested" I might like "How to know
who blocked me ?", a random Facebook Page. Now, ignoring the bad grammar, I
took a look anyway. What we have is this lovely set of instructions:

{{< figure src="Screenshot.png" >}}

OK, OK, wait... Those are the instructions? Seriously? Let's point out a few
reasons why you probably shouldn't follow those instructions:

1. Haven't you ever received an e-mail with a link (like an e-card, maybe)?
   It's always "Click link, or try copying this URL into your browser". Why are
   these instructions "backwards"?
2. What kind of crazy jibberish link is that? OK, so to a lot of people,
   *anything* on a computer is jibberish, but this one's a whole different
   level of jibberish.
3. It starts with "javascript:"!

So you don't know what that last point means? Basically, the part of the URI
before the colon defines the protocol the browser uses to communicate with a
server (the remote end). "http://" is a regular website, "https://" is a secure
website, etc. On the other hand, "javascript:" means "treat the rest of the URI
as [JavaScript](https://en.wikipedia.org/wiki/JavaScript) code". So by entering
that code, you're really making the browser do who-knows-what. Granted, the
instruction did say that it's code, and the step is to "Invite your friends",
but why doesn't it use Facebook's standard invite display?

Many questions and oddities means you probably shouldn't do it. But not
everyone notices those sorts of things.
