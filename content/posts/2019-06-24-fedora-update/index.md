+++
title = "Fedora Update 2019 Week 23--24"
date = 2019-06-24T19:46:39-04:00
tags = ["Fedora"]
+++

It's been another two weeks, so time for another update. Package updates have
been rather calm since the last post. I continue to work on adding *R*
packages, and some new things, like
[glava](https://github.com/wacossusca34/glava), the OpenGL audio spectrum
analyzer, which can produce cool things like:

{{< figure src="glava.gif" >}}

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-deldir" "0.1.21-1" %}}
{{% fedora-update "R-ellipsis" "0.2.0-1" "Rawhide only" %}}
{{% fedora-update "R-formatR" "1.7-1" %}}
{{% fedora-update "R-gdtools" "0.1.9-1" %}}
{{% fedora-update "R-git2r" "0.25.2-2" "Rawhide only for new libgit2" %}}
{{% fedora-update "R-mime" "0.7-1" %}}
{{% fedora-update "R-showtext" "0.7-1" %}}
{{% fedora-update "R.utils" "2.9.0-1" "Rawhide only" %}}
{{% fedora-update "git-annex" "7.20190615-1" %}}
{{% fedora-update "golang-github-jinzhu-now" "1.0.1-1" %}}
{{% fedora-update "golang-github-briandowns-spinner" "1.6.1-1" %}}
{{% fedora-update "golang-github-git-lfs-gitobj" "1.3.0-1" %}}
{{% fedora-update "golist" "0.10.1" "Fixes install of .S files" %}}
{{% fedora-update "python-octave-kernel" "0.31.0-1" %}}
{{% fedora-update "python-pyocr" "0.7.2-1" %}}

New packages
============

Continuing to work on the *tidyverse*, getting up to `R-devtools` and several
of its dependencies. This one will probably unlock quite a few optional tests,
and maybe other new packages.

* {{% rhbz "1721292" "R-sessioninfo" %}}
* {{% rhbz "1721338" "R-rcmdcheck" %}}
* {{% rhbz "1721732" "R-packrat" %}}
* {{% rhbz "1722291" "R-rsconnect" %}}
* {{% rhbz "1722293" "R-pkgdown" %}}
* {{% rhbz "1722295" "R-usethis" %}}
* {{% rhbz "1722393" "R-devtools" %}}
* {{% rhbz "1722397" "R-rhub" %}}

Also a cool new thing is [glava](https://github.com/wacossusca34/glava), an
OpenGL audio spectrum visualizer, which I just packaged. On Fedora 30, it's
already in stable due to eager user testing.

* {{% rhbz "1720575" "python-glad" %}}
* {{% rhbz "1721770" "glava" %}}

Most of these were accepted and now in testing:

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-clisymbols" "1.0.1-1" "Added F30 build" %}}
{{% fedora-update "R-gh" "1.0.1-1" "Added F30 build" %}}
{{% fedora-update "R-ini" "0.3.1-1" "Added F30 build" %}}
{{% fedora-update "R-packrat" "0.5.0-1" %}}
{{% fedora-update "R-parsedate" "1.2.0-1" "Added F30/F29 builds" %}}
{{% fedora-update "R-pkgdown" "1.3.0-1" %}}
{{% fedora-update "R-rappdirs" "0.3.1-1" "Added F30/F29 builds" %}}
{{% fedora-update "R-rcmdcheck" "1.3.3-1" %}}
{{% fedora-update "R-rhub" "1.1.1-1" %}}
{{% fedora-update "R-rsconnect" "0.8.13-1" %}}
{{% fedora-update "R-sessioninfo" "1.1.1-1" %}}
{{% fedora-update "R-usethis" "1.5.0-1" %}}
{{% fedora-update "R-xopen" "1.0.0-1" "Added F30/F29 builds" %}}
{{% fedora-update "glava" "1.6.3-1" %}}
{{% fedora-update "python-glad" "0.1.30-1" %}}

Reviews
=======

Not many this period, unfortunately.

* {{% rhbz "1712280" "golang-github-hanwen-fuse" %}} --- FUSE bindings for Go
