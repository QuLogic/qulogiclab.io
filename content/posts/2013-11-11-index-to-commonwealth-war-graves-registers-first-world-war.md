+++
title = "Index to Commonwealth War Graves Registers --- First World War"
published = 2013-11-11T23:00:00-05:00
tags = ["Genealogy"]
+++

***Note:*** Library and Archives Canada has since added an index, so this page
is unlikely to be of any additional help. [Check out their new page
instead](http://www.bac-lac.gc.ca/eng/discover/mass-digitized-archives/commonwealth-war-graves-registers/Pages/commonwealth-war-graves-registers.aspx).

---

This weekend, Ancestry.ca held another of their "freebies", with access to
Canadian Military records for the past 5 or so days. In general, I assumed that
I already have most of the information since they were available from the
previous freebies as well as free on the Government of Canada or CWGC sites.
However, I took a look anyway, just in case.

One new thing that I found was the "Commonwealth War Graves Registers, First
World War" (also known as the Black Binders, volumes 39 to 144 of the series
`Accession RG 150, 1992-93/314`). It's not *that* new, in fact, as [they were
digitized in
2011](https://www.bac-lac.gc.ca/eng/news/Pages/new-digitized-reels-war-graves-registers-of-the-first-world-war.aspx).
[As pointed out
here](https://thefamilyrecorder.blogspot.ca/2011/04/commonwealth-war-graves-registers-on.html),
the real advantage of the Ancestry site is the integrated search, as most of
the information is on the [CWGC site](https://www.cwgc.org/), but with a
slightly less capable search.

However, there are small bits of additional information, and if you're like me,
you'd like to have as many references as possible (even if just scanned). The
trouble is that the Ancestry site will no longer be free in a few hours. One
could [use the Library and Archives Canada
site](https://www.collectionscanada.gc.ca/microform-digitization/006003-110.02-e.php?&q2=27&interval=50&sk=0)
but it is completely unsearchable. I don't know why they couldn't have listed
the names included in each set. After all, they went through the trouble of
labelling the names on Page 2 of every set. The time it would take to type this
up is surely much shorter than the time taken to scan the binders (or else
something is terribly wrong).

Anyway, I decided to do so, and it didn't take too long. The index is available
below:

| Index | Name                                                                                                                                         | Location              | First Person | Last Person  |
|-------|----------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|--------------|--------------|
| 1     | [31830_B016669](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2149&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Aaron        | Allan        |
| 2     | [31830_B016673](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2150&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Allanson     | Angst        |
| 3     | [31830_B016674](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2151&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Angus        | Astley       |
| 4     | [31830_B016672](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2152&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Aston        | Baker        |
| 5     | [31830_B016677](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2153&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bakys        | Barrow       |
| 6     | [31830_B016663](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2154&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Barry        | Beebe        |
| 7     | [31830_B034750](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2155&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Beech        | Berner       |
| 8     | [31830_B016668](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2156&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bernhart     | Bittle       |
| 9     | [31830_B016662](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2157&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bjarnason    | Bolger       |
| 10    | [31830_B016667](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2158&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bolin        | Bowes        |
| 11    | [31830_B016666](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2159&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bowhay       | Briden       |
| 12    | [31830_B016665](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2160&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bridge       | Brown, I. F. |
| 13    | [31830_B016671](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2161&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Brown, J.    | Buist        |
| 14    | [31830_B016670](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2162&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Bulbick      | Bytheway     |
| 15    | [31830_B016591](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2163&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cabana       | Canning      |
| 16    | [31830_B016590](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2164&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cannon       | Cave         |
| 17    | [31830_B016589](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2165&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cavelle      | Christmas    |
| 18    | [31830_B016594](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2166&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Christo      | Cobley       |
| 19    | [31830_B016593](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2167&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Coburn       | Coogan       |
| 20    | [31830_B016592](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2168&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cook         | Cotton       |
| 21    | [31830_B016595](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2169&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cottrell     | Crewe        |
| 22    | [31830_B016596](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2170&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Cribbes      | Cyrs         |
| 23    | [31830_B016600](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2171&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Dabate       | Dawson       |
| 24    | [31830_B016597](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2172&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Day          | Dickie       |
| 25    | [31830_B016599](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2173&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Dickins      | Dostie       |
| 26    | [31830_B016598](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2174&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Doty         | Dunbar       |
| 27    | [31830_B016602](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2175&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Duncan       | Edwards      |
| 28    | [31830_B016601](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2176&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Edworthy     | Ezric        |
| 29    | [31830_B016606](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2177&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Faber        | Fincham      |
| 30    | [31830_B016605](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2178&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Finder       | Ford         |
| 31    | [31830_B034751](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2179&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Fordyce      | Freymuth     |
| 32    | [31830_B016604](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2180&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Fricker      | Gaston       |
| 33    | [31830_B016609](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2181&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Gate         | Gingras      |
| 34    | [31830_B016608](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2182&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Ginn         | Gotze        |
| 35    | [31830_B016607](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2183&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Goucher      | Green        |
| 36    | [31830_B016611](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2184&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Greenaway    | Gysin        |
| 37    | [31830_B016610](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2185&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Haas         | Hancox       |
| 38    | [31830_B016675](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2186&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Hand         | Harty        |
| 39    | [31830_B016676](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2187&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Harvey       | Helms        |
| 40    | [31830_B016680](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2188&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Helps        | Hildreth     |
| 41    | [31830_B034451](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2189&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Hill         | Holmden      |
| 42    | [31830_B016679](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2190&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Holmes       | Howles       |
| 43    | [31830_B016678](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2191&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Howlett      | Hutson       |
| 44    | [31830_B034447](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2192&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Hutt         | James        |
| 45    | [31830_B016681](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2193&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Jameson      | Johnsson     |
| 46    | [31830_B034450](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2194&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Johnston     | Juteau       |
| 47    | [31830_B034449](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2195&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Kaar         | Kernick      |
| 48    | [31830_B016682](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2196&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Kerr         | Knibbs       |
| 49    | [31830_B034448](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2197&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Knight       | Landymore    |
| 50    | [31830_B034452](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2198&amp;interval=50&amp;sk=0)   | France, Belgium, etc. | Lane         | Leach        |
| 51    | [31830_B016603](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2199&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Leachman     | Levine       |
| 52    | [31830_B016614](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2200&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Levings      | Lockie       |
| 53    | [31830_B016613](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2201&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Lockley      | Lyttle       |
| 54    | [31830_B016612](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2202&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Mabee        | Marsden      |
| 55    | [31830_B016617](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2203&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Marsh        | May          |
| 56    | [31830_B016616](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2204&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Mayall       | Millens      |
| 57    | [31830_B016615](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2205&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Miller       | Moir         |
| 58    | [31830_B016620](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2206&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Moisley      | Morley       |
| 59    | [31830_B016619](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2207&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Morphew      | Muir         |
| 60    | [31830_B016621](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2208&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Muirhead     | Myson        |
| 61    | [31830_B034752](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2209&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McAdam       | McCoubrey    |
| 62    | [31830_B016618](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2210&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McCourt      | McDonaugh    |
| 63    | [31830_B016622](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2211&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McDonell     | McIntire     |
| 64    | [31830_B016626](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2212&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McIntosh     | MacKenzie    |
| 65    | [31830_B016625](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2213&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McKeogh      | McLeman      |
| 66    | [31830_B016624](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2214&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McLennan     | McNaughton   |
| 67    | [31830_B016623](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2215&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | McNaul       | McWilliams   |
| 68    | [31830_B016630](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2216&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Nadeau       | Nicoletti    |
| 69    | [31830_B016629](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2217&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Nicoll       | Olivier      |
| 70    | [31830_B016628](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2218&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Olliff       | Papworth     |
| 71    | [31830_B016627](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2219&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Paquet       | Pattison     |
| 72    | [31830_B016633](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2220&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Patton       | Perry        |
| 73    | [31830_B016632](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2221&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Perryman     | Plow         |
| 74    | [31830_B016631](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2222&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Plowman      | Price        |
| 75    | [31830_B016635](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2223&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Prickett     | Ramsey       |
| 76    | [31830_B016634](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2224&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Rance        | Reid         |
| 77    | [31830_B016638](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2225&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Reiley       | Rizza        |
| 78    | [31830_B016637](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2226&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Roach        | Roffey       |
| 79    | [31830_B016636](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2227&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Roger        | Roy          |
| 80    | [31830_B016641](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2228&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Royal        | Saunders     |
| 81    | [31830_B016640](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2229&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Sauvage      | Selfe        |
| 82    | [31830_B034455](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2230&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Selkirk      | Sholds       |
| 83    | [31830_B016644](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2231&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Sholert      | Smales       |
| 84    | [31830_B016643](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2232&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Small        | Smith, P. M. |
| 85    | [31830_B034454](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2233&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Smith, R.    | Spence       |
| 86    | [31830_B016642](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2234&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Spencer      | Steven       |
| 87    | [31830_B016647](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2235&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Stevens      | St. Pierre   |
| 88    | [31830_B016646](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2236&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Stracey      | Syvret       |
| 89    | [31830_B016645](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2237&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Tabbener     | Thom         |
| 90    | [31830_B016639](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2238&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Thomas       | Tizzard      |
| 91    | [31830_B034453](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2239&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Tobias       | Turnbull     |
| 92    | [31830_B016649](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2240&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Turner       | Vyse         |
| 93    | [31830_B016656](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2241&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Wabanosse    | Warby        |
| 94    | [31830_B016651](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2242&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Ward         | Webb         |
| 95    | [31830_B016653](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2243&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Webber       | Whitcutt     |
| 96    | [31830_B016654](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2244&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | White        | Willey       |
| 97    | [31830_B016655](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2245&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Williams     | Wilson       |
| 98    | [31830_B016658](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2246&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Wilton       | Worsley      |
| 99    | [31830_B016652](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2247&amp;interval=50&amp;sk=51)  | France, Belgium, etc. | Worster      | Zykoski      |
| 100   | [31830_B016657](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2248&amp;interval=50&amp;sk=51)  | United Kingdom        | Abbott       | Byrt         |
| 101   | [31830_B016650](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2249&amp;interval=50&amp;sk=101) | United Kingdom        | Cadieux      | Ewens        |
| 102   | [31830_B016661](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2250&amp;interval=50&amp;sk=101) | United Kingdom        | Fagg         | Holmes       |
| 103   | [31830_B016648](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2251&amp;interval=50&amp;sk=101) | United Kingdom        | Holtum       | Meads        |
| 104   | [31830_B016660](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2252&amp;interval=50&amp;sk=101) | United Kingdom        | Meehen       | Ozanne       |
| 105   | [31830_B016659](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2253&amp;interval=50&amp;sk=101) | United Kingdom        | Packham      | Smith        |
| 106   | [31830_B016664](https://www.collectionscanada.gc.ca/microform-digitization/006003-119.01-e.php?q2=27&amp;q3=2254&amp;interval=50&amp;sk=101) | United Kingdom        | Smollet      | Zinck        |
