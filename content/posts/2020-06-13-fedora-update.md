+++
title = "Fedora Update Weeks 10--24"
date = 2020-06-13T00:01:28-04:00
tags = ["Fedora"]
+++

It's been quite a long while since the last packaging update. If you've not
followed me on Twitter, you may not have seen that I [started working on
Matplotlib](https://matplotlib.org/matplotblog/posts/matplotlib-rsef/). While I
have not stopped packaging, I have had somewhat less time for some of the
extras, like writing up these posts. One major impetus for putting this one
together is that Fedora 30 has reached End-of-Life.

I did fall behind slightly with *R* packages again, so that lead to a few
batches of many updates at once. I tried to squeeze these in before Fedora 30
went EOL, but was a bit too late with some of them. Unfortunately, that means I
have a few dangling updates in testing, but they're now marked obsolete. Alas,
nothing can be done about that now since Fedora 30 is locked.

The fixes for `tinygo` mentioned in the previous update have finally gone out.
Upstream and I worked out both Go 1.14 and LLVM 10 issues, so that all current
releases are now at the latest version.

Other than that, there have been few major issues with packaging, with anything
notable being rather minor. Some *JavaScript* packages were retired, so I've
had to re-bundle them into some packages. While I dislike it, that seems to be
a necessary evil with many *JavaScript* things these days. With
`python-xarray`, a {{% rhbz 1825455 "weird bug" %}} caused it to be unusable,
but it was an easy fix. In Rawhide, a few other packages needed tweaks due to
dependency changes, but these changes are pretty normal.

The only notable major thing has been updating `fedora-obsolete-packages`. In
Fedora 30 and 31, `python2-matplotlib` was updated to the latest version,
2.2.5, but the obsoletion was for an older version, which prevented upgrades to
Fedora 32. Also, I've held out on Fedora 30 for various reasons, and found
several conflicts on upgrades due to retired `python2-*` packages. So I went
through to find all of those, and added `Obsoletes` for all of them in the
Fedora 31 and 32 versions of `fedora-obsolete-packages`.

Lack of time did mean I was unable to do very many reviews unfortunately,
though there are some in progress at the moment.

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "R-AsioHeaders" "1.12.2.1-1" %}}
{{% fedora-update "R-Cairo" "1.5.11-1" %}}
{{% fedora-update "R-Cairo" "1.5.12-1" %}}
{{% fedora-update "R-RcppCCTZ" "0.2.7-1" %}}
{{% fedora-update "R-V8" "3.0.2-1" %}}
{{% fedora-update "R-V8" "3.1.0-1" %}}
{{% fedora-update "R-ape" "5.3-6" "Rebuild against R without libRlapack.so" %}}
{{% fedora-update "R-backports" "1.1.7-1" %}}
{{% fedora-update "R-broom" "0.5.6-1" %}}
{{% fedora-update "R-callr" "3.4.3-1" %}}
{{% fedora-update "R-corpus" "0.10.1-1" %}}
{{% fedora-update "R-dbplyr" "1.4.3-1" %}}
{{% fedora-update "R-diffobj" "0.2.4-1" %}}
{{% fedora-update "R-diffobj" "0.3.0-1" %}}
{{% fedora-update "R-dplyr" "0.8.5-1" %}}
{{% fedora-update "R-ellipsis" "0.3.1-1" %}}
{{% fedora-update "R-fontBitstreamVera" "0.1.1-7"
    "Update font paths to new location, cleanup spec" %}}
{{% fedora-update "R-forcats" "0.5.0-1" %}}
{{% fedora-update "R-foreach" "1.5.0-1" %}}
{{% fedora-update "R-future" "1.17.0-1" %}}
{{% fedora-update "R-gargle" "0.5.0-1" "Rawhide only" %}}
{{% fedora-update "R-gdata" "2.18.0-8" "Add explicit Perl dependency" %}}
{{% fedora-update "R-gdtools" "0.2.2-1" %}}
{{% fedora-update "R-git2r" "0.27.1-1" %}}
{{% fedora-update "R-glue" "1.3.2-1" %}}
{{% fedora-update "R-gmp" "0.5.14-1" %}}
{{% fedora-update "R-gss" "2.2.1-1" %}}
{{% fedora-update "R-gss" "2.2.2-1" %}}
{{% fedora-update "R-gtools" "3.8.2-1" %}}
{{% fedora-update "R-haven" "2.3.0-1" "Rawhide only" %}}
{{% fedora-update "R-httpuv" "1.5.3.1-1" %}}
{{% fedora-update "R-igraph" "1.2.5-1" %}}
{{% fedora-update "R-lifecycle" "0.2.0-1" %}}
{{% fedora-update "R-lubridate" "1.7.8-1" "Rawhide only" %}}
{{% fedora-update "R-mnormt" "1.5.7-1" %}}
{{% fedora-update "R-modelr" "0.1.8-1" %}}
{{% fedora-update "R-pillar" "1.4.4-1" %}}
{{% fedora-update "R-pkgbuild" "1.0.8-1" %}}
{{% fedora-update "R-pkgdown" "1.5.0-1" %}}
{{% fedora-update "R-pkgdown" "1.5.1-1" %}}
{{% fedora-update "R-plyr" "1.8.6-1" %}}
{{% fedora-update "R-ps" "1.3.3-1" %}}
{{% fedora-update "R-purrr" "0.3.4-1" %}}
{{% fedora-update "R-rematch2" "2.1.2-1" %}}
{{% fedora-update "R-reshape2" "1.4.4-1" %}}
{{% fedora-update "R-reticulate" "1.15-1" %}}
{{% fedora-update "R-reticulate" "1.16-1" %}}
{{% fedora-update "R-rex" "1.2.0-1" %}}
{{% fedora-update "R-rgeos" "0.5.3-1" %}}
{{% fedora-update "R-rlang" "0.4.6-1" %}}
{{% fedora-update "R-rmarkdown" "1.18-2"
    "Re-bundle highlightjs; Fedora 31 only" %}}
{{% fedora-update "R-rmarkdown" "2.1-2"
    "Re-bundle highlightjs; Fedora 32+" %}}
{{% fedora-update "R-roxygen2" "7.1.0-1" %}}
{{% fedora-update "R-rversions" "2.0.2-1" %}}
{{% fedora-update "R-scales" "1.1.1-1" %}}
{{% fedora-update "R-sfsmisc" "1.1.7-1" %}}
{{% fedora-update "R-shiny" "1.4.0-3" "Re-bundle highlightjs" %}}
{{% fedora-update "R-shiny" "1.4.0.2-1" %}}
{{% fedora-update "R-showtext" "0.8-1" %}}
{{% fedora-update "R-showtext" "0.8.1-1" %}}
{{% fedora-update "R-sp" "1.4.2-1" %}}
{{% fedora-update "R-styler" "1.3.2-1" %}}
{{% fedora-update "R-sysfonts" "0.8.1-1" %}}
{{% fedora-update "R-systemfonts" "0.2.2-1" %}}
{{% fedora-update "R-tibble" "3.0.0-1" %}}
{{% fedora-update "R-tibble" "3.0.1-1" %}}
{{% fedora-update "R-tidyselect" "1.1.0-1" %}}
{{% fedora-update "R-tinytest" "1.2.1-1" "Rawhide only" %}}
{{% fedora-update "R-tufte" "0.6-1" %}}
{{% fedora-update "R-unitizer" "1.4.9-1" %}}
{{% fedora-update "R-unitizer" "1.4.10-1" %}}
{{% fedora-update "R-vctrs" "0.2.4-1" %}}
{{% fedora-update "R-vctrs" "0.3.0-1" "Rawhide only" %}}
{{% fedora-update "R-webutils" "1.1-1" %}}
{{% fedora-update "R-withr" "2.2.0-1" %}}
{{% fedora-update "R-xfun" "0.14-1" %}}
{{% fedora-update "R-xml2" "1.2.5-1" %}}
{{% fedora-update "R-xml2" "1.3.2-1" %}}
{{% fedora-update "asv" "0.4.1-8" "Loosen up jQuery dependency" %}}
{{% fedora-update "asv" "0.4.2-1" %}}
{{% fedora-update "exercism" "3.0.13-3"
    "Move fish completions to vendor directory" %}}
{{% fedora-update "fedora-obsolete-packages" "31-42"
    "Bump versions on `python2-matplotlib-*` Obsoletes" %}}
{{% fedora-update "fedora-obsolete-packages" "31-43"
    "Fix several F30->F31 upgrade issues" %}}
{{% fedora-update "fedora-obsolete-packages" "32-49"
    "Add back `python2-matplotlib-*` Obsoletes" %}}
{{% fedora-update "fedora-obsolete-packages" "32-51"
    "Fix several F30->F32 upgrade issues" %}}
{{% fedora-update "fzf" "0.21.1-1" %}}
{{% fedora-update "git-annex" "7.20200309-1" "Fedora 31 only" %}}
{{% fedora-update "git-annex" "8.20200226-1" "Fedora 32+ only" %}}
{{% fedora-update "git-annex" "8.20200309-1" "Fedora 32+ only" %}}
{{% fedora-update "git-annex" "8.20200330-1" "Fedora 32+ only" %}}
{{% fedora-update "git-cinnabar" "0.5.5-1" %}}
{{% fedora-update "git-lfs" "2.11.0-1" %}}
{{% fedora-update "golang-bug-serial-1" "1.1.0-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.3-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.4-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.5-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.7-1" %}}
{{% fedora-update "golang-github-alecthomas-kong" "0.2.9-1" %}}
{{% fedora-update "golang-github-bep-golibsass" "0.7.0-1" %}}
{{% fedora-update "golang-github-briandowns-spinner" "1.10.0-1" %}}
{{% fedora-update "golang-github-cosiner-argv" "0.1.0-1" %}}
{{% fedora-update "golang-github-golangplus-sort" "1.0.0-1" %}}
{{% fedora-update "golang-github-niklasfasching-org" "1.1.0-1" %}}
{{% fedora-update "golang-github-pebbe-zmq4" "1.2.0-1" %}}
{{% fedora-update "golang-github-pebbe-zmq4" "1.2.1-1" %}}
{{% fedora-update "golang-github-stretchr-testify" "1.5.1-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.7.3-1" %}}
{{% fedora-update "golang-github-tdewolff-minify" "2.7.4-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.24-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.25-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.26-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.27-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.29-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.30-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.31-1" %}}
{{% fedora-update "golang-github-yuin-goldmark" "1.1.32-1" %}}
{{% fedora-update "golang-tinygo-x-llvm" "0-0.10.20200419git8d12088" %}}
{{% fedora-update "golang-x-tools" "0-34.20200420git5fc56a9" %}}
{{% fedora-update "hugo" "0.68.3-1" %}}
{{% fedora-update "hugo" "0.69.0-3" "De-bootstrap" %}}
{{% fedora-update "libinsane" "1.0.4-1" %}}
{{% fedora-update "ocrmypdf" "9.6.1-1" %}}
{{% fedora-update "ocrmypdf" "9.7.0-1" %}}
{{% fedora-update "ocrmypdf" "9.7.1-1" %}}
{{% fedora-update "ocrmypdf" "9.7.2-1" %}}
{{% fedora-update "ocrmypdf" "9.8.0-1" %}}
{{% fedora-update "ocrmypdf" "9.8.1-1" %}}
{{% fedora-update "paperwork" "1.3.1-3" "Remove unused BuildRequires" %}}
{{% fedora-update "proj-datumgrid-europe" "1.6-1" %}}
{{% fedora-update "proj-datumgrid-north-america" "1.4-1" %}}
{{% fedora-update "proj-datumgrid-oceania" "1.2-1" %}}
{{% fedora-update "python-branca" "0.4.1-1" %}}
{{% fedora-update "python-cartopy" "0.18.0~b2-1" %}}
{{% fedora-update "python-cartopy" "0.18.0-1" %}}
{{% fedora-update "python-cherrypy" "18.1.2-4"
    "Fix Obsoletes for python2-cherrypy" %}}
{{% fedora-update "python-contextily" "1.0.0-1" %}}
{{% fedora-update "python-dask" "2.12.0-1" %}}
{{% fedora-update "python-dask" "2.13.0-1" %}}
{{% fedora-update "python-dask" "2.14.0-1" %}}
{{% fedora-update "python-dask" "2.16.0-1" %}}
{{% fedora-update "python-dask" "2.17.2-1" %}}
{{% fedora-update "python-dask" "2.18.0-1" %}}
{{% fedora-update "python-fsspec" "0.6.3-1" %}}
{{% fedora-update "python-fsspec" "0.7.0-1" %}}
{{% fedora-update "python-fsspec" "0.7.2-1" %}}
{{% fedora-update "python-fsspec" "0.7.3-1" %}}
{{% fedora-update "python-fsspec" "0.7.4-1" %}}
{{% fedora-update "python-geoplot" "0.4.0-2" %}}
{{% fedora-update "python-geoplot" "0.4.1-1" %}}
{{% fedora-update "python-gpxpy" "1.4.1-1" %}}
{{% fedora-update "python-jupyter-console" "6.1.0-2"
    "Backport patch for Python 3.9 support" %}}
{{% fedora-update "python-kiwisolver" "1.2.0-1" %}}
{{% fedora-update "python-matplotlib" "3.1.3-1" "Fedora 31 only" %}}
{{% fedora-update "python-matplotlib" "3.2.1-1" "Fedora 32+ only" %}}
{{% fedora-update "python-mercantile" "1.1.3-1" %}}
{{% fedora-update "python-mercantile" "1.1.4-1" %}}
{{% fedora-update "python-mplcairo" "0.3-1" %}}
{{% fedora-update "python-octave-kernel" "0.31.1-1" %}}
{{% fedora-update "python-octave-kernel" "0.32.0-1" %}}
{{% fedora-update "python-pep8-naming" "0.10.0-1" %}}
{{% fedora-update "python-pikepdf" "1.10.3-1" %}}
{{% fedora-update "python-pikepdf" "1.10.4-1" %}}
{{% fedora-update "python-pikepdf" "1.11.0-1" %}}
{{% fedora-update "python-pikepdf" "1.11.1-1" %}}
{{% fedora-update "python-pikepdf" "1.11.2-1" %}}
{{% fedora-update "python-pikepdf" "1.12.0-1" %}}
{{% fedora-update "python-pikepdf" "1.13.0-1" %}}
{{% fedora-update "python-pikepdf" "1.14.0-1" %}}
{{% fedora-update "python-pyshtools" "4.6.2-1" %}}
{{% fedora-update "python-rasterio" "1.1.4-1" %}}
{{% fedora-update "python-rasterio" "1.1.5-1" %}}
{{% fedora-update "python-ruffus" "2.8.4-1" %}}
{{% fedora-update "python-xarray" "0.15.0-2"
    "Backport fix for seaborn 0.10.0" %}}
{{% fedora-update "python-xarray" "0.15.1-1" %}}
{{% fedora-update "python-xarray" "0.15.1-2"
    "Fix broken package; test install instead of build" %}}
{{% fedora-update "tinygo" "0.13.0-1" %}}
{{% fedora-update "tinygo" "0.13.1-1" %}}
{{% fedora-update "xeus" "0.23.6-1" %}}
{{% fedora-update "xeus" "0.23.8-1" %}}
{{% fedora-update "xeus" "0.23.9-1" %}}
{{% fedora-update "xeus" "0.23.14-1" %}}
{{% fedora-update "xtl" "0.6.13-1" %}}

New packages
============

* {{% rhbz 1808635 "golang-github-gohugoio-testmodbuilder" %}} --- Some helper
  scripts used for Hugo testing
* {{% rhbz 1809908 "R-profvis" %}} --- Interactive Visualizations for Profiling
  R Code
* {{% rhbz 1813648 "golang-github-saracen-walker" %}} --- Walker is a faster,
  parallel version, of filepath.Walk
* {{% rhbz 1819968 "python-cppy" %}} --- C++ headers for C extension
  development

Reviews
=======

Several in progress, but not completed yet.
