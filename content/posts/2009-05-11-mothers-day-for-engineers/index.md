+++
title = "Mother's Day for Engineers"
published = 2009-05-11T00:24:00.015000-04:00
tags = ["Family", "DIY", "Electronics"]
+++

So, in case you were wondering, when it's Mother's Day, engineers don't give
gifts; they fix things!

In this case, we have a 5-CD changer that was starting to have problems. Now
sure, you might say that a computer would be able to save all the music from
those CDs and more. But if you know engineers (or at least, myself), then you
know we'd never throw away something that can still be salvaged (or at least
kept for parts).

The problem was that the audio seemed to cut out every once in a while. It
turns out that something was wrong with the output jacks. I tried fiddling with
them to make the connections tighter, but it didn't seem to work. Some of you
may remember the troubles I had to go through to get cables to stay connected
in such a way that the sound didn't cut out when you were over.

So, obviously, the best solution is to open it up and fix it! The circuit board
with the jack is in the bottom left of the left image, with a closeup on the
right. The other circuit board sure looks empty though.

{{< figure src="IMG_0658.JPG" >}}
{{< figure src="IMG_0660.JPG" >}}

With a little fiddling of the restraints, the connector board comes out pretty
easily. A few minutes of desoldering later, and out come the jacks. If you
recall, I made a post about [Kycon]({{< ref "2009-05-05-kycon-inc" >}})
recently. They sent me some nice red and white gold-plated RCA jacks, perfect
for replacing these (not that that's a coincidence). Here we have the new and
the old next to each other. Aren't the new ones all nice and shiny?

{{< figure src="IMG_0661.JPG" >}}

If you look closely, you'll notice that the plastic casing is shaped slightly
differently in the new ones. The old had a little hook, while the new are just
little stubs. Unfortunately, the gaps are also a little smaller. That required
a little bit of sanding on the circuit board to fit.

Anyway, with a little elbow grease, and some more soldering, I had them right
back in. The connection wasn't that great any more though (probably need to
tone down the iron), so I added a couple jumper wires for the relevant paths.
Fortunately, the sound quality did not suffer because of it (at least to my
ears and with the sound system we have).

{{< figure src="IMG_0665.JPG" >}}

And that gives us this nice little board with shiny new gold-plated jacks. The
plastic on the jumpers got a little melty, but that helped me to position them
in a good place. Anyway, after putting it back together, I tried it out on the
surround system and it sounded great. So, back upstairs it went, and that makes
one nice present done.
