+++
title = "About power outages, again"
published = 2010-07-05T23:48:00.012000-04:00
tags = ["Infrastructure", "Toronto", "Technology"]
+++

So in case you aren't in Toronto, we had a little bit of a [power
outage](https://www.680news.com/news/local/article/74019--power-restored-to-city-after-major-blackout).
Downtown, power went out about quarter to 5, though it didn't affect my work's
building. But then when we got back home and I was watching Jeopardy!, the
power went out in our area too. And it was out for about 1.75 hours, which I
think is the longest (local) blackout I've experienced in Canada (barring the
[day-long one in
2003](https://en.wikipedia.org/wiki/Northeast_Blackout_of_2003)).

Now I'm sure you remember that [previous post about power outages]({{< ref
"2009-12-29-when-the-power-goes-out" >}}) I made. The problem, of course, was
that the Hydro One site didn't cover Toronto. Well, things have changed.
Toronto Hydro now has a [site for power
outages](https://www.torontohydro.com/sites/electricsystem/poweroutages/Pages/OutageMap.aspx).

Of course, there's a different problem now. It's totally broken. Or is it? I
could access it with IE7 (which we're stuck with at work), but then it didn't
work in Firefox at home. For me, that's basically broken. Had I not tried it at
work, I would have dismissed it as useless.

Having peeked at the source of the website, I'm quite unimpressed.
"JavaScript" / "javascript"? Capitalized HTML tags? &lt;meta content="MSHTML
6.00.6000.16788" name="GENERATOR" /&gt;? Hello, Toronto Hydro? It's **2010**.
Who writes *real* websites in MS software? I'm surprised they even managed to
get Google Maps there, with the junk they have for the rest of the site.

So apparently, they haven't heard of case-sensitivity (a symptom of living on
Windows). The link on the main page uses ALLCAPS, which IE7 will ignore and
lowercase, but Firefox doesn't. Then for whatever reason, their horrible
JavaScript fails to load the Google Maps API. Sometimes it will pop up an
error, but usually it just sits there with a big blank space where the map
should be. If you lowercase the URI and go to that in Firefox, then it all
loads fine (the link above is already corrected.)

And then you go a little further through the source... and what's this? The map
data for outages is inlined in the webpage? That explains why it goes and
refreshes the *entire* page every once in a while. You wouldn't do that with
proper AJAX.

Technical reasons aside, is it any good? Well, after seeing the [HydroOne
site](https://www.hydroone.com/stormcenter/), I wouldn't say so. It'd be nice
to see the border of Toronto Hydro's jurisdiction, but that's kind of minor.
Really though, it's just a map with some patterns on it, which tells you
whether it's a big or small problem, but not much else. It says the "start of
the outage", but not when it was fixed, or even *if* it was fixed. Unlike the
HydroOne site, there's no indication whether repair crews have been dispatched,
or whether the cause is known. Even better would be an estimate of how long it
might be till the problem's fixed, though the HydroOne site doesn't seem to
have that either. (I'm a bit jaded from the horrible code.)

Someone needs to go hit them on the head with some real Web 2.0 design, and
then maybe think about what someone might want to know when the power goes out.
