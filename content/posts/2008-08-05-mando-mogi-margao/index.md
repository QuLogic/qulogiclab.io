+++
title = "Mando Mogi Margão"
published = 2008-08-05T01:16:00.006000-04:00
tags = ["Goa"]
+++

As I had mentioned before, we had a whole mando troupe staying over at our
houses. It turns out they were able to play three nights in a row. Anyway,
here are a few pictures from their show.

Performing at UTM:
{{< figure src="IMG_7394.JPG" >}}
{{< figure src="IMG_7571.JPG" >}}

Performing at the Mississauga Convention Centre:  
{{< figure src="IMG_7699.JPG" >}}
{{< figure src="IMG_7723.JPG" >}}

At the Grand Ball:
{{< figure src="IMG_7926.JPG" >}}
{{< figure src="IMG_8045.JPG" >}}
