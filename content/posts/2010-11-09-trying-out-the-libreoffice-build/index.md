+++
title = "Trying out the LibreOffice build"
published = 2010-11-09T21:42:00.003000-05:00
tags = ["Linux", "OpenOffice", "LibreOffice"]
+++

In case you haven't heard, there was a [little
fork](https://www.documentfoundation.org/) of the OpenOffice project not too
long ago. They can explain the reasons for it a whole lot better than I can, so
I'm not going to. Normally, I never built OpenOffice on Gentoo because I knew
it would take a darn long time, and the pre-built 32-bit binaries worked well
enough. With the fork, I decided to try it out anyway.

It's somewhat easier to do with the instructions for LibreOffice. Download a
bootstrapping tarball, and run a few commands. It's not as easy as the usual
`./configure; make; make install`, but pretty close.

First step is to configure and download all the dependencies. I tried to make
it use as many system libraries as I could so as to limit the build size/time.
The largest one left was Firefox/xulrunner which seemingly isn't built with the
correct flags on Fedora. So that all took up about 2-3GB. Then a "quick" `make`
later, and voilà:

{{< figure src="Screenshot-1.png" >}}

It only 4 hours 20 minutes to build. After building, it all took up a *mere*
7GB or so. It turns out that the install instructions on the [Get
Involved](https://www.libreoffice.org/community/get-involved/) page are
incorrect, but the wiki [How to
Build](https://wiki.documentfoundation.org/Development/How_to_build) page is.
So I used `make dev-install` and everything ends up in `./build/install/`.
Unlike what it says in the wiki, the makefile does not print the location of
the final install. Fortunately, it wasn't too hard to find, in
`./build/install/program`. Run `cd ./build/install/program; . ./ooenv;
./soffice.bin -writer` and there you go:

{{< figure src="Screenshot-2.png" >}}

Now I just have to figure out something to contribute.
