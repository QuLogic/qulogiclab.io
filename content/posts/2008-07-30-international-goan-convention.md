+++
title = "International Goan Convention"
published = 2008-07-30T00:29:00.003000-04:00
tags = ["Goa"]
+++

So last week was the 2008 International Goan Convention. There were a bunch of
workshops and stuff during the day which I did not attend because I had work.
Now even though it was last week, it's really ending for us tomorrow (or, later
today, anyway). That's because the Mando group was staying at our house.

Now, I wouldn't say the preparation for this event was spectacular. The
coordinators *thought* the Mando troupe was arriving Monday, when in fact they
arrived on Sunday night. As if that wasn't a shock, they didn't really have
anywhere set up for them to stay. And because we're such nice people, that's
how we ended up with *6* people staying over, not to mention the 5 others at my
grandparent's house across the street. There were 3 more not with us, but since
they needed to practice, they came over quite a bit, too.

Anyway, last week was quite a busy thing. First there was the Goan Idol on
Thursday, where I had to video tape and take pictures at the same time. Then
the Tiatr on Friday, which was amazingly long. And finally the Grand Ball on
Saturday. No pictures to put up yet, but I've got tons from everyone's camera.
More on that in another post...
