+++
title = "{{ delimit ( split .Name "-" | after 3 ) " " | title }}"
date = {{ .Date }}
tags = ["Fedora"]
+++

Updated packages
================

| Package | Version | Notes |
|---------|---------|-------|
{{% fedora-update "name" "version" "notes" %}}

New packages
============

* {{% rhbz 1234 "Name" %}} --- Description

Reviews
=======

* {{% rhbz 1234 "Name" %}} --- Description
